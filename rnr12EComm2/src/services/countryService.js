export const getCountryDataV2 = () => {
  let countryData;
  fetch('https://restcountries.com/v2/all')
    .then(response => response.json())
    .then(data => {
      countryData = data.map(item => {
        return {
          code: item.alpha2Code,
          name: item.name,
          callingCode: `+${item.callingCodes[0]}`,
          flag: `https://flagcdn.com/w320/${item.alpha2Code.toLowerCase()}.jpg`,
        };
      });
    });

  return countryData;
};
