import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {COLORS} from '../theme/theme';

const IconButton = ({containerStyle, icon, iconStyle, onPress}) => {
  return (
    <Pressable
      style={{...styles.pressable, ...containerStyle}}
      onPress={onPress}>
      <Image source={icon} style={{...styles.image, ...iconStyle}} />
    </Pressable>
  );
};

export default IconButton;

const styles = StyleSheet.create({
  pressable: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 30,
    height: 30,
    tintColor: COLORS.white,
  },
});
