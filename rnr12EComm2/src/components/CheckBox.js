import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../theme/theme';
import icons from '../constants/icons';

const CheckBox = ({containerStyle, isSelected, onPress}) => {
  return (
    <Pressable
      style={{
        flexDirection: 'row',
        ...containerStyle,
      }}
      onPress={onPress}>
      <View
        style={{
          ...styles.container,
          borderColor: isSelected ? COLORS.primary : COLORS.grey,
          backgroundColor: isSelected ? COLORS.primary : null,
        }}>
        {isSelected && <Image source={icons.checkmark} style={styles.image} />}
      </View>
      <Text style={styles.text}>
        By registering, you agree to our Terms and that you have read our Data
        Use Policy.
      </Text>
    </Pressable>
  );
};

export default CheckBox;

const styles = StyleSheet.create({
  container: {
    width: 25,
    height: 25,
    aligniItems: 'center',
    justifyContent: 'center',
    borderRadius: SIZES.base,
    borderWidth: 3,
  },
  image: {width: 20, height: 20, tintColor: COLORS.light},
  text: {flex: 1, marginLeft: SIZES.base, ...FONTS.body5},
});
