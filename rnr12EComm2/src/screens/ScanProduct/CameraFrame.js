import React from 'react';

import {Svg, Defs, Rect, Mask} from 'react-native-svg';

const CameraFrame = () => {
  return (
    <Svg height="100%" width="100%">
      <Defs>
        <Mask id="mask" x="0" y="0" height="100%" width="100%">
          <Rect height="100%" width="100%" fill="#fff" />
          <Rect x="18%" y="30%" width="250" height="250" fill="black" />
        </Mask>
      </Defs>
      <Rect
        height="100%"
        width="100%"
        fill="rgba(0, 0, 0, 0.8)"
        mask="url(#mask)"
      />
      {/* Frame Border */}
      <Rect
        x="18%"
        y="30%"
        width="250"
        height="250"
        strokeWidth="5"
        stroke="#fff"
        fillOpacity={0}
      />
    </Svg>
  );
};

export default CameraFrame;
