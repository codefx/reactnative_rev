import React, {useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import icons from '../../constants/icons';
import IconButton from '../../components/IconButton';
import {CameraContext} from '../../contexts/CameraContext';
const Header = ({navigation}) => {
  const {selectedOption, options} = useContext(CameraContext);
  return (
    <View style={styles.container}>
      {/* closeButton */}
      <IconButton icon={icons.close} onPress={() => navigation.goBack()} />
      {/* title */}
      <Text style={styles.title}>
        {selectedOption == options.camera ? 'Scan Camera' : 'Scan QR Code'}
      </Text>
      {/* options */}
      <IconButton
        icon={icons.flash}
        iconStyle={styles.rightIcons}
        onPress={() => navigation.goBack()}
      />
      <IconButton
        icon={icons.question_mark}
        iconStyle={styles.rightIcons}
        onPress={() => navigation.goBack()}
      />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: SIZES.padding * 2,
    paddingBottom: SIZES.radius,
    paddingHorizontal: SIZES.padding,
    alignItems: 'center',
    backgroundColor: COLORS.light,
    zIndex: 1,
    gap: SIZES.base,
  },
  title: {
    flex: 1,
    marginLeft: SIZES.radius,
    ...FONTS.h2,
  },
  rightIcons: {width: 25, height: 25},
});
