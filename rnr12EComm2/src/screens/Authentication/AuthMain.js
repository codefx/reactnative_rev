import React, {useContext, useEffect} from 'react';
import {View, Image, StyleSheet} from 'react-native';

import images from '../../constants/images';
import {AuthContext} from '../../contexts/AuthContext';

import {COLORS, SIZES} from '../../theme/theme';
import strConsts from '../../constants/stringConstants';
import SignIn from './SignIn';
import SignUp from './SignUp';
import AuthFooter from './AuthFooter';
import SocialLogins from './SocialLogins';

const AuthMain = ({navigation}) => {
  const {mode, setMode, animationState} = useContext(AuthContext);
  useEffect(() => {
    animationState.transitionTo(strConsts.SIGN_IN);
  }, []);

  return (
    <View style={styles.container}>
      {/* logo */}
      <Image source={images.logo} style={styles.logo} />
      {/* SignIn -- SıgnUp */}
      <View style={{zIndex: 1}}>
        {mode === strConsts.SIGN_IN ? (
          <SignIn navigation={navigation} />
        ) : (
          <SignUp />
        )}
      </View>
      <AuthFooter />
      {mode === strConsts.SIGN_IN && <SocialLogins />}
    </View>
  );
};

export default AuthMain;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.padding,
    backgroundColor: COLORS.lightGrey,
  },
  logo: {
    alignSelf: 'center',
    marginTop: SIZES.padding * 2,
    width: 52,
    height: 50,
  },
});
