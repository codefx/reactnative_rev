import React, {useContext, useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {MotiView} from 'moti';
import {AuthContext} from '../../contexts/AuthContext';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import {Shadow} from 'react-native-shadow-2';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FormInput from '../../components/FormInput';
import icons from '../../constants/icons';
import IconButton from '../../components/IconButton';
import TextButton from '../../components/TextButton';

const SignIn = ({navigation}) => {
  const {animationState, email, setEmail, password, setPassword} =
    useContext(AuthContext);
  const [isVisible, setIsVisible] = useState(false);
  return (
    <MotiView state={animationState} style={styles.motiView}>
      <Shadow>
        <View style={styles.authContainer}>
          <Text style={styles.title}>Sign in to continue </Text>
          <KeyboardAwareScrollView
            enableOnAndroid={true}
            keyboardDismissMode="on-drag"
            keyboardShouldPersistTaps={'handled'}
            extraScrollHeight={-300}
            contentContainerStyle={styles.keyboardCont}>
            {/* email */}
            <FormInput
              containerStyle={styles.finputContainer}
              placeholder="Email"
              value={email}
              onChange={text => setEmail(text)}
              prependComponent={
                <Image source={icons.email} style={styles.finputImage} />
              }
            />
            {/* password */}
            <FormInput
              containerStyle={styles.finputContainer}
              placeholder="Password"
              value={password}
              secureTextEntry={!isVisible}
              onChange={text => setPassword(text)}
              prependComponent={
                <Image source={icons.lock} style={styles.finputImage} />
              }
              appendComponent={
                <IconButton
                  icon={isVisible ? icons.eye_off : icons.eye}
                  iconStyle={{tintColor: COLORS.grey}}
                  onPress={() => setIsVisible(!isVisible)}
                />
              }
            />
            <View style={styles.forgotPas.view}>
              <TextButton
                label="Forgot Password?"
                contentContainerStyle={styles.forgotPas.button}
                labelStyle={{color: COLORS.support3, ...FONTS.h4}}
              />
            </View>
          </KeyboardAwareScrollView>
          {/* loginbutton */}
          <TextButton
            label="Log In"
            contentContainerStyle={styles.loginButton}
            labelStyle={{...FONTS.h3}}
            onPress={() => navigation.navigate('Home')}
          />
        </View>
      </Shadow>
    </MotiView>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  motiView: {
    marginTop: SIZES.padding,
    height: SIZES.height * 0.57,
  },
  authContainer: {
    flex: 1,
    width: SIZES.width - SIZES.padding * 2,
    padding: SIZES.padding,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.light,
    zIndex: 1,
  },
  title: {
    width: '60%',
    color: COLORS.dark,
    ...FONTS.h1,
  },
  keyboardCont: {
    flexGrow: 1,
    justifyContent: 'center',
    gap: SIZES.radius,
  },
  finputContainer: {
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.error,
  },
  finputImage: {
    width: 25,
    height: 25,
    marginRight: SIZES.base,
  },
  forgotPas: {
    view: {alignItems: 'flex-end'},
    button: {
      marginTop: SIZES.radius,
      backgroundColor: null,
    },
  },
  loginButton: {
    height: 55,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary,
  },
});
