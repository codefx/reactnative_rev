import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import TextButton from '../../components/TextButton';

const Home = ({navigation}) => {
  return (
    <View style={styles.main}>
      <TextButton
        label="scan"
        onPress={() => navigation.navigate('ScanProduct')}
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  main: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});
