import React, {useContext} from 'react';
import {View, Animated, StyleSheet, Text} from 'react-native';

import {COLORS, FONTS, SIZES} from '../../theme/theme';
import walkthrough from '../../constants/walkthrough';
import Footer from './Footer';
import {WalktroughContext} from '../../contexts/WalktroughContext';
import Dots from './Dots';
import SlidesFirst from './SlidesFirst';
import SlidesSecond from './SlidesSecond';

const Walkthrough = ({navigation}) => {
  const {scrollX, onViewChangeRef} = useContext(WalktroughContext);

  const rendrItem = ({item, index}) => {
    return (
      <View style={styles.renderItemMainView}>
        {/* Walkthrough Images */}
        <View style={styles.renderItemWalkImagesContainer}>
          {index == 0 && <SlidesFirst />}
          {index == 1 && <SlidesSecond />}
        </View>
        {/* Title & Descriptions */}
        <View style={styles.renderItemTitleView}>
          <Text style={styles.renderItemTitleBig}>{item.title}</Text>
          <Text style={styles.renderItemSubtitle}>{item.sub_title}</Text>
        </View>
      </View>
    );
  };
  return (
    <View style={styles.mainView}>
      <Animated.FlatList
        data={walkthrough}
        keyExtractor={item => item.id}
        horizontal
        snapToInterval={SIZES.width}
        decelerationRate="fast"
        showsHorizontalScrollIndicator={false}
        scrollEventThrottle={16}
        onViewableItemsChanged={onViewChangeRef.current}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {
            useNativeDriver: false,
          },
        )}
        renderItem={rendrItem}
      />
      {/* Dots */}
      <Dots />
      {/* buttons /footer */}
      <Footer navigation={navigation} />
    </View>
  );
};

export default Walkthrough;

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: COLORS.light,
  },
  renderItemMainView: {
    width: SIZES.width,
    justifyContent: 'center',
  },
  renderItemWalkImagesContainer: {flex: 1, justifyContent: 'center'},
  renderItemTitleView: {
    height: SIZES.height * 0.35,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: SIZES.padding,
  },
  renderItemTitleBig: {...FONTS.h1},
  renderItemSubtitle: {
    marginTop: SIZES.radius,
    textAlign: 'center',
    ...FONTS.body3,
    color: COLORS.grey,
  },
});
