import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

import TextButton from '../../components/TextButton';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import images from '../../constants/images';
import routeNames from '../../navigations/routeNames';

const Welcome = ({navigation}) => {
  return (
    <View
      style={styles.container}>
      {/* Logo & Title */}
      <View
        style={styles.header.Container}>
        <Image
          source={images.logo}
          style={styles.header.logo}
        />
        <Text style={styles.header.titleBig}>Welcome to</Text>
        <Text style={styles.header.titleText}>Margaret</Text>
      </View>
      {/* Footer Buttons */}
      <View
        style={styles.footer.container}>
        <TextButton
          contentContainerStyle={styles.footer.tBut1ContStyle}
          label="Get Started"
          onPress={() => navigation.navigate(routeNames.Walkthrough)}
        />

        <TextButton
          contentContainerStyle={styles.footer.tBut2ContStyle}
          label="Already have an account"
          labelStyle={{color: COLORS.primary,}}
          //onPress
        />
      </View>
    </View>
  );
};

export default Welcome;
const styles=StyleSheet.create({
container:{
        flex: 1,
        backgroundColor: COLORS.light,
      },
      header:{
        container:{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        },
        logo: {
            width: 150,
            height: 150,
          },
          titleBig:{marginTop: SIZES.padding, ...FONTS.h1},
          titleText:{marginTop: SIZES.base, ...FONTS.h1}

      },
    footer:{
      container: {
          paddingHorizontal: SIZES.padding,
          marginBottom: 30,
        },
        tBut1ContStyle:{
            height: 50,
            borderRadius: SIZES.radius,
          },
          tBut2ContStyle:{
            height: 50,
            marginTop: SIZES.base,
            backgroundColor: null,
          },

    }
})
