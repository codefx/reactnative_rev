import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Home from '../screens/Home/Home'
import Welcome from '../screens/Walkthrough/Welcome'
import Walkthrough from '../screens/Walkthrough/Walkthrough'
import AuthMain from '../screens/Authentication/AuthMain'
import ScanProduct from '../screens/ScanProduct/ScanProduct'
import routeNames from './routeNames';
import WalktroughCtxProvider from '../contexts/WalktroughContext';
import AuthCtxProvider from '../contexts/AuthContext';


const Stack = createStackNavigator();
const Routes = () => {
    return (
        <NavigationContainer>
      <AuthCtxProvider>
        <WalktroughCtxProvider>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
            initialRouteName={routeNames.ScanProduct}>
            <Stack.Group>
              <Stack.Screen name={routeNames.Welcome} component={Welcome} />
              <Stack.Screen
                name={routeNames.Walkthrough}
                component={Walkthrough}
              />
            </Stack.Group>
            <Stack.Group>
              <Stack.Screen name={routeNames.AuthMain} component={AuthMain} />
              <Stack.Screen name={routeNames.Home} component={Home} />
              <Stack.Screen
                name={routeNames.ScanProduct}
                component={ScanProduct}
              />
            </Stack.Group>
          </Stack.Navigator>
        </WalktroughCtxProvider>
      </AuthCtxProvider>
    </NavigationContainer>
    );
};

export default Routes;
