const Home = 'Home';
const SignUp = 'SignUpScreen';
const Scan = 'ScanScreen';
const Tabs = 'Tabs';
const User = 'User';
const ScanProduct = 'ScanProduct';
const Welcome = 'Welcome';
const Walkthrough = 'Walkthrough';
const AuthMain = 'AuthMain';

const routeNames = {
  Home,
  SignUp,
  Scan,
  Tabs,
  User,
  ScanProduct,
  Walkthrough,
  Welcome,
  AuthMain,
};
export default routeNames;
