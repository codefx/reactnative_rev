const walkthrough_01_01_images = [
  require('../assets/images/walkthrough/walkthrough_01_01.png'),
  require('../assets/images/walkthrough/walkthrough_01_02.png'),
  require('../assets/images/walkthrough/walkthrough_01_03.png'),
  require('../assets/images/walkthrough/walkthrough_01_04.png'),
];

const walkthrough_01_02_images = [
  require('../assets/images/walkthrough/walkthrough_01_05.png'),
  require('../assets/images/walkthrough/walkthrough_01_06.png'),
  require('../assets/images/walkthrough/walkthrough_01_07.png'),
  require('../assets/images/walkthrough/walkthrough_01_01.png'),
];

export {walkthrough_01_01_images, walkthrough_01_02_images};
