const SIGN_IN = 'signIn';
const SIGN_UP = 'signUp';

const stringConstants = {SIGN_IN, SIGN_UP};
export default stringConstants;
