import 'react-native-reanimated';
import React from 'react';
import 'react-native-gesture-handler';
import Routes from './src/navigations/Routes';

const App = () => {
  return <Routes />;
};
export default App;
