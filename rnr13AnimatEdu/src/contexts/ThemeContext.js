import {View, Text} from 'react-native';
import React, {createContext, useState} from 'react';
import {lightTheme, darkTheme} from '../theme/appTheme';

export const ThemeContext = createContext({});
const ThemeCtxProvider = ({children}) => {
  const light = 'light';
  const dark = 'dark';
  const [appThemeName, setAppThemeName] = useState(light);
  const [currTheme, setCurrTheme] = useState(lightTheme);

  const themeSelector = currTheme => {
    const selectedTheme =
      currTheme?.name === lightTheme.name ? darkTheme : lightTheme;
    return selectedTheme;
  };
  const toggleThemeName = () => {
    if (appThemeName == light) {
      setAppThemeName(dark);
      setCurrTheme(() => themeSelector(currTheme));
    } else {
      setAppThemeName(light);
      setCurrTheme(() => themeSelector(currTheme));
    }
  };
  //const {currTheme, toggleThemeName} = useContext(ThemeContext);
  //backgroundColor: currTheme?.backgroundColor1,
  const values = {
    appThemeName,
    setAppThemeName,
    lightTheme,
    darkTheme,
    currTheme,
    setCurrTheme,
    toggleThemeName,
  };
  return (
    <ThemeContext.Provider value={values}>{children}</ThemeContext.Provider>
  );
};

export default ThemeCtxProvider;
