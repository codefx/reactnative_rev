import {View, Text, Animated} from 'react-native';
import React, {
  createContext,
  createRef,
  useCallback,
  useRef,
  useState,
} from 'react';
import constants from '../constants';
import SIZES from '../theme/sizes';

export const MainLayoutContext = createContext({});
const bottom_tabs = constants.bottom_tabs.map(bottom_tab => ({
  ...bottom_tab,
  ref: createRef(),
}));
const MainLayoutCtxProvider = ({children}) => {
  const scrollX = useRef(new Animated.Value(0)).current;
  const flatListRef = useRef();
  const [measureLayout, setMeasureLayout] = useState([]);
  const onBottomTabPress = useCallback(bottomTabIndex => {
    flatListRef?.current?.scrollToOffset({
      offset: bottomTabIndex * SIZES.width,
    });
  }, []);
  const values = {
    scrollX,
    measureLayout,
    setMeasureLayout,
    bottom_tabs,
    flatListRef,
    onBottomTabPress,
  };
  return (
    <MainLayoutContext.Provider value={values}>
      {children}
    </MainLayoutContext.Provider>
  );
};

export default MainLayoutCtxProvider;
