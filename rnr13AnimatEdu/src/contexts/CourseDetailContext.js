import {View, Text, Animated} from 'react-native';
import React, {
  createContext,
  createRef,
  useCallback,
  useRef,
  useState,
} from 'react';
import course_details_tabs from '../constants/course_details_tabs';
import SIZES from '../theme/sizes';

export const CourseDetailContext = createContext({});

const courseDetailsTabs = course_details_tabs.map(course_details_tab => ({
  ...course_details_tab,
  ref: createRef(),
}));
const CourseDetailCtxProvider = ({children}) => {
  const [playVideo, setPlayVideo] = useState(false);
  const [measureLayout, setMeasureLayout] = useState([]);
  const containerRef = useRef();
  const flatlistRef = useRef();
  const scrollX = useRef(new Animated.Value(0)).current;

  const onTabPress = useCallback(tabIndex => {
    flatlistRef?.current?.scrollToOffset({
      offset: tabIndex * SIZES.width,
    });
  }, []);

  const values = {
    measureLayout,
    setMeasureLayout,
    containerRef,
    flatlistRef,
    scrollX,
    courseDetailsTabs,
    playVideo,
    setPlayVideo,
    onTabPress,
  };
  return (
    <CourseDetailContext.Provider value={values}>
      {children}
    </CourseDetailContext.Provider>
  );
};

export default CourseDetailCtxProvider;
