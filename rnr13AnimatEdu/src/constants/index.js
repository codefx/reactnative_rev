import bottom_tabs from './bottom_tabs';
import categories from './categories';
import class_types from './class_types';
import class_levels from './class_levels';
import created_within from './created_within';
import course_details_tabs from './course_details_tabs';
import screenNames from './screenNames';
import walkthrough from './walkthrough';
import register_options from './register_options';

const constants = {
  register_options,
  walkthrough,
  categories,
  screenNames,
  bottom_tabs,
  class_types,
  class_levels,
  created_within,
  course_details_tabs,
};
export default constants;
