import screenNames from './screenNames';

const bottom_tabs = [
  {
    id: 0,
    label: screenNames.home,
    icon: require('../assets/icons/home.png'),
  },
  {
    id: 1,
    label: screenNames.search,
    icon: require('../assets/icons/search.png'),
  },
  {
    id: 2,
    label: screenNames.profile,
    icon: require('../assets/icons/profile.png'),
  },
];
export default bottom_tabs;
