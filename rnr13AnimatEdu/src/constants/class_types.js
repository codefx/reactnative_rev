const class_types = [
  {
    id: 0,
    label: 'All',
    icon: require('../assets/icons/all.png'),
  },
  {
    id: 1,
    label: 'Staff Pick',
    icon: require('../assets/icons/staff_pick.png'),
  },
  {
    id: 2,
    label: 'Original',
    icon: require('../assets/icons/original.png'),
  },
];

export default class_types;
