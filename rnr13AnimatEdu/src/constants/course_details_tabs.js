const course_details_tabs = [
  {
    id: 0,
    label: 'Chapters',
  },
  {
    id: 1,
    label: 'Files',
  },
  {
    id: 2,
    label: 'Discussions',
  },
];
export default course_details_tabs;
