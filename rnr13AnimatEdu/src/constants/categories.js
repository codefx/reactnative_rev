const categories = [
  {
    id: 0,
    label: 'Mobile Design',
    icon: require('../assets/icons/mobile.png'),
  },
  {
    id: 1,
    label: '3D Modeling',
    icon: require('../assets/icons/model_3d.png'),
  },
  {
    id: 2,
    label: 'Web Designing',
    icon: require('../assets/icons/web_design.png'),
  },
  {
    id: 3,
    label: 'Illustrations',
    icon: require('../assets/icons/illustration.png'),
  },
  {
    id: 4,
    label: 'Drawing',
    icon: require('../assets/icons/drawing.png'),
  },
  {
    id: 5,
    label: 'Animation',
    icon: require('../assets/icons/animation.png'),
  },
  {
    id: 6,
    label: 'Education',
    icon: require('../assets/icons/education.png'),
  },
  {
    id: 7,
    label: 'Networking',
    icon: require('../assets/icons/networking.png'),
  },
  {
    id: 8,
    label: 'Coding',
    icon: require('../assets/icons/coding.png'),
  },
];

export default categories;
