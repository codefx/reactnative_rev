const screenNames = {
  home: 'Home',
  search: 'Search',
  profile: 'Profile',
};

export default screenNames;
