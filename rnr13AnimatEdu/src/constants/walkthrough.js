const walkthrough = [
  {
    id: 0,
    title: 'Explore Online Courses',
    sub_title:
      'All types of educational & professional courses available online.',
    image: require('../assets/images/work.png'),
  },
  {
    id: 1,
    title: 'Explore Online Courses',
    sub_title:
      'All types of educational & professional courses available online.',
    image: require('../assets/images/work.png'),
  },
  {
    id: 2,
    title: 'Explore Online Courses',
    sub_title:
      'All types of educational & professional courses available online.',
    image: require('../assets/images/work.png'),
  },
];
export default walkthrough;
