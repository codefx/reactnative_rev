const class_levels = [
  {
    id: 0,
    label: 'Beginner',
  },
  {
    id: 1,
    label: 'Intermediate',
  },
  {
    id: 2,
    label: 'Advanced',
  },
];
export default class_levels;
