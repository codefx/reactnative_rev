import {StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';

import SIZES from '../../../theme/sizes';
import {Shadow} from 'react-native-shadow-2';
import COLORS from '../../../theme/colors';
import Tabs from '../../../components/tabs/Tabs';
import {ThemeContext} from '../../../contexts/ThemeContext';

const BottomTabs = () => {
  const {currTheme} = useContext(ThemeContext);
  return (
    <View
      style={{
        ...styles.mainContainer,
        backgroundColor: currTheme?.backgroundColor1,
      }}>
      <Shadow style={styles.shadowStyle}>
        <View
          style={{
            ...styles.tabContainer,
            backgroundColor: currTheme?.backgroundColor2,
          }}>
          <Tabs />
        </View>
      </Shadow>
    </View>
  );
};

export default BottomTabs;

const styles = StyleSheet.create({
  mainContainer: {
    paddingBottom: SIZES.height > 800 ? 20 : 10,
    paddingHorizontal: SIZES.padding,
    paddingVertical: SIZES.radius,
  },
  shadowStyle: {
    width: SIZES.width - SIZES.padding * 2,
    height: 85,
  },
  tabContainer: {
    flex: 1,
    borderRadius: SIZES.radius,
    //backgroundColor: COLORS.primary3,
  },
});
