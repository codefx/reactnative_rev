import {Animated, StyleSheet, Text, View} from 'react-native';
import React, {useContext, useRef} from 'react';

import SIZES from '../../../theme/sizes';
import constants from '../../../constants';
import Home from '../Home/Home';
import Search from '../Search/Search';
import Profile from '../Profile/Profile';
import {MainLayoutContext} from '../../../contexts/MainLayoutContext';

const MainContents = () => {
  const {scrollX, flatListRef} = useContext(MainLayoutContext);

  return (
    <View style={styles.main}>
      <Animated.FlatList
        ref={flatListRef}
        horizontal
        scrollEnabled={false}
        pagingEnabled
        snapToALlignment="center"
        snapToiInterval={SIZES.width}
        decelerationRate="fast"
        showsHorizontalScrollIndicator={false}
        data={constants.bottom_tabs}
        keyExtractor={item => `Main-${item.id}`}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {useNativeDriver: false},
        )}
        renderItem={({item, index}) => <RenderItem item={item} index={index} />}
      />
    </View>
  );
};
const RenderItem = ({item, index}) => {
  return (
    <View style={styles.renderItem}>
      {item.label == constants.screenNames.home && <Home />}
      {item.label == constants.screenNames.search && <Search />}
      {item.label == constants.screenNames.profile && <Profile />}
    </View>
  );
};

export default MainContents;

const styles = StyleSheet.create({
  main: {flex: 1},
  renderItem: {height: SIZES.height, width: SIZES.width},
});
