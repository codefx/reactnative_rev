import React from 'react';
import {View, StyleSheet} from 'react-native';

import COLORS from '../../../theme/colors';

import MainContents from './MainContents';
import BottomTabs from './BottomTabs';
import MainLayoutCtxProvider from '../../../contexts/MainLayoutContext';

const MainLayout = () => {
  return (
    <MainLayoutCtxProvider>
      <View style={styles.mainContainer}>
        {/* Content */}
        <MainContents />
        {/* Bottom tab */}
        <BottomTabs />
      </View>
    </MainLayoutCtxProvider>
  );
};

export default MainLayout;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
});
