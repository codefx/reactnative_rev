import {Image, StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
} from 'react-native-reanimated';
import SIZES from '../../../theme/sizes';
import {Shadow} from 'react-native-shadow-2';
import COLORS from '../../../theme/colors';
import icons from '../../../assetsConstants/icons';
import FONTS from '../../../theme/fontStyles';

const SearchBar = ({scrollY}) => {
  const inputRange = [0, 55];
  const searchBarAnimatedStyle = useAnimatedStyle(() => {
    return {
      height: interpolate(
        scrollY?.value,
        inputRange,
        [55, 0],
        Extrapolate.CLAMP,
      ),
      opacity: interpolate(
        scrollY.value,
        inputRange,
        [1, 0],
        Extrapolate.CLAMP,
      ),
    };
  });

  return (
    <Animated.View style={[styles.main, searchBarAnimatedStyle]}>
      <Shadow>
        <View style={styles.shadowInner}>
          <Image source={icons.search} style={styles.searchIcon} />
          <TextInput
            style={styles.textInput}
            value=""
            placeholder="Search for Topics,Courses & Educators"
            placeholderTextColor={COLORS.gray20}
          />
        </View>
      </Shadow>
    </Animated.View>
  );
};

export default SearchBar;

const styles = StyleSheet.create({
  main: {
    position: 'absolute',
    top: 50,
    left: 0,
    right: 0,
    paddingHorizontal: SIZES.padding,
    height: 50,
  },
  shadowInner: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    width: SIZES.width - SIZES.padding * 2,
    paddingHorizontal: SIZES.radius,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.white,
  },
  searchIcon: {
    width: 25,
    height: 25,
    tintColor: COLORS.gray40,
  },
  textInput: {
    flex: 1,
    marginLeft: SIZES.base,
    ...FONTS.h4,
  },
});
