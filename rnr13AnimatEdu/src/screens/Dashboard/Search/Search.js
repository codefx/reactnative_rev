import React, {useRef} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Animated, {
  useAnimatedScrollHandler,
  useSharedValue,
} from 'react-native-reanimated';
import COLORS from '../../../theme/colors';
import BrowseCategories from './BrowseCategories';
import SearchBar from './SearchBar';
import TopSearches from './TopSearches';

const Search = () => {
  const scrollViewRef = useRef();
  const scrollY = useSharedValue(0);
  const onScroll = useAnimatedScrollHandler(e => {
    scrollY.value = e.contentOffset.y;
  });
  return (
    <View style={styles.mainContainer}>
      <Animated.ScrollView
        ref={scrollViewRef}
        contentContainerStyle={{marginTop: 100, paddingBottom: 300}}
        showsVerticalScrollIndicator={false}
        scrollEventThrottle={16}
        keyboardDismissMode="on-drag"
        onScroll={onScroll}
        onScrollEndDrag={e => {
          if (
            e.nativeEvent.contentOffset.y > 10 &&
            e.nativeEvent.contentOffset.y < 50
          ) {
            scrollViewRef.current?.scrollTo({x: 0, y: 60, animated: true});
          }
        }}>
        {/* topsearches */}
        <TopSearches />
        {/* browseCategories */}
        <BrowseCategories />
      </Animated.ScrollView>
      <SearchBar scrollY={scrollY} />
    </View>
  );
};

export default Search;
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
});
