import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../../theme/sizes';
import FONTS from '../../../theme/fontStyles';
import categories from '../../../data/categories';
import CategoryCard from '../../../components/CategoryCard';
import {FlatList} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const BrowseCategories = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.main}>
      <Text style={styles.title}>Browse Categories</Text>
      <FlatList
        data={categories}
        numColumns={2}
        scrollEnabled={false}
        listKey="BrowseCategories"
        keyExtractor={item => `BrowseCategories-${item.id}`}
        contentContainerStyle={{marginTop: SIZES.radius}}
        renderItem={({item, index}) => (
          <CategoryCard
            sharedElementPrefix="Search"
            category={item}
            containerStyle={styles.categories}
            onPress={() =>
              navigation.navigate('CourseListing', {
                category: item,
                sharedElementPrefix: 'Search',
              })
            }
          />
        )}
      />
    </View>
  );
};

export default BrowseCategories;

const styles = StyleSheet.create({
  main: {marginTop: SIZES.padding},
  title: {marginHorizontal: SIZES.padding, ...FONTS.h2},
  categories: {
    height: 130,
    width: (SIZES.width - SIZES.padding * 2 - SIZES.radius) / 2,
    marginTop: SIZES.radius,
    marginLeft: SIZES.padding,
  },
});
