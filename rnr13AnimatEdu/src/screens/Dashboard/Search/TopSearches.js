import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import FONTS from '../../../theme/fontStyles';
import SIZES from '../../../theme/sizes';
import {FlatList} from 'react-native-gesture-handler';
import top_searches from '../../../data/top_searches';
import TextButton from '../../../components/TextButton';
import COLORS from '../../../theme/colors';

const TopSearches = () => {
  return (
    <View style={{marginTop: SIZES.padding}}>
      <Text style={{marginHorizontal: SIZES.padding, ...FONTS.h2}}>
        Top Searches
      </Text>
      <FlatList
        horizontal
        data={top_searches}
        listKey="TopSearches"
        keyExtractor={item => `TopSearches—${item.id}`}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{marginTop: SIZES.radius}}
        renderItem={({item, index}) => (
          <TextButton
            label={item.label}
            contentContainerStyle={styles.contCtnr}
            labelStyle={styles.label}
          />
        )}
      />
    </View>
  );
};

export default TopSearches;

const styles = StyleSheet.create({
  contCtnr: {
    paddingVertical: SIZES.radius,
    paddingHorizontal: SIZES.padding,
    marginLeft: SIZES.radius,
    marginRight: 5,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.gray10,
  },
  label: {
    color: COLORS.gray50,
    ...FONTS.h3,
  },
});
