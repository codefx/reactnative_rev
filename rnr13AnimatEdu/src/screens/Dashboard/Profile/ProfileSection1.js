import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import ProfileValue from '../../../components/ProfileValue';
import icons from '../../../assetsConstants/icons';
import LineDivider from '../../../components/LineDivider';

const ProfileSection1 = ({prostyle}) => {
  return (
    <View style={prostyle}>
      <ProfileValue icon={icons.profile} label="Name" value={'Doctor Who'} />
      <LineDivider />
      <ProfileValue
        icon={icons.email}
        label="E-mail"
        value={'doctor@gmail.com'}
      />
      <LineDivider />
      <ProfileValue
        icon={icons.password}
        label="Password"
        value={'*********'}
      />
      <LineDivider />
      <ProfileValue
        icon={icons.call}
        label="Contact Number"
        value={'055512345678'}
      />
    </View>
  );
};

export default ProfileSection1;

const styles = StyleSheet.create({});
