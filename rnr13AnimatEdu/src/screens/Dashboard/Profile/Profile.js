import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import COLORS from '../../../theme/colors';
import Header from './Header';
import SIZES from '../../../theme/sizes';
import ProfileCard from './ProfileCard';
import ProfileSection1 from './ProfileSection1';
import ProfileSection2 from './ProfileSection2';
import {ThemeContext} from '../../../contexts/ThemeContext';

const Profile = () => {
  const {currTheme} = useContext(ThemeContext);
  return (
    <View
      style={{
        ...styles.mainContainer,
        backgroundColor: currTheme?.backgroundColor1,
      }}>
      <Header />
      <ScrollView contentContainerStyle={styles.scroll}>
        {/* profile card */}
        <ProfileCard />

        {/* ProfileSection 1 */}
        <ProfileSection1 prostyle={styles.profileSectionContainer} />
        {/* ProfileSection 2 */}
        <ProfileSection2 prostyle={styles.profileSectionContainer} />
      </ScrollView>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    //backgroundColor: COLORS.white,
  },
  scroll: {
    paddingHorizontal: SIZES.padding,
    paddingBottom: 150,
  },
  profileSectionContainer: {
    marginTop: SIZES.padding,
    paddingHorizontal: SIZES.padding,
    borderWidth: 1,
    borderRadius: SIZES.radius,
    borderColor: COLORS.gray20,
  },
});
