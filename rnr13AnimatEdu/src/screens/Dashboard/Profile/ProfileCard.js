import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import SIZES from '../../../theme/sizes';
import COLORS from '../../../theme/colors';
import images from '../../../assetsConstants/images';
import icons from '../../../assetsConstants/icons';
import FONTS from '../../../theme/fontStyles';
import ProgressBar from '../../../components/ProgressBar';
import TextButton from '../../../components/TextButton';
import {ThemeContext} from '../../../contexts/ThemeContext';

const ProfileCard = () => {
  const {currTheme} = useContext(ThemeContext);
  return (
    <View
      style={{...styles.main, backgroundColor: currTheme?.backgroundColor2}}>
      {/* profileImage */}
      <Pressable style={styles.pressable}>
        <Image
          source={images.profile}
          style={styles.image}
          resizeMode="center"
        />
        <View style={styles.cameraIconContainer}>
          <View style={styles.cameraIcon}>
            <Image
              source={icons.camera}
              resizeMode="contain"
              style={styles.camera}
            />
          </View>
        </View>
      </Pressable>
      {/* detail */}

      <View style={styles.detail}>
        <Text style={styles.detailText}>By Doctor Who</Text>
        <Text style={styles.detailDesc}>All Around Universe Traveller</Text>
        <ProgressBar
          progress={'62%'}
          containerStyle={{marginTop: SIZES.radius}}
        />
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.progtext}>Overall Progress</Text>
          <Text style={styles.progPercent}>%62</Text>
        </View>
        <TextButton
          label="+ Become Member"
          contentContainerStyle={{
            ...styles.txtBtn,
            backgroundColor: currTheme?.backgroundColor4,
          }}
          labelStyle={{color: currTheme?.textColor2}}
        />
      </View>
    </View>
  );
};

export default ProfileCard;

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    marginTop: SIZES.padding,
    paddingHorizontal: SIZES.radius,
    paddingVertical: 20,
    borderRadius: SIZES.radius,
    //backgroundColor: COLORS.primary3,
  },
  pressable: {
    width: 80,
    height: 80,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 40,
    borderWidth: 1,
    borderColor: COLORS.white,
  },
  cameraIconContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  cameraIcon: {
    width: 30,
    height: 30,
    marginBottom: -15,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
    backgroundColor: COLORS.primary,
  },
  camera: {width: 17, height: 17},
  detail: {
    flex: 1,
    marginLeft: SIZES.radius,
    alignItems: 'flex-start',
  },
  detailText: {color: COLORS.white, ...FONTS.h2},
  detailDesc: {color: COLORS.white, ...FONTS.body4},
  progtext: {flex: 1, color: COLORS.white, ...FONTS.body4},
  progPercent: {color: COLORS.white, ...FONTS.body4},
  txtBtn: {
    height: 35,
    marginTop: SIZES.padding,
    paddingHorizontal: SIZES.radius,
    borderRadius: 20,
    backgroundColor: COLORS.white,
  },
});
