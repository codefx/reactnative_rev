import {StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import SIZES from '../../../theme/sizes';
import FONTS from '../../../theme/fontStyles';
import IconButton from '../../../components/IconButton';
import icons from '../../../assetsConstants/icons';
import COLORS from '../../../theme/colors';
import {ThemeContext} from '../../../contexts/ThemeContext';

const Header = () => {
  const {currTheme, toggleThemeName} = useContext(ThemeContext);
  return (
    <View style={styles.main}>
      <Text style={{...styles.text, color: currTheme?.textColor}}>Profile</Text>
      <IconButton
        icon={icons.sun}
        iconStyle={{tintColor: currTheme?.tintColor}}
        onPress={() => toggleThemeName()}
      />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    marginTop: 30,
    paddingHorizontal: SIZES.padding,
    justifyContent: 'space-between',
  },
  text: {...FONTS.body1},
  icon: {
    tintColor: COLORS.black,
  },
});
