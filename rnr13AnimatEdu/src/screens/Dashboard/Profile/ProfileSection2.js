import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import ProfileValue from '../../../components/ProfileValue';
import icons from '../../../assetsConstants/icons';
import LineDivider from '../../../components/LineDivider';
import ProfileRadioButton from '../../../components/ProfileRadioButton';

const ProfileSection2 = ({prostyle}) => {
  const [newCourseNotification, setNewCourseNotification] = useState(false);
  const [studyReminder, setStudyReminder] = useState(false);
  return (
    <View style={prostyle}>
      <ProfileValue icon={icons.star_1} value={'Pages'} />
      <LineDivider />
      <ProfileRadioButton
        icon={icons.new_icon}
        label="New Course Notification"
        isSelected={newCourseNotification}
        onPress={() => setNewCourseNotification(!newCourseNotification)}
      />
      <LineDivider />
      <ProfileRadioButton
        icon={icons.reminder}
        label="Study Reminder"
        isSelected={studyReminder}
        onPress={() => setStudyReminder(!studyReminder)}
      />
    </View>
  );
};

export default ProfileSection2;

const styles = StyleSheet.create({});
