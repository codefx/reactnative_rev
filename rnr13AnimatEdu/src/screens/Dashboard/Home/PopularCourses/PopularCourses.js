import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {FlatList} from 'react-native-gesture-handler';
import HeadSection from '../../../../components/HeadSection';
import courses_list_2 from '../../../../data/courses_list_2';
import SIZES from '../../../../theme/sizes';
import HorizontalCourseCard from '../../../../components/HorizontalCourseCard';
import LineDivider from '../../../../components/LineDivider';
import COLORS from '../../../../theme/colors';

const PopularCourses = () => {
  return (
    <HeadSection title={'Popular Courses'} containerStyle={styles.head}>
      <FlatList
        data={courses_list_2}
        listKey="PopularCourses"
        scrollEnabled={false}
        keyExtractor={item => `PopularCourses-${item.id}`}
        contentContainerStyle={{
          marginTop: SIZES.radius,
          paddingHorizontal: SIZES.padding,
        }}
        renderItem={({item, index}) => (
          <HorizontalCourseCard
            course={item}
            containerStyle={styles.renderItem}
          />
        )}
        ItemSeparatorComponent={() => (
          <LineDivider lineStyle={{backgroundColor: COLORS.gray20}} />
        )}
      />
    </HeadSection>
  );
};

export default PopularCourses;

const styles = StyleSheet.create({
  head: {marginTop: 30},
  renderItem: {
    marginVertical: SIZES.padding / 3,
    marginTop: SIZES.padding / 3,
  },
});
