import {Image, ImageBackground, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import images from '../../../assetsConstants/images';
import SIZES from '../../../theme/sizes';
import COLORS from '../../../theme/colors';
import FONTS from '../../../theme/fontStyles';
import TextButton from '../../../components/TextButton';

const StartLearning = () => {
  return (
    <ImageBackground
      source={images.featured_bg_image}
      style={styles.imgBgrnd}
      imageStyle={styles.imageStyle}>
      {/* info */}
      <View>
        <Text style={styles.infoText1}> HOW TO</Text>
        <Text style={styles.infoText2}>
          Make your brand more visible with our checklist
        </Text>
        <Text style={styles.infoText3}> By Doctor Who</Text>
      </View>
      {/* image */}
      <Image source={images.start_learning} style={styles.imageStartlearning} />
      {/* button */}
      <TextButton
        label="Start Learning"
        contentContainerStyle={styles.txtCtntCont}
        labelStyle={styles.labelStyle}
      />
    </ImageBackground>
  );
};

export default StartLearning;

const styles = StyleSheet.create({
  imgBgrnd: {
    alignItems: 'flex-start',
    marginTop: SIZES.padding / 2,
    marginHorizontal: SIZES.padding,
    padding: 10,
  },
  imageStyle: {
    borderRadius: SIZES.radius,
  },
  infoText1: {
    color: COLORS.white,
    ...FONTS.body2,
  },
  infoText2: {color: COLORS.white, ...FONTS.h2},
  infoText3: {
    marginTop: SIZES.radius,
    color: COLORS.white,
    ...FONTS.body4,
  },
  imageStartlearning: {
    width: '100%',
    height: 110,
    //marginTop: SIZES.padding,
  },
  txtCtntCont: {
    height: 40,
    paddingHorizontal: SIZES.padding,
    borderRadius: 20,
    backgroundColor: COLORS.white,
  },
  labelStyle: {color: COLORS.black},
});
