import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import COLORS from '../../../theme/colors';
import ContentSection from './ContentSection';
import Header from './Header';

const Home = () => {
  return (
    <View style={styles.mainContainer}>
      {/* header */}
      <Header />
      {/* content */}
      <ContentSection />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
});
