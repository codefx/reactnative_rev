import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import HeadSection from '../../../../components/HeadSection';
import {FlatList} from 'react-native-gesture-handler';
import categories from '../../../../data/categories';
import CategoryCard from '../../../../components/CategoryCard';
import SIZES from '../../../../theme/sizes';
import {useNavigation} from '@react-navigation/native';

const Categories = () => {
  const navigation = useNavigation();
  return (
    <HeadSection title={'Categories'}>
      <FlatList
        horizontal
        data={categories}
        listKey="Categories"
        keyExtractor={item => `Categories—${item.id}`}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{marginTop: SIZES.radius}}
        renderItem={({item, index}) => (
          <CategoryCard
            sharedElementPrefix="Home"
            category={item}
            containerStyle={styles.renderItem}
            onPress={() =>
              navigation.navigate('CourseListing', {
                category: item,
                sharedElementPrefix: 'Home',
              })
            }
          />
        )}
      />
    </HeadSection>
  );
};

export default Categories;

const styles = StyleSheet.create({
  renderItem: {
    marginLeft: SIZES.base,
    marginRight: SIZES.base,
  },
});
