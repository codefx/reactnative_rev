import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

import SIZES from '../../../theme/sizes';
import FONTS from '../../../theme/fontStyles';
import COLORS from '../../../theme/colors';
import {formatDate} from '../../../utils/dateAndTime';
import IconButton from '../../../components/IconButton';
import icons from '../../../assetsConstants/icons';
// new Date().toLocaleTimeString()
// new Date().toDateString();
const Header = () => {
  return (
    <View style={styles.main}>
      {/* greetings */}
      <View style={styles.textContainer}>
        <Text style={styles.bigText}>Hello, Cfx!</Text>
        <Text style={styles.dateText}>{formatDate(new Date())}</Text>
      </View>
      {/* notification */}
      <IconButton
        icon={icons.notification}
        iconStyle={{tintColor: COLORS.black}}
      />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    marginTop: 15,
    marginBottom: 5,
    paddingHorizontal: SIZES.padding,
    alignItems: 'center',
  },
  textContainer: {flex: 1},
  bigText: {...FONTS.h2},
  dateText: {color: COLORS.gray50, ...FONTS.body3},
});
