import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import StartLearning from './StartLearning';
import CoursesSection from './CoursesSection';
import LineDivider from '../../../components/LineDivider';
import SIZES from '../../../theme/sizes';
import Categories from './Categories/Categories';
import PopularCourses from './PopularCourses/PopularCourses';

const ContentSection = () => {
  return (
    <ScrollView
      contentContainerStyle={styles.main}
      showsVerticalScrollIndicator={false}>
      {/* start learning */}
      <StartLearning />
      {/* Courses */}
      <CoursesSection />
      <LineDivider lineStyle={{marginVertical: SIZES.padding / 2}} />
      {/* Categories */}
      <Categories />
      {/* Popularcourses */}
      <PopularCourses />
    </ScrollView>
  );
};

export default ContentSection;

const styles = StyleSheet.create({
  main: {
    paddingBottom: 130,
  },
});
