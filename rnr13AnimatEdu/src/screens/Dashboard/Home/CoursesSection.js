import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../../theme/sizes';
import courses_list_1 from '../../../data/courses_list_1';
import VerticalCourseCard from '../../../components/VerticalCourseCard';
import {FlatList} from 'react-native-gesture-handler';

const CoursesSection = () => {
  return (
    <FlatList
      horizontal
      data={courses_list_1}
      listKey="Courses"
      keyExtractor={item => `Courses—${item.id}`}
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{
        marginTop: SIZES.padding,
      }}
      renderItem={({item, index}) => (
        <VerticalCourseCard containerStyle={styles.renderItem} course={item} />
      )}
    />
  );
};

export default CoursesSection;

const styles = StyleSheet.create({
  //flatlist: {},
  renderItem: {marginLeft: SIZES.padding, marginRight: 5},
});
