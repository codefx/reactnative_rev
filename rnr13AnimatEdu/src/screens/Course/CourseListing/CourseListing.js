import {StyleSheet, Text, View} from 'react-native';
import React, {useRef} from 'react';
import COLORS from '../../../theme/colors';
import Header from './Header';
import {
  useAnimatedScrollHandler,
  useSharedValue,
  withDelay,
  withTiming,
} from 'react-native-reanimated';
import ResultsSection from './ResultsSection';
import FilterModal from '../../../components/filterModal/FilterModal';
import SIZES from '../../../theme/sizes';

const CourseListing = ({navigation, route}) => {
  const {category, sharedElementPrefix} = route.params;
  const headerSharedValue = useSharedValue(80);
  const filterModalSharedValue1 = useSharedValue(SIZES.height);
  const filterModalSharedValue2 = useSharedValue(SIZES.height);
  const flatListRef = useRef();
  const scrollY = useSharedValue(0);
  const onScroll = useAnimatedScrollHandler(event => {
    scrollY.value = event.contentOffset.y;
  });
  const filterOnPress = () => {
    filterModalSharedValue1.value = withTiming(0, {
      duration: 100,
    });
    filterModalSharedValue2.value = withDelay(
      100,
      withTiming(0, {
        duration: 500,
      }),
    );
  };
  return (
    <View style={styles.mainContainer}>
      <ResultsSection
        flatRef={flatListRef}
        onScroll={onScroll}
        filterOnPress={filterOnPress}
      />
      {/* header */}
      <Header
        shel={sharedElementPrefix}
        cat={category}
        nav={navigation}
        heSharedVal={headerSharedValue}
        scrlY={scrollY}
      />
      <FilterModal
        filterModalSharedValue1={filterModalSharedValue1}
        filterModalSharedValue2={filterModalSharedValue2}
      />
    </View>
  );
};

CourseListing.sharedElements = (route, otherRoute, showing) => {
  if (otherRoute.name === 'Dashboard') {
    const {category, sharedElementPrefix} = route.params;
    return [
      {
        id: `${sharedElementPrefix}-CategoryCard-Bg-${category?.id}`,
      },
      {
        id: `${sharedElementPrefix}-CategoryCard-Title-${category?.id}`,
      },
    ];
  }
};
export default CourseListing;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
});
