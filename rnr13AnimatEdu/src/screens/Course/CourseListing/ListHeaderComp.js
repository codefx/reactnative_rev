import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../../theme/sizes';
import FONTS from '../../../theme/fontStyles';
import IconButton from '../../../components/IconButton';
import icons from '../../../assetsConstants/icons';
import COLORS from '../../../theme/colors';

const ListHeaderComp = ({onPress}) => {
  return (
    <View style={styles.main}>
      <Text style={styles.text}>8834 Results</Text>
      <IconButton
        icon={icons.filter}
        iconStyle={styles.icon}
        containerStyle={styles.icoConContainer}
        onPress={onPress}
      />
    </View>
  );
};

export default ListHeaderComp;

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 270,
    marginBottom: SIZES.base,
  },
  text: {
    flex: 1,
    ...FONTS.body3,
  },
  icon: {
    width: 20,
    height: 20,
  },
  icoConContainer: {
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: COLORS.primary,
  },
});
