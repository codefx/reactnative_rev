import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {SharedElement} from 'react-navigation-shared-element';
import COLORS from '../../../theme/colors';
import FONTS from '../../../theme/fontStyles';
import IconButton from '../../../components/IconButton';
import icons from '../../../assetsConstants/icons';
import images from '../../../assetsConstants/images';
import Animated, {
  Extrapolate,
  interpolate,
  runOnJS,
  useAnimatedStyle,
  withDelay,
  withTiming,
} from 'react-native-reanimated';
const HEADER_HEIGHT = 250;
const Header = ({shel, cat, nav, heSharedVal, scrlY}) => {
  const inputRange = [0, HEADER_HEIGHT - 50];

  heSharedVal.value = withDelay(
    500,
    withTiming(0, {
      duration: 500,
    }),
  );
  const headerHeightAnimatedStyle = useAnimatedStyle(() => {
    return {
      height: interpolate(
        scrlY.value,
        inputRange,
        [HEADER_HEIGHT, 120],
        Extrapolate.CLAMP,
      ),
    };
  });
  const headerHideOnScrollAnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: interpolate(scrlY.value, [80, 0], [0, 1], Extrapolate.CLAMP),
      transform: [
        {
          translateY: interpolate(
            scrlY.value,
            inputRange,
            [0, 200],
            Extrapolate.CLAMP,
          ),
        },
      ],
    };
  });
  const headerShowOnScrollAnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: interpolate(scrlY.value, [80, 0], [1, 0], Extrapolate.CLAMP),
      transform: [
        {
          translateY: interpolate(
            scrlY.value,
            inputRange,
            [50, 130],
            Extrapolate.CLAMP,
          ),
        },
      ],
    };
  });
  const headerFadeAnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: interpolate(heSharedVal.value, [80, 0], [0, 1]),
    };
  });
  const headerTranslateAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{translateY: heSharedVal.value}],
    };
  });
  const backHandler = () => {
    nav.goBack();
  };
  return (
    <Animated.View style={[styles.main, headerHeightAnimatedStyle]}>
      <SharedElement
        id={`${shel}-CategoryCard-Bg-${cat?.id}`}
        style={[StyleSheet.absoluteFillObject]}>
        <Image
          source={cat?.thumbnail}
          resizeMode="cover"
          style={styles.image}
        />
      </SharedElement>
      {/* Title */}
      <Animated.View
        style={[styles.fromTopAnimate, headerShowOnScrollAnimatedStyle]}>
        <Text style={styles.fromTopText}>{cat?.title}</Text>
      </Animated.View>
      <Animated.View
        style={[styles.titleMain, headerHideOnScrollAnimatedStyle]}>
        <SharedElement
          id={`${shel}-CategoryCard-Title-${cat?.id}`}
          style={[StyleSheet.absoluteFillObject]}>
          <Text style={styles.title}>{cat?.title}</Text>
        </SharedElement>
      </Animated.View>
      {/* backbutton */}
      <Animated.View style={headerFadeAnimatedStyle}>
        <IconButton
          icon={icons.back}
          iconStyle={{tintColor: COLORS.black}}
          containerStyle={styles.icoButCont}
          onPress={() => {
            //backHandler()
            setTimeout(() => {
              heSharedVal.value = withTiming(
                80,
                {
                  duration: 500,
                },
                () => {
                  runOnJS(backHandler)();
                },
              );
            }, 100);
          }}
        />
      </Animated.View>
      {/* categoryImageSection */}
      <Animated.Image
        source={images.mobile_image}
        resizeMode="contain"
        style={[
          styles.categoryImage,
          headerFadeAnimatedStyle,
          headerTranslateAnimatedStyle,
          headerHideOnScrollAnimatedStyle,
        ]}
      />
    </Animated.View>
  );
};

export default Header;

const styles = StyleSheet.create({
  main: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 250,
    overflow: 'hidden',
  },
  image: {
    height: '100%',
    width: '100%',
    borderBottomLeftRadius: 60,
  },
  titleMain: {position: 'absolute', bottom: 70, left: 30},
  title: {
    position: 'absolute',
    color: COLORS.white,
    ...FONTS.h1,
  },
  icoButCont: {
    position: 'absolute',
    top: 40,
    left: 20,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: COLORS.white,
  },
  categoryImage: {
    position: 'absolute',
    right: 40,
    bottom: -40,
    width: 100,
    height: 200,
  },
  fromTopAnimate: {
    position: 'absolute',
    top: -80,
    left: 0,
    right: 0,
  },
  fromTopText: {textAlign: 'center', color: COLORS.white, ...FONTS.h2},
});
