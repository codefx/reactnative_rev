import {FlatList, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Animated from 'react-native-reanimated';
import courses_list_2 from '../../../data/courses_list_2';
import SIZES from '../../../theme/sizes';
import ListHeaderComp from './ListHeaderComp';
import HorizontalCourseCard from '../../../components/HorizontalCourseCard';
import LineDivider from '../../../components/LineDivider';
import COLORS from '../../../theme/colors';
import {useNavigation} from '@react-navigation/native';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const ResultsSection = ({flatRef, onScroll, filterOnPress}) => {
  const navigation = useNavigation();
  return (
    <AnimatedFlatList
      ref={flatRef}
      data={courses_list_2}
      keyExtractor={item => `Results—${item.id}`}
      contentContainerStyle={styles.flat}
      showsHorizontalScrollIndicator={false}
      scrollEventThrottle={16}
      keyboardDismissMode="on-drag"
      onScroll={onScroll}
      ListHeaderComponent={() => <ListHeaderComp onPress={filterOnPress} />}
      renderItem={({item, index}) => (
        <HorizontalCourseCard
          course={item}
          containerStyle={styles.containerStyle}
          onPress={() =>
            navigation.navigate('CourseDetails', {selectedCourse: item})
          }
        />
      )}
      ItemSeparatorComponent={() => (
        <LineDivider lineStyle={{backgroundColor: COLORS.gray20}} />
      )}
    />
  );
};

export default ResultsSection;

const styles = StyleSheet.create({
  flat: {
    paddingHorizontal: SIZES.padding,
  },
  containerStyle: {
    marginVertical: SIZES.padding / 2,
    marginTop: SIZES.padding / 2,
  },
});
