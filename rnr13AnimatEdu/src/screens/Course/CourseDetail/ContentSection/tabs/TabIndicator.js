import {Animated, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import SIZES from '../../../../../theme/sizes';
import COLORS from '../../../../../theme/colors';
import {CourseDetailContext} from '../../../../../contexts/CourseDetailContext';

const TabIndicator = () => {
  const {measureLayout, scrollX, courseDetailsTabs} =
    useContext(CourseDetailContext);

  const inputRange = courseDetailsTabs.map((_, i) => i * SIZES.width);
  const tabIndicatorWidth = scrollX.interpolate({
    inputRange,
    outputRange: measureLayout.map(measure => measure.width),
  });
  const translateX = scrollX.interpolate({
    inputRange,
    outputRange: measureLayout.map(measure => measure.x),
  });
  return (
    <Animated.View
      style={{
        ...styles.main,
        width: tabIndicatorWidth,
        transform: [{translateX}],
      }}
    />
  );
};

export default TabIndicator;

const styles = StyleSheet.create({
  main: {
    position: 'absolute',
    bottom: 0,
    height: 4,
    // width: 100,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary,
  },
});
