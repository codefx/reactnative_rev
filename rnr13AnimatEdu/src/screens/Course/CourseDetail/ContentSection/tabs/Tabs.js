import {Animated, Pressable, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useContext, useRef} from 'react';

import FONTS from '../../../../../theme/fontStyles';
import SIZES from '../../../../../theme/sizes';
import TabIndicator from './TabIndicator';
import {CourseDetailContext} from '../../../../../contexts/CourseDetailContext';

const Tabs = () => {
  const {
    measureLayout,
    setMeasureLayout,
    containerRef,
    courseDetailsTabs,
    onTabPress,
  } = useContext(CourseDetailContext);

  useEffect(() => {
    let ml = [];
    courseDetailsTabs.forEach(cdt => {
      cdt?.ref?.current?.measureLayout(
        containerRef.current,
        (x, y, width, height) => {
          ml.push({
            x,
            y,
            width,
            height,
          });
          if (ml.length === courseDetailsTabs.length) {
            setMeasureLayout(ml);
          }
        },
      );
    });
  }, [containerRef.current]);

  return (
    <Animated.View ref={containerRef} style={styles.main}>
      {/* tab Indicator */}
      {measureLayout.length > 0 && <TabIndicator />}
      {/* tabs */}
      {courseDetailsTabs.map((item, index) => {
        return (
          <Pressable
            key={`Tab-${index}`}
            ref={item.ref}
            style={styles.pressable}
            onPress={() => onTabPress(index)}>
            <Text style={styles.text}>{item.label}</Text>
          </Pressable>
        );
      })}
    </Animated.View>
  );
};

export default Tabs;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    flexDirection: 'row',
  },
  pressable: {
    flex: 1,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    ...FONTS.h3,
    fontSize: SIZES.height > 800 ? 18 : 17,
  },
});
