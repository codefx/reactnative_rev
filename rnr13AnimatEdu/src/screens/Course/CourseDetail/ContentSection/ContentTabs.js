import {Animated, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Tabs from './tabs/Tabs';

const ContentTabs = () => {
  return (
    <Animated.View style={styles.main}>
      <Tabs />
    </Animated.View>
  );
};

export default ContentTabs;

const styles = StyleSheet.create({
  main: {
    height: 60,
    //backgroundColor: 'red',
  },
});
