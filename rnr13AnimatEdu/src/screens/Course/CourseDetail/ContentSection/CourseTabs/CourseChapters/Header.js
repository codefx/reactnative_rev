import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../../../../../theme/sizes';
import FONTS from '../../../../../../theme/fontStyles';
import course_details from '../../../../../../data/course_details';
import COLORS from '../../../../../../theme/colors';
import IconLabel from '../../../../../../components/IconLabel';
import icons from '../../../../../../assetsConstants/icons';
import images from '../../../../../../assetsConstants/images';
import TextButton from '../../../../../../components/TextButton';

const Header = () => {
  return (
    <View style={styles.main}>
      {/* title */}
      <Text style={{...FONTS.h2}}>{course_details?.title} </Text>
      {/* student duration */}
      <View style={styles.studMain}>
        <Text style={styles.studMainText}>
          {course_details?.number_of_students}
        </Text>
        <IconLabel
          icon={icons.time}
          label={course_details?.duration}
          containerStyle={styles.icon.ctrStyle}
          iconStyle={styles.icon.style}
          labelStyle={styles.icon.label}
        />
      </View>
      {/* instructor */}
      <View style={styles.instructMain}>
        {/* Profile Photo */}
        <Image
          source={images.profile}
          style={styles.profileImage}
          resizeMode="center"
        />
        {/* Name & Title */}
        <View style={styles.nameMain}>
          <Text style={styles.name}>{course_details?.instructor?.name}</Text>
          <Text style={styles.title}>{course_details?.instructor?.title}</Text>
        </View>
        {/*Text Button */}
        <TextButton
          label="Follow +"
          contentContainerStyle={styles.txtBtn.cntnr}
          labelStyle={styles.txtBtn.label}
        />
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  main: {
    marginTop: SIZES.padding,
    paddingHorizontal: SIZES.padding,
  },
  studMain: {flexDirection: 'row', marginTop: SIZES.base},
  studMainText: {color: COLORS.gray30, ...FONTS.body4},
  icon: {
    ctrStyle: {marginLeft: SIZES.radius},
    style: {width: 15, height: 15},
    label: {...FONTS.body4},
  },
  instructMain: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
    alignItems: 'center',
  },
  profileImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#6f96bb',
  },
  nameMain: {
    flex: 1,
    marginLeft: SIZES.base,
    justifyContent: 'center',
  },
  name: {...FONTS.h3, fontSize: 18},
  title: {...FONTS.body3},
  txtBtn: {
    cntnr: {
      width: 80,
      height: 35,
      borderRadius: 20,
    },
    label: {
      ...FONTS.h3,
    },
  },
});
