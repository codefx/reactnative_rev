import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import course_details from '../../../../../../data/course_details';
import COLORS from '../../../../../../theme/colors';
import SIZES from '../../../../../../theme/sizes';
import {Image} from 'react-native';
import icons from '../../../../../../assetsConstants/icons';
import FONTS from '../../../../../../theme/fontStyles';

const Chapter = () => {
  return (
    <View>
      {course_details?.videos.map((item, index) => {
        return (
          <View
            key={`Videos-${index}`}
            style={{
              ...styles.innerMain,
              backgroundColor: item?.is_playing
                ? COLORS.additionalColor11
                : null,
            }}>
            <View style={styles.innerContainer}>
              {/* Icon */}
              <Image
                source={
                  item?.is_complete
                    ? icons.completed
                    : item?.is_playing
                    ? icons.play_1
                    : icons.lock
                }
                style={styles.icon}
              />
              {/* Title & Duration */}
              <View style={styles.titleMain}>
                <Text style={styles.title}>{item?.title}</Text>
                <Text style={styles.duration}>{item?.duration}</Text>
              </View>
              {/* Size & Status */}
              <View style={styles.sizeMain}>
                <Text style={styles.size}>{item?.size}</Text>
                <Image
                  source={
                    item?.is_downloaded ? icons.completed : icons.download
                  }
                  style={{
                    ...styles.status,
                    tintColor: item?.is_lock ? COLORS.additionalColor4 : null,
                  }}
                />
              </View>
            </View>
            {/* Progress Bar */}
            {item?.is_playing && (
              <View style={{...styles.progress, width: item?.progress}} />
            )}
          </View>
        );
      })}
    </View>
  );
};

export default Chapter;

const styles = StyleSheet.create({
  innerMain: {
    alignItems: 'center',
    height: 70,
  },
  innerContainer: {
    flexDirection: 'row',
    paddingHorizontal: SIZES.padding,
    alignItems: 'center',
    height: 70,
  },
  icon: {width: 40, height: 40},
  titleMain: {
    flex: 1,
    marginLeft: SIZES.radius,
  },
  title: {...FONTS.h3},
  duration: {color: COLORS.gray30, ...FONTS.body4},
  sizeMain: {
    flexDirection: 'row',
  },
  size: {
    color: COLORS.gray30,
    ...FONTS.body4,
  },
  status: {marginLeft: SIZES.base, width: 25, height: 25},
  progress: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: 5,
    backgroundColor: COLORS.primary,
  },
});
