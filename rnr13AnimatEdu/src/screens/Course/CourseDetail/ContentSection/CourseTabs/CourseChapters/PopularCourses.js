import {FlatList, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../../../../../theme/sizes';
import FONTS from '../../../../../../theme/fontStyles';
import TextButton from '../../../../../../components/TextButton';
import COLORS from '../../../../../../theme/colors';
import courses_list_2 from '../../../../../../data/courses_list_2';
import HorizontalCourseCard from '../../../../../../components/HorizontalCourseCard';
import LineDivider from '../../../../../../components/LineDivider';

const PopularCourses = () => {
  return (
    <View style={styles.main}>
      {/* Section Header */}
      <View style={styles.sectionHeader}>
        <Text style={styles.headertext}>Popular Courses</Text>
        <TextButton contentContainerStyle={styles.txtBtn} label="See ALL" />
      </View>
      {/* Popular Courses List */}
      <FlatList
        data={courses_list_2}
        listKey="PopularCourses"
        scrollEnabled={false}
        keyExtractor={item => `PopularCourses—${item.id}`}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.flatcontainer}
        renderItem={({item, index}) => (
          <HorizontalCourseCard
            course={item}
            containerStyle={styles.renderItemstyle}
          />
        )}
        ItemSeparatorComponent={LineDivider}
      />
    </View>
  );
};

export default PopularCourses;

const styles = StyleSheet.create({
  main: {marginTop: SIZES.padding},
  sectionHeader: {
    flexDirection: 'row',
    paddingHorizontal: SIZES.padding,
  },
  headertext: {flex: 1, ...FONTS.h2},
  txtBtn: {
    width: 80,
    borderRadius: 30,
    backgroundColor: COLORS.primary,
  },
  flatcontainer: {
    marginTop: SIZES.radius,
    paddingHorizontal: SIZES.padding,
  },
  renderItemstyle: {
    marginVertical: SIZES.padding,
    marginTop: SIZES.padding,
  },
});
