import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Header from './Header';
import LineDivider from '../../../../../../components/LineDivider';
import SIZES from '../../../../../../theme/sizes';
import Chapter from './Chapter';
import PopularCourses from './PopularCourses';

const CourseChapters = () => {
  return (
    <ScrollView>
      {/* Header */}
      <Header />
      {/* Line Divider */}
      <LineDivider lineStyle={styles.lineDivid} />
      {/* Chapters */}
      <Chapter />
      {/* Popular Courses */}
      <PopularCourses />
    </ScrollView>
  );
};

export default CourseChapters;

const styles = StyleSheet.create({
  lineDivid: {height: 1, marginVertical: SIZES.radius},
});
