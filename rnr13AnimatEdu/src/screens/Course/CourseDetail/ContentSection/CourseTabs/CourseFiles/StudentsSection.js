import {StyleSheet, Image, Text, View} from 'react-native';
import React from 'react';
import FONTS from '../../../../../../theme/fontStyles';
import course_details from '../../../../../../data/course_details';
import SIZES from '../../../../../../theme/sizes';
import TextButton from '../../../../../../components/TextButton';
import COLORS from '../../../../../../theme/colors';

const StudentsSection = () => {
  let students = [];
  if (course_details?.students.length > 3) {
    students = course_details?.students.slice(0, 3);
  } else {
    students = course_details?.students;
  }

  return (
    <View>
      {/* title */}
      <Text style={styles.header}>Students</Text>
      {/* students */}
      <View style={styles.studentContainer}>
        {students.map((item, index) => {
          return (
            <View
              key={`Students-${index}`}
              style={{marginLeft: index > 0 ? SIZES.radius : 0}}>
              <Image source={item?.thumbnail} style={styles.image} />
            </View>
          );
        })}

        {course_details?.students.length > 3 && (
          <TextButton
            label="View All"
            labelStyle={styles.txtBtnLabel}
            contentContainerStyle={styles.txtBtnCont}
          />
        )}
      </View>
    </View>
  );
};

export default StudentsSection;

const styles = StyleSheet.create({
  header: {...FONTS.h2, fontSize: 22},
  studentContainer: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
    alignItems: 'center',
  },
  image: {
    width: 80,
    height: 80,
  },
  txtBtnLabel: {
    color: COLORS.primary,
    ...FONTS.h3,
  },
  txtBtnCont: {
    marginLeft: SIZES.base,
    backgroundColor: null,
  },
});
