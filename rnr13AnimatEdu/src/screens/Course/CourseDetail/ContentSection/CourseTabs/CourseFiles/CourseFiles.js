import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../../../../../theme/sizes';
import StudentsSection from './StudentsSection';
import FilesSection from './FilesSection';
const CourseFiles = () => {
  return (
    <ScrollView contentContainerStyle={styles.main}>
      {/* studentSection */}
      <StudentsSection />
      {/* filesSection */}
      <FilesSection />
    </ScrollView>
  );
};

export default CourseFiles;

const styles = StyleSheet.create({
  main: {
    padding: SIZES.padding,
  },
});
