import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../../../../../theme/sizes';
import FONTS from '../../../../../../theme/fontStyles';
import course_details from '../../../../../../data/course_details';
import FilesItem from './FilesItem';

const FilesSection = () => {
  return (
    <View style={styles.main}>
      <Text style={styles.header}>Files</Text>
      {course_details?.files.map((item, index) => {
        return (
          <View key={`Files-${index}`} style={styles.sectCont}>
            <FilesItem item={item} index={index} />
          </View>
        );
      })}
    </View>
  );
};

export default FilesSection;

const styles = StyleSheet.create({
  main: {
    marginTop: SIZES.padding,
  },
  header: {...FONTS.h2, fontSize: 22},
  sectCont: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
  },
});
