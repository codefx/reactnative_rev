import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../../../../../theme/sizes';
import FONTS from '../../../../../../theme/fontStyles';
import COLORS from '../../../../../../theme/colors';
import icons from '../../../../../../assetsConstants/icons';
import IconButton from '../../../../../../components/IconButton';

const FilesItem = ({item, index}) => {
  return (
    <>
      {/* thumbnail */}
      <Image source={item?.thumbnail} style={styles.thumb} />
      {/* Name, author & date */}
      <View style={styles.nameSectionCont}>
        <Text style={styles.nameTxt}>{item?.name}</Text>
        <Text style={styles.authorTxt}>{item?.author}</Text>
        <Text style={styles.upload}>{item?.upload_date}</Text>
      </View>
      {/* menu */}
      <IconButton
        icon={icons.menu}
        iconStyle={styles.iconstyle}
        containerStyle={styles.iconcont}
      />
    </>
  );
};

export default FilesItem;

const styles = StyleSheet.create({
  thumb: {width: 80, height: 80},
  nameSectionCont: {flex: 1, marginLeft: SIZES.radius},
  nameTxt: {...FONTS.h2},
  authorTxt: {color: COLORS.gray30, ...FONTS.body3},
  upload: {...FONTS.body4},
  iconstyle: {
    width: 25,
    height: 25,
    tintColor: COLORS.black,
  },
  iconcont: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
  },
});
