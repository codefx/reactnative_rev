import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import IconLabelButton from '../../../../../../components/IconLabelButton';
import icons from '../../../../../../assetsConstants/icons';
import SIZES from '../../../../../../theme/sizes';
import COLORS from '../../../../../../theme/colors';
import FONTS from '../../../../../../theme/fontStyles';

const CommentOptionSection = ({commentItem, replay}) => {
  return (
    <>
      {/* commentOption */}
      <View style={styles.commentOptionCont}>
        {/* Comment  */}
        <IconLabelButton
          icon={replay ? icons.reply : icons.comment}
          label={replay ? 'Reply' : commentItem.no_of_comments}
          //containerStyle={styles.commBut}
          iconStyle={styles.commButIconSty}
          labelStyle={styles.commButTxtSty}
        />
        {/* Like */}
        <IconLabelButton
          icon={icons.heart}
          label={commentItem.no_of_likes}
          containerStyle={styles.likeBut}
          labelStyle={styles.commButTxtSty}
        />
        {/* Date */}
        <Text style={styles.dateTxt}>{commentItem?.posted_on}</Text>
      </View>
    </>
  );
};

export default CommentOptionSection;

const styles = StyleSheet.create({
  main: {
    margintTop: 3,
    marginLeft: SIZES.radius,
  },
  commentOptionCont: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
    paddingVertical: SIZES.base,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: COLORS.gray20,
  },
  commBut: {
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  commButIconSty: {
    // width: 20,
    // height: 20,
    tintColor: COLORS.black,
  },
  commButTxtSty: {marginLeft: 3, color: COLORS.black, ...FONTS.h4},
  likeBut: {
    marginLeft: SIZES.radius,
  },
  dateTxt: {flex: 1, textAlign: 'right', ...FONTS.h4},
});
