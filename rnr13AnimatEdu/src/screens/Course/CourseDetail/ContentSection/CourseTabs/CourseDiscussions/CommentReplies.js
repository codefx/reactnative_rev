import {FlatList, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import CommentItem from './CommentItem';

const CommentReplies = ({commentItem, commOpts}) => {
  return (
    <FlatList
      data={commentItem?.replies}
      scrollEnabled={false}
      keyExtractor={item => `Discussions-replies-${item.id}`}
      renderItem={({item, index}) => (
        <CommentItem commentItem={item} commentOptions={commOpts} />
      )}
    />
  );
};

export default CommentReplies;

const styles = StyleSheet.create({});
