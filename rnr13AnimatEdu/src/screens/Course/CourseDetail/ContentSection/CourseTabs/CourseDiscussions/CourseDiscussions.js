import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import COLORS from '../../../../../../theme/colors';
import Discussions from './Discussions';

const CourseDiscussions = () => {
  return (
    <View style={styles.main}>
      {/* Discussions */}
      <Discussions />
      {/* footer */}
    </View>
  );
};

export default CourseDiscussions;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
});
