import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../../../../../theme/sizes';
import FONTS from '../../../../../../theme/fontStyles';
import COLORS from '../../../../../../theme/colors';
import IconLabelButton from '../../../../../../components/IconLabelButton';
import icons from '../../../../../../assetsConstants/icons';
import CommentItem from './CommentItem';
import CommentOptionSection from './CommentOptionSection';
import CommentReplies from './CommentReplies';

const CommentsSection = ({commentItem, commentOption, replies}) => {
  return (
    <View style={styles.main}>
      <CommentItem
        commentItem={commentItem}
        commentOptions={<CommentOptionSection commentItem={commentItem} />}
        replies={
          <CommentReplies
            commentItem={commentItem}
            commOpts={
              <CommentOptionSection commentItem={commentItem} replay={true} />
            }
          />
        }
      />

      {/* replies */}

      {/* <FlatList
        data={commentItem?.replies}
        scrollEnabled={false}
        keyExtractor={item => `Discussions-replies-${item.id}`}
        renderItem={({item, index}) => <CommentItem commentItem={item} />}
      /> */}
    </View>
  );
};

export default CommentsSection;

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    marginTop: SIZES.padding,
  },
  profile: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#a1afec',
  },
  namecommentCont: {
    flex: 1,
    margintTop: 3,
    marginLeft: SIZES.radius,
  },
  nameTxt: {...FONTS.h3},
  comment: {
    ...FONTS.body4,
  },
  commentOptionCont: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
    paddingVertical: SIZES.base,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: COLORS.gray20,
  },
  commBut: {
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  commButIconSty: {
    // width: 20,
    // height: 20,
    tintColor: COLORS.black,
  },
  commButTxtSty: {marginLeft: 3, color: COLORS.black, ...FONTS.h4},
  likeBut: {
    marginLeft: SIZES.radius,
  },
  dateTxt: {flex: 1, textAlign: 'right', ...FONTS.h4},
});
