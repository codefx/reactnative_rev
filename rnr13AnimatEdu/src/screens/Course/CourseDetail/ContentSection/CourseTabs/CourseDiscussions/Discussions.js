import {FlatList, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import course_details from '../../../../../../data/course_details';
import SIZES from '../../../../../../theme/sizes';
import CommentsSection from './CommentsSection';

const Discussions = () => {
  return (
    <View style={styles.main}>
      <FlatList
        data={course_details?.discussions}
        keyExtractor={item => `Discussions-main-${item.id}`}
        contentContainerStyle={styles.flatCont}
        renderItem={({item, index}) => (
          <CommentsSection
            commentItem={item}
            //   commentOption={}
          />
        )}
      />
    </View>
  );
};

export default Discussions;

const styles = StyleSheet.create({
  main: {flex: 1},
  flatCont: {
    paddingHorizontal: SIZES.padding,
    paddingBottom: 70,
  },
});
