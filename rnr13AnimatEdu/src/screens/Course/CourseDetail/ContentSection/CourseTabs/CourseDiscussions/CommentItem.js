import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import IconLabelButton from '../../../../../../components/IconLabelButton';
import icons from '../../../../../../assetsConstants/icons';
import SIZES from '../../../../../../theme/sizes';
import FONTS from '../../../../../../theme/fontStyles';
import COLORS from '../../../../../../theme/colors';

const CommentItem = ({commentItem, commentOptions, replies}) => {
  return (
    <>
      <Image
        source={commentItem?.profile}
        style={styles.profile}
        resizeMode="center"
      />
      {/* Name & Comment */}
      <View style={styles.namecommentCont}>
        <Text style={styles.nameTxt}>{commentItem?.name}</Text>
        <Text style={styles.comment}>{commentItem?.comment}</Text>
        {commentOptions}
        {replies}
      </View>
    </>
  );
};

export default CommentItem;

const styles = StyleSheet.create({
  profile: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#a1afec',
  },
  namecommentCont: {
    flex: 1,
    margintTop: 3,
    marginLeft: SIZES.radius,
  },
  nameTxt: {...FONTS.h3},
  comment: {
    ...FONTS.body4,
  },
});
