import {Animated, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import SIZES from '../../../../theme/sizes';
import course_details_tabs from '../../../../constants/course_details_tabs';
import {CourseDetailContext} from '../../../../contexts/CourseDetailContext';
import CourseChapters from './CourseTabs/CourseChapters/CourseChapters';
import CourseFiles from './CourseTabs/CourseFiles/CourseFiles';
import CourseDiscussions from './CourseTabs/CourseDiscussions/CourseDiscussions';

const Contents = () => {
  const {scrollX, flatlistRef} = useContext(CourseDetailContext);
  return (
    <Animated.FlatList
      ref={flatlistRef}
      horizontal
      pagingEnabled
      snapToAlignment="center"
      snapToInterval={SIZES.width}
      decelerationRate="fast"
      keyboardDismissMode="on-drag"
      showsHorizontalScrollIndicator={false}
      data={course_details_tabs}
      keyExtractor={item => `CourseDetailTabs-${item.id}`}
      onScroll={Animated.event([{nativeEvent: {contentOffset: {x: scrollX}}}], {
        useNativeDriver: false,
      })}
      renderItem={({item, index}) => {
        return (
          <View style={{width: SIZES.width}}>
            {index == 0 && <CourseChapters />}
            {index == 1 && <CourseFiles />}
            {index == 2 && <CourseDiscussions />}
          </View>
        );
      }}
    />
  );
};

export default Contents;

const styles = StyleSheet.create({});
