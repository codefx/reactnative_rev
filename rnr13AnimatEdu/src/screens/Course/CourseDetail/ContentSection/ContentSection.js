import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import ContentTabs from './ContentTabs';
import LineDivider from '../../../../components/LineDivider';
import Contents from './Contents';

const ContentSection = () => {
  return (
    <View style={styles.main}>
      {/* tabs */}
      <ContentTabs />
      {/* linedivider */}
      <LineDivider />
      {/* content */}
      <Contents />
    </View>
  );
};

export default ContentSection;

const styles = StyleSheet.create({
  main: {flex: 1},
});
