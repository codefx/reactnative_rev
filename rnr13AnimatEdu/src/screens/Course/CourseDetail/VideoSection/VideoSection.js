import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import COLORS from '../../../../theme/colors';
import SIZES from '../../../../theme/sizes';
import IconButton from '../../../../components/IconButton';
import icons from '../../../../assetsConstants/icons';
import Video from 'react-native-video';
import sample_video_url from '../../../../data/videouri';

import {CourseDetailContext} from '../../../../contexts/CourseDetailContext';

const VideoSection = ({selectedCourse}) => {
  const {playVideo, setPlayVideo} = useContext(CourseDetailContext);
  return (
    <View style={styles.main}>
      {/* Thumbnail */}
      <ImageBackground
        source={selectedCourse?.thumbnail}
        style={styles.imgBground}>
        {/* playbutton */}
        <IconButton
          icon={icons.play}
          iconStyle={styles.icon}
          containerStyle={styles.iconContainer}
          onPress={() => setPlayVideo(true)}
        />
      </ImageBackground>
      {playVideo && (
        <Video
          source={{uri: sample_video_url}}
          controls={true}
          style={styles.video}
        />
      )}
    </View>
  );
};

export default VideoSection;

const styles = StyleSheet.create({
  main: {
    height: SIZES.height > 800 ? 220 : 200,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.gray90,
  },
  imgBground: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 25,
    height: 25,
    tintColor: COLORS.white,
  },
  iconContainer: {
    width: 55,
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: SIZES.padding,
    borderRadius: 30,
    backgroundColor: COLORS.primary,
  },
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: COLORS.black,
  },
});
