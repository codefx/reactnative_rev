import {StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import SIZES from '../../../theme/sizes';
import HeaderComp from './HeaderComp';

import COLORS from '../../../theme/colors';
import {CourseDetailContext} from '../../../contexts/CourseDetailContext';

const HeaderBar = () => {
  const {playVideo} = useContext(CourseDetailContext);
  if (playVideo) {
    return (
      <View style={styles.main2}>
        <HeaderComp />
      </View>
    );
  } else {
    return (
      <View style={styles.main}>
        <HeaderComp />
      </View>
    );
  }
};

export default HeaderBar;

const styles = StyleSheet.create({
  main: {
    position: 'absolute',
    top: SIZES.height > 800 ? 40 : 20,
    left: 0,
    right: 0,
    flexDirection: 'row',
    paddingHorizontal: SIZES.padding,
    zIndex: 1,
  },
  main2: {
    flexDirection: 'row',
    paddingHorizontal: SIZES.radius,
    paddingBottom: SIZES.base,
    height: 85,
    backgroundColor: COLORS.black,
    alignItems: 'flex-end',
  },
});
