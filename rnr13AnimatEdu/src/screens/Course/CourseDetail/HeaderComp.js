import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import IconButton from '../../../components/IconButton';
import icons from '../../../assetsConstants/icons';
import COLORS from '../../../theme/colors';
import {useNavigation} from '@react-navigation/native';

const HeaderComp = () => {
  const navigation = useNavigation();
  return (
    <>
      {/* backbutton */}
      <View style={styles.backBtn}>
        <IconButton
          icon={icons.back}
          iconStyle={styles.icon}
          containerStyle={styles.iconContainer}
          onPress={() => navigation.goBack()}
        />
      </View>
      {/* share&favourite */}
      <View style={styles.otherButtonsMain}>
        <IconButton
          icon={icons.media}
          iconStyle={styles.otherIcons}
          containerStyle={styles.otherIconContainer}
        />
        <IconButton
          icon={icons.favourite_outline}
          iconStyle={styles.otherIcons}
          containerStyle={styles.otherIconContainer}
        />
      </View>
    </>
  );
};

export default HeaderComp;

const styles = StyleSheet.create({
  backBtn: {
    flex: 1,
  },
  icon: {
    width: 25,
    height: 25,
    tintColor: COLORS.black,
  },
  iconContainer: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: COLORS.white,
  },
  otherButtonsMain: {
    flexDirection: 'row',
  },
  otherIcons: {
    tintColor: COLORS.white,
  },
  otherIconContainer: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
