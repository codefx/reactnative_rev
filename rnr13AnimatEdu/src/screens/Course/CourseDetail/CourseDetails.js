import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import COLORS from '../../../theme/colors';
import VideoSection from './VideoSection/VideoSection';
import HeaderBar from './HeaderBar';

import ContentSection from './ContentSection/ContentSection';
import CourseDetailCtxProvider from '../../../contexts/CourseDetailContext';

const CourseDetails = ({navigation, route}) => {
  const {selectedCourse} = route.params;
  return (
    <CourseDetailCtxProvider>
      <View style={styles.mainContainer}>
        {/* headerBar */}
        <HeaderBar />
        {/* video */}
        <VideoSection selectedCourse={selectedCourse} />
        {/* contentsection */}
        <ContentSection />
      </View>
    </CourseDetailCtxProvider>
  );
};

export default CourseDetails;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
});
