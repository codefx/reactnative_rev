import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../theme/sizes';
import FONTS from '../../theme/fontStyles';
import TextButton from '../TextButton';
import COLORS from '../../theme/colors';

const FilterHeader = ({onPress}) => {
  return (
    <View style={styles.main}>
      <View style={{width: 60}} />
      <Text style={styles.text}>Filter</Text>
      <TextButton
        label="Cancel"
        contentContainerStyle={styles.txtBtnContainer}
        labelStyle={styles.label}
        onPress={onPress}
      />
    </View>
  );
};

export default FilterHeader;

const styles = StyleSheet.create({
  main: {
    marginTop: SIZES.padding,
    flexDirection: 'row',
    paddingHorizontal: SIZES.padding,
  },
  text: {
    flex: 1,
    textAlign: 'center',
    ...FONTS.h2,
  },
  txtBtnContainer: {width: 60, backgroundColor: null},
  label: {
    color: COLORS.black,
    ...FONTS.body3,
  },
});
