import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import SIZES from '../../theme/sizes';
import COLORS from '../../theme/colors';
import CustomMarker from './CustomMarker';

const TwoPointSlider = ({
  values,
  min,
  max,
  prefix,
  postfix,
  onValuesChange,
}) => {
  return (
    <MultiSlider
      values={values}
      sliderLength={SIZES.width - SIZES.padding * 2 - 30}
      min={min}
      max={max}
      step={1}
      markerOffsetY={15}
      selectedStyle={styles.selectedStyle}
      trackStyle={styles.trackStyle}
      customMarker={e => (
        <CustomMarker e={e} prefix={prefix} postfix={postfix} />
      )}
      onValuesChange={values => onValuesChange(values)}
    />
  );
};

export default TwoPointSlider;

const styles = StyleSheet.create({
  selectedStyle: {
    height: 2,
    backgroundColor: COLORS.primary,
  },
  trackStyle: {
    height: 1,
    borderRadius: 10,
    backgroundColor: COLORS.gray30,
  },
});
