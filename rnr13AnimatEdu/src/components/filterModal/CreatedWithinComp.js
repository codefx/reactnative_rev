import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import SIZES from '../../theme/sizes';
import FONTS from '../../theme/fontStyles';
import created_within from '../../constants/created_within';
import TextButton from '../TextButton';
import COLORS from '../../theme/colors';

const CreatedWithinComp = () => {
  const [selectedCreatedwithin, setSelectedCreatedwithin] = useState('');
  return (
    <View style={styles.main}>
      <Text style={styles.text}>Created Within</Text>
      <View style={styles.innerContainer}>
        {created_within.map((item, index) => {
          return (
            <TextButton
              key={`CreatedWithin-${index}`}
              label={item?.label}
              contentContainerStyle={{
                ...styles.txtBtnContainer,
                backgroundColor:
                  item?.id == selectedCreatedwithin ? COLORS.primary3 : null,
              }}
              labelStyle={{
                ...styles.label,
                color:
                  item?.id == selectedCreatedwithin
                    ? COLORS.white
                    : COLORS.black,
              }}
              onPress={() => setSelectedCreatedwithin(item.id)}
            />
          );
        })}
      </View>
    </View>
  );
};

export default CreatedWithinComp;

const styles = StyleSheet.create({
  main: {
    marginTop: SIZES.radius,
  },
  text: {
    ...FONTS.h3,
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  txtBtnContainer: {
    height: 40,
    paddingHorizontal: SIZES.radius,
    marginLeft: SIZES.radius,
    marginTop: SIZES.radius,
    borderWidth: 1,
    borderRadius: SIZES.radius,
    borderColor: COLORS.gray20,
  },
  label: {
    ...FONTS.body3,
  },
});
