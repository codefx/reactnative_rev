import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import COLORS from '../../theme/colors';
import FONTS from '../../theme/fontStyles';

const CustomMarker = ({prefix, e, postfix}) => {
  return (
    <View style={styles.main}>
      <View style={styles.inner} />
      <Text style={styles.text}>
        {prefix}
        {e.currentValue} {postfix}
      </Text>
    </View>
  );
};

export default CustomMarker;

const styles = StyleSheet.create({
  main: {
    height: 60,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inner: {
    height: 15,
    width: 15,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: COLORS.primary,
    backgroundColor: COLORS.white,
  },
  text: {
    marginTop: 5,
    color: COLORS.gray80,
    ...FONTS.body3,
  },
});
