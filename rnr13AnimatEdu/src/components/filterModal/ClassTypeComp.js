import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import SIZES from '../../theme/sizes';
import FONTS from '../../theme/fontStyles';
import class_types from '../../constants/class_types';
import ClassTypeOption from './ClassTypeOption';

const ClassTypeComp = () => {
  const [selectedClassType, setSelectedClassType] = useState('');
  return (
    <View style={styles.main}>
      <Text style={styles.text}>Class Type</Text>
      <View style={styles.optionsContainer}>
        {class_types.map((item, index) => {
          return (
            <ClassTypeOption
              key={`ClassType- ${index}`}
              classType={item}
              isSelected={selectedClassType === item?.id}
              containerStyle={{marginLeft: SIZES.base}}
              onPress={() => {
                setSelectedClassType(item.id);
              }}
            />
          );
        })}
      </View>
    </View>
  );
};

export default ClassTypeComp;

const styles = StyleSheet.create({
  main: {
    marginTop: SIZES.radius,
  },
  text: {
    ...FONTS.h3,
  },
  optionsContainer: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
  },
});
