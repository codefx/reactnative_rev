import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import SIZES from '../../theme/sizes';
import FONTS from '../../theme/fontStyles';
import class_levels from '../../constants/class_levels';
import ClassLevelOption from './ClassLevelOption';

const ClassLevelComp = () => {
  const [selectedClassLevel, setSelectedClassLevel] = useState('');
  return (
    <View style={styles.main}>
      <Text style={styles.text}>Class Level</Text>
      <View>
        {class_levels.map((item, index) => {
          return (
            <ClassLevelOption
              key={`ClassLevel-${index}`}
              classLevel={item}
              isLastItem={index === class_levels.length - 1}
              isSelected={selectedClassLevel === item?.id}
              onPress={() => {
                setSelectedClassLevel(item.id);
              }}
            />
          );
        })}
      </View>
    </View>
  );
};

export default ClassLevelComp;

const styles = StyleSheet.create({
  main: {
    marginTop: SIZES.padding,
  },
  text: {
    ...FONTS.h3,
  },
  //   innerContainer:
});
