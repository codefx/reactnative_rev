import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../theme/sizes';
import FONTS from '../../theme/fontStyles';
import TwoPointSlider from './TwoPointSlider';

const ClassLengthComp = () => {
  return (
    <View style={styles.main}>
      <Text style={styles.text}>Class Length</Text>
      <View style={styles.sliderContainer}>
        <TwoPointSlider
          values={[20, 50]}
          min={15}
          max={60}
          postfix="min"
          onValuesChange={values => console.log(values)}
        />
      </View>
    </View>
  );
};

export default ClassLengthComp;

const styles = StyleSheet.create({
  main: {
    marginTop: SIZES.padding,
  },
  text: {
    ...FONTS.h3,
  },
  sliderContainer: {
    alignItems: 'center',
  },
});
