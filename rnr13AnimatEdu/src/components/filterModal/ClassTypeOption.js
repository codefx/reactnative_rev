import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../theme/sizes';
import COLORS from '../../theme/colors';
import FONTS from '../../theme/fontStyles';

const ClassTypeOption = ({containerStyle, classType, isSelected, onPress}) => {
  return (
    <Pressable
      style={{
        ...styles.pressable,
        backgroundColor: isSelected ? COLORS.primary3 : COLORS.additionalColor9,
        ...containerStyle,
      }}
      onPress={onPress}>
      <Image
        source={classType.icon}
        resizeMode="contain"
        style={{
          ...styles.image,
          tintColor: isSelected ? COLORS.white : COLORS.gray80,
        }}
      />
      <Text
        style={{
          ...styles.text,
          color: isSelected ? COLORS.white : COLORS.gray80,
        }}>
        {classType.label}
      </Text>
    </Pressable>
  );
};

export default ClassTypeOption;

const styles = StyleSheet.create({
  pressable: {
    flex: 1,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: SIZES.radius,
    borderRadius: SIZES.radius,
  },
  image: {
    width: 40,
    height: 40,
  },
  text: {marginTop: SIZES.base, ...FONTS.h3},
});
