import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Animated, {
  interpolate,
  useAnimatedStyle,
  withDelay,
  withTiming,
} from 'react-native-reanimated';
import SIZES from '../../theme/sizes';
import COLORS from '../../theme/colors';
import FilterHeader from './FilterHeader';
import ClassTypeComp from './ClassTypeComp';
import ClassLevelComp from './ClassLevelComp';
import CreatedWithinComp from './CreatedWithinComp';
import ClassLengthComp from './ClassLengthComp';
import FooterSection from './FooterSection';

const FilterModal = ({filterModalSharedValue1, filterModalSharedValue2}) => {
  const filterModalContainerAnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: interpolate(
        filterModalSharedValue1.value,
        [SIZES.height, 0],
        [0, 1],
      ),
      transform: [
        {
          translateY: filterModalSharedValue1.value,
        },
      ],
    };
  });
  const filterModalBgAnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: interpolate(
        filterModalSharedValue2.value,
        [SIZES.height, 0],
        [0, 1],
      ),
    };
  });
  const filterModalContentAnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: interpolate(
        filterModalSharedValue2.value,
        [SIZES.height, 0],
        [0, 1],
      ),
      transform: [
        {
          translateY: filterModalSharedValue2.value,
        },
      ],
    };
  });
  const filterCancelOnPress = () => {
    filterModalSharedValue2.value = withTiming(SIZES.height, {duration: 500});
    filterModalSharedValue1.value = withDelay(
      500,
      withTiming(SIZES.height, {
        duration: 100,
      }),
    );
  };

  return (
    <Animated.View
      style={[styles.mainContainer, filterModalContainerAnimatedStyle]}>
      <Animated.View style={[styles.bgContainer, filterModalBgAnimatedStyle]}>
        <Animated.View
          style={[styles.contentContainer, filterModalContentAnimatedStyle]}>
          {/* header */}
          <FilterHeader onPress={filterCancelOnPress} />
          {/* content */}
          <ScrollView contentContainerStyle={styles.contentScroll}>
            {/* classType */}
            <ClassTypeComp />
            {/* classLevel */}
            <ClassLevelComp />
            {/* created Within */}
            <CreatedWithinComp />
            {/* classLength */}
            <ClassLengthComp />
          </ScrollView>
          {/* footerbuttons */}
          <FooterSection />
        </Animated.View>
      </Animated.View>
    </Animated.View>
  );
};

export default FilterModal;

const styles = StyleSheet.create({
  mainContainer: {
    position: 'absolute',
    bottom: 0,
    height: SIZES.height,
    width: SIZES.width,
  },
  bgContainer: {
    flex: 1,
    height: SIZES.height,
    width: SIZES.width,
    backgroundColor: COLORS.transparentBlack7,
  },
  contentContainer: {
    position: 'absolute',
    bottom: 0,
    height: SIZES.height * 0.91,
    width: SIZES.width,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: COLORS.white,
  },
  contentScroll: {
    paddingHorizontal: SIZES.padding,
    paddingBottom: 50,
  },
});
