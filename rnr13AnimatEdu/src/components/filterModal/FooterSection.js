import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../../theme/sizes';
import TextButton from '../TextButton';
import COLORS from '../../theme/colors';
import FONTS from '../../theme/fontStyles';

const FooterSection = () => {
  return (
    <View style={styles.main}>
      <TextButton
        label="Reset"
        contentContainerStyle={styles.LeftTxtBtnContainer}
        labelStyle={styles.labelLeft}
      />
      <TextButton
        label="Apply"
        contentContainerStyle={styles.RightTxtBtnContainer}
        labelStyle={styles.labelRight}
      />
    </View>
  );
};

export default FooterSection;

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    height: 50,
    marginBottom: 30,
    paddingHorizontal: SIZES.padding,
    gap: 10,
  },
  LeftTxtBtnContainer: {
    flex: 1,
    borderRadius: SIZES.radius,
    borderWidth: 1,
    backgroundColor: null,
  },
  labelLeft: {
    color: COLORS.black,
    ...FONTS.h3,
  },
  RightTxtBtnContainer: {
    flex: 1,
    marginLeft: SIZES.radius,
    borderRadius: SIZES.radius,
    borderWidth: 2,
    borderColor: COLORS.primary,
    backgroundColor: COLORS.primary,
  },
  labelRight: {
    color: COLORS.white,
    ...FONTS.h3,
  },
});
