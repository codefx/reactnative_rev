import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import FONTS from '../../theme/fontStyles';
import icons from '../../assetsConstants/icons';
import LineDivider from '../LineDivider';

const ClassLevelOption = ({
  containerStyle,
  classLevel,
  isLastItem,
  isSelected,
  onPress,
}) => {
  return (
    <>
      <Pressable
        style={{...styles.pressable, ...containerStyle}}
        onPress={onPress}>
        <Text style={styles.text}>{classLevel.label}</Text>
        <Image
          source={isSelected ? icons.checkbox_on : icons.checkbox_off}
          resizeMode="contain"
          style={styles.icon}
        />
      </Pressable>
      {!isLastItem && <LineDivider lineStyle={{height: 1}} />}
    </>
  );
};

export default ClassLevelOption;

const styles = StyleSheet.create({
  pressable: {
    flexDirection: 'row',
    height: 50,
    alignItems: 'center',
  },
  text: {
    flex: 1,
    ...FONTS.body3,
  },
  icon: {width: 20, height: 20},
});
