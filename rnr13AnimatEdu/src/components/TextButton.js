import {Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import COLORS from '../theme/colors';
import FONTS from '../theme/fontStyles';

const TextButton = ({
  contentContainerStyle,
  disabled,
  label,
  labelStyle,
  onPress,
}) => {
  return (
    <Pressable
      style={{...styles.pressable, ...contentContainerStyle}}
      disabled={disabled}
      onPress={onPress}>
      <Text style={{...styles.label, ...labelStyle}}>{label}</Text>
    </Pressable>
  );
};

export default TextButton;

const styles = StyleSheet.create({
  pressable: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primary,
  },
  label: {
    color: COLORS.white,
    ...FONTS.h3,
  },
});
