import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../theme/sizes';
import FONTS from '../theme/fontStyles';

const IconLabelButton = ({
  containerStyle,
  icon,
  iconStyle,
  label,
  labelStyle,
  onPress,
}) => {
  return (
    <Pressable
      style={{...styles.pressable, ...containerStyle}}
      onPress={onPress}>
      <Image
        source={icon}
        resizeMode="contain"
        style={{
          ...styles.icon,
          ...iconStyle,
        }}
      />
      <Text style={{...styles.text, ...labelStyle}}>{label}</Text>
    </Pressable>
  );
};

export default IconLabelButton;

const styles = StyleSheet.create({
  pressable: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // paddingVertical: SIZES.base,
    // paddingHorizontal: SIZES.radius,
  },
  icon: {width: 20, height: 20},
  text: {marginLeft: SIZES.base, ...FONTS.body3},
});
