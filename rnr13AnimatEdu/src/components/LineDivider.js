import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import COLORS from '../theme/colors';

const LineDivider = ({lineStyle}) => {
  return <View style={{...styles.divider, ...lineStyle}} />;
};

export default LineDivider;

const styles = StyleSheet.create({
  divider: {
    height: 2,
    width: '100%',
    backgroundColor: COLORS.gray20,
  },
});
