import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../theme/sizes';
import COLORS from '../theme/colors';
import icons from '../assetsConstants/icons';
import FONTS from '../theme/fontStyles';
import IconLabel from './IconLabel';

const VerticalCourseCard = ({containerStyle, course}) => {
  return (
    <Pressable style={{...styles.pressable, ...containerStyle}}>
      {/* ThumbNail */}
      <Image
        source={course.thumbnail}
        resizeMode="cover"
        style={styles.thumbnail}
      />
      {/* Detail */}
      <View style={styles.detail}>
        {/* Play */}
        <View style={styles.play}>
          <Image
            source={icons.play}
            resizeMode="contain"
            style={styles.playImage}
          />
        </View>
        {/* Info */}
        <View style={styles.info}>
          <Text style={styles.infoText}>{course.title}</Text>
          <IconLabel
            icon={icons.time}
            label={course.duration}
            containerStyle={{
              marginTop: SIZES.base,
            }}
          />
        </View>
      </View>
    </Pressable>
  );
};

export default VerticalCourseCard;

const styles = StyleSheet.create({
  pressable: {width: 270},
  thumbnail: {
    width: '100%',
    height: 140,
    marginBottom: SIZES.radius,
    borderRadius: SIZES.radius,
  },
  detail: {flexDirection: 'row'},
  play: {
    width: 45,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: COLORS.primary,
  },
  playImage: {
    width: 20,
    height: 20,
  },
  info: {flexShrink: 1, paddingHorizontal: SIZES.radius},
  infoText: {flex: 1, ...FONTS.h3, fontSize: 16},
});
