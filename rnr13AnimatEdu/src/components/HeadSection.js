import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import SIZES from '../theme/sizes';
import FONTS from '../theme/fontStyles';
import TextButton from './TextButton';
import COLORS from '../theme/colors';

const HeadSection = ({containerStyle, title, onPress, children}) => {
  return (
    <View style={{...containerStyle}}>
      <View style={styles.main}>
        <Text style={styles.title}>{title}</Text>
        <TextButton
          contentContainerStyle={styles.textButton}
          label="See All"
          onPress={onPress}
        />
      </View>
      {children}
    </View>
  );
};

export default HeadSection;

const styles = StyleSheet.create({
  main: {flexDirection: 'row', paddingHorizontal: SIZES.padding},
  title: {flex: 1, ...FONTS.h2},
  textButton: {
    width: 80,
    borderRadius: 30,
    backgroundColor: COLORS.primary,
  },
});
