import {Pressable, StyleSheet, Text, View, Image} from 'react-native';
import React, {useContext} from 'react';
import COLORS from '../theme/colors';
import SIZES from '../theme/sizes';
import FONTS from '../theme/fontStyles';
import icons from '../assetsConstants/icons';
import {ThemeContext} from '../contexts/ThemeContext';

const ProfileValue = ({icon, label, value, onPress}) => {
  const {currTheme} = useContext(ThemeContext);
  return (
    <Pressable style={styles.pressable} onPress={onPress}>
      {/* icon */}
      <View
        style={{
          ...styles.iconContainer,
          backgroundColor: currTheme?.backgroundColor3,
        }}>
        <Image source={icon} resizeMode="contain" style={styles.icon} />
      </View>
      {/* label&value */}
      <View style={styles.labelContainer}>
        {label && <Text style={styles.label}>{label}</Text>}
        <Text
          style={{
            ...styles.value,
            color: currTheme?.textColor,
          }}>
          {value}
        </Text>
      </View>
      {/* Right Icon */}
      <Image
        source={icons.right_arrow}
        style={{...styles.rightIcon, tintColor: currTheme?.tintColor}}
      />
    </Pressable>
  );
};

export default ProfileValue;

const styles = StyleSheet.create({
  pressable: {
    flexDirection: 'row',
    height: 60,
    alignItems: 'center',
  },
  iconContainer: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: COLORS.additionalColor11,
  },
  icon: {width: 25, height: 25, tintColor: COLORS.primary},
  labelContainer: {flex: 1, marginLeft: SIZES.radius},
  label: {color: COLORS.gray30, ...FONTS.body3},
  value: {...FONTS.h3},
  rightIcon: {
    width: 15,
    height: 15,
  },
});
