import {
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React from 'react';
import SIZES from '../theme/sizes';
import COLORS from '../theme/colors';
import FONTS from '../theme/fontStyles';
import icons from '../assetsConstants/icons';
import IconLabel from './IconLabel';

const HorizontalCourseCard = ({containerStyle, course, onPress}) => {
  return (
    <Pressable
      style={{...styles.pressable, ...containerStyle}}
      onPress={onPress}>
      {/* thumbnail */}
      <ImageBackground
        source={course.thumbnail}
        resizeMode="cover"
        style={styles.imgBgrnd}
        imageStyle={{borderRadius: SIZES.radius}}>
        <View style={styles.topIcon}>
          <Image
            source={icons.favourite}
            resizeMode="contain"
            style={{
              ...styles.topIconImage,
              tintColor: course.is_favourite
                ? COLORS.secondary
                : COLORS.additionalColor4,
            }}
          />
        </View>
      </ImageBackground>
      {/* detail */}
      <View style={styles.detail}>
        {/* Title */}
        <Text style={styles.title}>{course.title}</Text>
        {/* Instructor & Duration */}
        <View style={styles.instruct}>
          <Text style={{...FONTS.body4}}>By {course.instructor}</Text>
          <IconLabel
            icon={icons.time}
            label={course.duration}
            containerStyle={styles.timeIcon.cont}
            iconStyle={styles.timeIcon.icon}
            labelStyle={styles.timeIcon.label}
          />
        </View>
        {/* price &ratings */}
        <View style={styles.price}>
          <Text style={styles.priceText}>${course.price.toFixed(2)}</Text>
          <IconLabel
            icon={icons.star}
            label={course.ratings}
            containerStyle={styles.starIcon.cont}
            iconStyle={styles.starIcon.icon}
            labelStyle={styles.starIcon.label}
          />
        </View>
      </View>
    </Pressable>
  );
};

export default HorizontalCourseCard;

const styles = StyleSheet.create({
  pressable: {
    flexDirection: 'row',
  },
  imgBgrnd: {
    width: 138,
    height: 130,
    marginBottom: SIZES.radius / 2,
  },
  topIcon: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    backgroundColor: COLORS.white,
  },
  topIconImage: {
    width: 20,
    height: 20,
  },
  detail: {flex: 1, marginLeft: SIZES.base},
  title: {
    ...FONTS.h3,
    fontSize: 16,
  },
  instruct: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: SIZES.base,
  },
  price: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: SIZES.base,
  },
  priceText: {...FONTS.h2, color: COLORS.primary},
  timeIcon: {
    icon: {width: 15, height: 15},
    cont: {marginLeft: SIZES.base},
    label: {...FONTS.body4},
  },
  starIcon: {
    icon: {width: 15, height: 15, tintColor: COLORS.primary2},
    cont: {marginLeft: SIZES.base},
    label: {marginLeft: 5, color: COLORS.black, ...FONTS.h3},
  },
});
