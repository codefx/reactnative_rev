import {StyleSheet, Text, View, Animated, Image, Pressable} from 'react-native';
import React, {useContext, useEffect, useRef} from 'react';
import COLORS from '../theme/colors';
import SIZES from '../theme/sizes';
import FONTS from '../theme/fontStyles';
import {ThemeContext} from '../contexts/ThemeContext';

const ProfileRadioButton = ({icon, label, isSelected, onPress}) => {
  const {currTheme} = useContext(ThemeContext);
  const radioAnimated = useRef(new Animated.Value(0)).current;
  const circleColorAnimated = radioAnimated.interpolate({
    inputRange: [0, 17],
    outputRange: [COLORS.gray40, COLORS.primary],
  });
  const lineColorAnimated = radioAnimated.interpolate({
    inputRange: [0, 17],
    outputRange: [COLORS.additionalColor4, COLORS.additionalColor13],
  });

  useEffect(() => {
    if (isSelected) {
      Animated.timing(radioAnimated, {
        toValue: 17,
        duration: 300,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(radioAnimated, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
      }).start();
    }
  }, [isSelected]);

  return (
    <View style={styles.main}>
      {/* Icon */}
      <View
        style={{
          ...styles.iconContainer,
          backgroundColor: currTheme?.backgroundColor3,
        }}>
        <Image source={icon} resizeMode="contain" style={styles.image} />
      </View>
      {/* label */}
      <View style={styles.labelContainer}>
        <Text style={{...styles.label, color: currTheme?.textColor}}>
          {label}
        </Text>
      </View>
      {/* radio button */}
      <Pressable style={styles.pressable} onPress={onPress}>
        <Animated.View
          style={{...styles.lineAnimated, backgroundColor: lineColorAnimated}}
        />
        <Animated.View
          style={{
            ...styles.circleAnimated,
            backgroundColor: currTheme?.backgroundColor1,
            left: radioAnimated,
            borderColor: circleColorAnimated,
          }}
        />
      </Pressable>
    </View>
  );
};

export default ProfileRadioButton;

const styles = StyleSheet.create({
  main: {flexDirection: 'row', height: 80, alignItems: 'center'},
  iconContainer: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: COLORS.additionalColor11,
  },
  image: {width: 25, height: 25, tintColor: COLORS.primary},
  labelContainer: {
    flex: 1,
    marginLeft: SIZES.radius,
  },
  label: {
    ...FONTS.h3,
  },
  pressable: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  lineAnimated: {
    width: '100%',
    height: 5,
    borderRadius: 3,
  },
  circleAnimated: {
    position: 'absolute',
    width: 25,
    height: 25,
    borderRadius: 15,
    borderWidth: 5,
    backgroundColor: COLORS.white,
  },
});
