import {Pressable, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Image} from 'react-native';
import COLORS from '../theme/colors';

const IconButton = ({containerStyle, icon, iconStyle, onPress}) => {
  return (
    <Pressable
      style={{...styles.pressable, ...containerStyle}}
      onPress={onPress}>
      <Image
        source={icon}
        resizeMode="contain"
        style={{...styles.image, ...iconStyle}}
      />
    </Pressable>
  );
};

export default IconButton;

const styles = StyleSheet.create({
  pressable: {},
  image: {width: 30, height: 30, tintColor: COLORS.white},
});
