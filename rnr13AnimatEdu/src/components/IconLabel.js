import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import COLORS from '../theme/colors';
import SIZES from '../theme/sizes';
import FONTS from '../theme/fontStyles';

const IconLabel = ({containerStyle, icon, iconStyle, label, labelStyle}) => {
  return (
    <View style={{...styles.main, ...containerStyle}}>
      <Image source={icon} style={{...styles.image, ...iconStyle}} />
      <Text style={{...styles.text, ...labelStyle}}>{label}</Text>
    </View>
  );
};

export default IconLabel;

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    width: 20,
    height: 20,
    tintColor: COLORS.gray30,
  },
  text: {
    marginLeft: SIZES.base,
    color: COLORS.gray30,
    ...FONTS.body3,
  },
});
