import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React, {useContext, useEffect, useRef} from 'react';

import COLORS from '../../theme/colors';
import FONTS from '../../theme/fontStyles';
import {MainLayoutContext} from '../../contexts/MainLayoutContext';
import TabIndicator from './TabIndicator';

const Tabs = () => {
  const {
    scrollX,
    measureLayout,
    setMeasureLayout,
    bottom_tabs,
    onBottomTabPress,
  } = useContext(MainLayoutContext);
  const containerRef = useRef();

  useEffect(() => {
    let ml = [];
    bottom_tabs.forEach(bottom_tab => {
      bottom_tab?.ref?.current?.measureLayout(
        containerRef.current,
        (x, y, width, height) => {
          ml.push({
            x,
            y,
            width,
            height,
          });
          if (ml.length === bottom_tabs.length) {
            setMeasureLayout(ml);
          }
        },
      );
    });
  }, [containerRef.current]);

  return (
    <View ref={containerRef} style={styles.main}>
      {/* Tab Indicator */}
      {measureLayout.length > 0 && (
        <TabIndicator measureLayout={measureLayout} scrollX={scrollX} />
      )}
      {/* Tabs */}
      {bottom_tabs.map((item, index) => {
        return (
          <Pressable
            key={`BottomTab-${index}`}
            ref={item.ref}
            style={styles.pressable}
            onPress={() => onBottomTabPress(index)}>
            <Image
              source={item.icon}
              resizeMode="contain"
              style={styles.image}
            />
            <Text style={styles.text}>{item.label}</Text>
          </Pressable>
        );
      })}
    </View>
  );
};

export default Tabs;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    flexDirection: 'row',
  },
  pressable: {
    flex: 1,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {width: 25, height: 25},
  text: {marginTop: 3, color: COLORS.white, ...FONTS.h3},
});
