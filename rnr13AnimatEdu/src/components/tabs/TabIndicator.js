import {Animated, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import SIZES from '../../theme/sizes';
import COLORS from '../../theme/colors';
import {MainLayoutContext} from '../../contexts/MainLayoutContext';

const TabIndicator = ({measureLayout, scrollX}) => {
  const {bottom_tabs} = useContext(MainLayoutContext);
  const inputRange = bottom_tabs.map((_, i) => i * SIZES.width);
  const tabIndicatorWidth = scrollX.interpolate({
    inputRange,
    outputRange: measureLayout.map(measure => measure.width),
  });
  const translateX = scrollX.interpolate({
    inputRange,
    outputRange: measureLayout.map(measure => measure.x),
  });
  return (
    <Animated.View
      style={{
        ...styles.main,
        width: tabIndicatorWidth,
        transform: [{translateX}],
      }}
    />
  );
};

export default TabIndicator;

const styles = StyleSheet.create({
  main: {
    position: 'absolute',
    left: 0,
    height: '100%',
    width: 80,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary,
  },
});
