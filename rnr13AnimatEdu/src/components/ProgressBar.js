import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import COLORS from '../theme/colors';

const ProgressBar = ({containerStyle, progress}) => {
  return (
    <View style={{...styles.main, ...containerStyle}}>
      <View style={{...styles.bar, width: progress}} />
    </View>
  );
};

export default ProgressBar;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: 13,
    borderRadius: 10,
    backgroundColor: COLORS.white,
  },
  bar: {
    position: 'absolute',
    left: 0,
    height: '100%',
    borderRadius: 10,
    backgroundColor: COLORS.primary,
  },
});
