import {
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React from 'react';
import SIZES from '../theme/sizes';
import COLORS from '../theme/colors';
import FONTS from '../theme/fontStyles';
import {SharedElement} from 'react-navigation-shared-element';

const CategoryCard = ({
  sharedElementPrefix,
  category,
  containerStyle,
  onPress,
}) => {
  return (
    <Pressable
      onPress={onPress}
      style={{...styles.pressable, ...containerStyle}}>
      <SharedElement
        id={`${sharedElementPrefix}-CategoryCard-Bg-${category?.id}`}
        style={[StyleSheet.absoluteFillObject]}>
        <Image
          source={category?.thumbnail}
          resizeMode="cover"
          style={{...styles.bigImage}}
        />
      </SharedElement>
      {/* <ImageBackground
        source={category?.thumbnail}
        resizeMode="cover"
        style={{...styles.imgBgrnd, ...containerStyle}}
        imageStyle={styles.image}>
        <Text style={styles.title}>{category?.title}</Text>
      </ImageBackground> */}
      {/* Title */}
      <View style={styles.titleContainer}>
        <SharedElement
          id={`${sharedElementPrefix}-CategoryCard-Title-${category?.id}`}
          style={[StyleSheet.absoluteFillObject]}>
          <Text style={styles.title}>{category?.title}</Text>
        </SharedElement>
      </View>
    </Pressable>
  );
};

export default CategoryCard;

const styles = StyleSheet.create({
  pressable: {
    height: 150,
    width: 200,
  },
  imgBgrnd: {
    height: 150,
    width: 200,
    paddingVertical: SIZES.padding,
    paddingHorizontal: SIZES.radius,
    justifyContent: 'flex-end',
  },
  bigImage: {
    width: '100%',
    height: '100%',
    borderRadius: SIZES.radius,
  },
  titleContainer: {position: 'absolute', bottom: 50, left: 5},
  title: {
    position: 'absolute',
    color: COLORS.white,
    ...FONTS.h2,
  },
  image: {
    borderRadius: SIZES.radius,
  },
});
