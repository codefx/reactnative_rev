import React from 'react';

import {Pressable, Text, TextInput, View} from 'react-native';


const InputField = ({
  label,
  icon,
  inputType,
  keyboardType,
  fieldButtonLabel,
  fieldButtonFunc,
}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        paddingBottom: 8,
        marginBottom: 25,
      }}>
      {icon}
      {inputType == 'password' ? (
        <TextInput
          secureTextEntry={true}
          keyboardType={keyboardType}
          placeholder={label}
          style={{
            flex: 1,
            paddingVertical: 0,
          }}
        />
      ) : (
        <TextInput
          keyboardType={keyboardType}
          placeholder={label}
          style={{
            flex: 1,
            paddingVertical: 0,
          }}
        />
      )}
      <Pressable onPress={fieldButtonFunc}>
        <Text style={{color: '#ff7030', fontWeight: '600'}}>
          {fieldButtonLabel}
        </Text>
      </Pressable>
    </View>
  );
};

export default InputField;
