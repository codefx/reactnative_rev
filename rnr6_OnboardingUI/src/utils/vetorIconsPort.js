import MIcon from "react-native-vector-icons/MaterialIcons";
import MCIcon from "react-native-vector-icons/MaterialCommunityIcons";
import Fa5Icon from "react-native-vector-icons/FontAwesome5";

export { MIcon, MCIcon, Fa5Icon };
