import { Dimensions } from "react-native";

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
const fontScale = Dimensions.get("window").fontScale;
const windowScale = Dimensions.get("window").scale;
export { deviceWidth, deviceHeight, fontScale, windowScale };
