import React, {useRef, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  //StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  //useColorScheme,
  View,
} from 'react-native';
import {MCIcon} from '../utils/vetorIconsPort';
import DatePicker from 'react-native-date-picker';
import Inputfield from '../components/Inputfield';

const LoginScreen = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  const [dobLabel, setDobLabel] = useState('Date of Birth');

  const checkUser = () => {
    if (email == 'cfx@cfx.com' && password == '123456') {
      alert('Successfully entered.');
    } else {
      alert('not successful operation.');
    }
  };
  return (
    <SafeAreaView
      style={{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
        }}>
        <TextInput
          placeholder="Enter your email"
          value={email}
          onChangeText={value => {
            setEmail(value);
          }}
          style={{
            borderColor: '#00f',
            paddingLeft: 8,
            borderWidth: 1,
            width: '100%',
            height: 60,
            borderRadius: 10,
            marginBottom: 10,
          }}
        />
        <TextInput
          secureTextEntry
          placeholder="Password"
          value={password}
          onChangeText={value => {
            setPassword(value);
          }}
          style={{
            paddingLeft: 8,
            borderColor: '#00f',
            borderWidth: 1,
            width: '100%',
            height: 60,
            borderRadius: 10,
            marginBottom: 10,
          }}
        />
        <Inputfield
          inputType={'password'}
          label={'confirm password'}
          icon={
            <MCIcon
              name="lock"
              size={20}
              color="#666"
              style={{marginRight: 5, paddingLeft: 8}}
            />
          }
        />
        <View
          style={{
            alignItems: 'center',
            flexDirection: 'row',
            borderColor: '#00f',
            borderWidth: 1,
            height: 60,
            borderRadius: 10,
            paddingBottom: 8,
            marginBottom: 30,
            marginTop: 10,
            width: '100%',
          }}>
          <MCIcon
            name="calendar-month-outline"
            size={20}
            color="#666"
            style={{marginRight: 5, paddingLeft: 8}}
          />
          <TouchableOpacity onPress={() => setOpen(true)}>
            <Text style={{color: '#666', marginLeft: 5, marginTop: 5}}>
              {dobLabel}
            </Text>
          </TouchableOpacity>
        </View>
        <DatePicker
          modal
          open={open}
          date={date}
          mode={'date'}
          maximumDate={new Date('2004-01-01')}
          minimumDate={new Date('1970-01-01')}
          onConfirm={date => {
            setOpen(false);
            setDate(date);
            setDobLabel(date.toDateString());
          }}
          onCancel={() => {
            setOpen(false);
          }}
        />
        <TouchableOpacity
          onPress={() => checkUser()}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#00f',
            padding: 5,
            width: '100%',
            height: 60,
            borderRadius: 25,
            marginTop: 10,
            marginBottom: 10,
          }}>
          <Text style={{color: '#fff', fontSize: 25}}>Giriş</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
export default LoginScreen;
