import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

import {Walkthrough, Welcome} from './src/screens';
import WalktroughCtxProvider from './src/contexts/WalktroughContext';
import AuthCtxProvider from './src/contexts/AuthContext';
import AuthMain from './src/screens/Authentication/AuthMain';
import Home from './src/screens/Home/Home';
import ScanProduct from './src/screens/ScanProduct/ScanProduct';
import routeNames from './src/navigations/routeNames';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <AuthCtxProvider>
        <WalktroughCtxProvider>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
            initialRouteName={routeNames.ScanProduct}>
            <Stack.Group>
              <Stack.Screen name={routeNames.Welcome} component={Welcome} />
              <Stack.Screen
                name={routeNames.Walkthrough}
                component={Walkthrough}
              />
            </Stack.Group>
            <Stack.Group>
              <Stack.Screen name={routeNames.AuthMain} component={AuthMain} />
              <Stack.Screen name={routeNames.Home} component={Home} />
              <Stack.Screen
                name={routeNames.ScanProduct}
                component={ScanProduct}
              />
            </Stack.Group>
          </Stack.Navigator>
        </WalktroughCtxProvider>
      </AuthCtxProvider>
    </NavigationContainer>
  );
};

export default App;
