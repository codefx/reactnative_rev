import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {FONTS, COLORS} from '../theme/theme';

const TextButton = ({
  contentContainerStyle,
  disabled,
  label,
  labelStyle,
  onPress,
}) => {
  return (
    <TouchableOpacity
      style={{...styles.main, ...contentContainerStyle}}
      disabled={disabled}
      onPress={onPress}>
      <Text style={{color: COLORS.secondary, ...FONTS.h3, ...labelStyle}}>
        {label}
      </Text>
    </TouchableOpacity>
  );
};

export default TextButton;

const styles = StyleSheet.create({
  main: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primary,
  },
});
