import React, {useEffect, useRef, useState} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {
  walkthrough_01_01_images,
  walkthrough_01_02_images,
} from '../../constants/walkthrough01images';
import {SIZES} from '../../theme/theme';

const ITEM_WIDTH = 120;
const SlidesFirst = () => {
  const [row1Images, setRow1Images] = useState([
    ...walkthrough_01_01_images,
    ...walkthrough_01_01_images,
  ]);
  const [currentPosition, setCurrentPosition] = useState(0);

  const [row2Images, setRow2Images] = useState([
    ...walkthrough_01_02_images,
    ...walkthrough_01_02_images,
  ]);
  const [row2CurrentPosition, setrow2CurrentPosition] = useState(0);

  const row1FlatListRef = useRef();
  const row2FlatListRef = useRef();

  useEffect(() => {
    let positionTimer;
    const timer = () => {
      positionTimer = setTimeout(() => {
        // Increment scroll position with each new interval
        // Slider 1
        setCurrentPosition(prevPosition => {
          const position = Number(prevPosition) + 1;
          row1FlatListRef?.current?.scrollToOffset({
            offset: position,
            animated: false,
          });
          const maxOffset = walkthrough_01_01_images.length * ITEM_WIDTH;
          if (prevPosition > maxOffset) {
            const offset = prevPosition - maxOffset;
            row1FlatListRef?.current?.scrollToOffset({offset, animated: false});
            return offset;
          } else {
            return position;
          }
        });
        // Slider 2
        setrow2CurrentPosition(prevPosition => {
          const position = Number(prevPosition) + 1;
          row2FlatListRef?.current?.scrollToOffset({
            offset: position,
            animated: false,
          });
          const maxOffset = walkthrough_01_02_images.length * ITEM_WIDTH;
          if (prevPosition > maxOffset) {
            const offset = prevPosition - maxOffset;
            row2FlatListRef?.current?.scrollToOffset({offset, animated: false});
            return offset;
          } else {
            return position;
          }
        });
        timer();
      }, 32);
    };
    timer();
    return () => {
      clearTimeout(positionTimer);
    };
  }, []);

  return (
    <View>
      {/* slider1  */}
      <FlatList
        ref={row1FlatListRef}
        decelerationRate="fast"
        horizontal
        showsHorizontalScrollIndicator={false}
        listKey="Slider1"
        keyExtractor={(_, index) => `Slider1_${index}`}
        data={row1Images}
        scrollEnabled={false}
        renderItem={({item, index}) => {
          return (
            <View style={styles.renderItemContainer}>
              <Image source={item} style={styles.slider1Images} />
            </View>
          );
        }}
      />
      {/* slider2 */}
      <FlatList
        ref={row2FlatListRef}
        decelerationRate="fast"
        style={{
          marginTop: SIZES.padding,
          transform: [{rotate: '180deg'}],
        }}
        horizontal
        showsHorizontalScrollIndicator={false}
        listKey="Slider2"
        keyExtractor={(_, index) => `Slider2_${index}`}
        data={row2Images}
        scrollEnabled={false}
        renderItem={({item, index}) => {
          return (
            <View
              style={{
                ...styles.renderItemContainer,
                transform: [{rotate: '180deg'}],
              }}>
              <Image source={item} style={styles.slider1Images} />
            </View>
          );
        }}
      />
    </View>
  );
};

export default SlidesFirst;

const styles = StyleSheet.create({
  renderItemContainer: {
    width: ITEM_WIDTH,
    alignItems: 'center',
    justifyContent: 'center',
  },
  slider1Images: {
    height: 110,
    width: 110,
  },
});
