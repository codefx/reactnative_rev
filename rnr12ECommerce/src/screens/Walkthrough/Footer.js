import {StyleSheet, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';

import TextButton from '../../components/TextButton';

const Footer = ({navigation}) => {
  return (
    <View style={styles.buttonContainer}>
      <TextButton
        label={'Join Now!'}
        contentContainerStyle={styles.textbut1.ctntContSty}
        labelStyle={styles.textbut1.labelStyle}
      />
      <TextButton
        label={'Log In'}
        contentContainerStyle={styles.textbut2.ctntContSty}
        labelStyle={styles.textbut2.labelStyle}
        onPress={() => navigation.navigate('AuthMain')}
      />
    </View>
  );
};

export default Footer;

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    height: 55,
    gap: SIZES.radius,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  textbut1: {
    ctntContSty: {
      flex: 1,
      borderRadius: SIZES.radius,
      backgroundColor: COLORS.grey20,
    },
    labelStyle: {
      color: COLORS.primary,
      ...FONTS.h3,
    },
  },
  textbut2: {
    ctntContSty: {
      flex: 1,
      borderRadius: SIZES.radius,
      backgroundColor: COLORS.primary,
    },
    labelStyle: {
      ...FONTS.h3,
    },
  },
});
