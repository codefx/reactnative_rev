import {Animated, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import {COLORS, SIZES} from '../../theme/theme';
import walkthrough from '../../constants/walkthrough';
import {WalktroughContext} from '../../contexts/WalktroughContext';

const Dots = () => {
  const {scrollX} = useContext(WalktroughContext);
  const dotPosition = Animated.divide(scrollX, SIZES.width);
  return (
    <View style={styles.container}>
      <View style={styles.mainView}>
        {walkthrough.map((item, index) => {
          const dotColor = dotPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [COLORS.dark08, COLORS.primary, COLORS.dark08],
            extrapolate: 'clamp',
          });
          return (
            <Animated.View
              key={`dot-${index}`}
              style={{...styles.items, backgroundColor: dotColor}}
            />
          );
        })}
      </View>
    </View>
  );
};

export default Dots;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: SIZES.height * 0.2,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: SIZES.padding,
    paddingVertical: SIZES.height > 700 ? SIZES.padding : 20,
  },
  mainView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  items: {
    borderRadius: 5,
    marginHorizontal: 6,
    width: 10,
    height: 10,
  },
});
