import React, {useContext, useEffect, useRef, useState} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {MotiImage, useDynamicAnimation} from 'moti';
import {FlatList} from 'react-native-gesture-handler';

import {SIZES} from '../../theme/theme';
import images from '../../constants/images';
import {WalktroughContext} from '../../contexts/WalktroughContext';

const ITEM_WIDTH = 120;
const SlidesSecond = () => {
  const {slidesSecondAnimate, setSlidesSecondAnimate} =
    useContext(WalktroughContext);
  // Moti initial position
  const motiImage1 = useDynamicAnimation(() => ({
    top: '30%',
    left: '25%',
  }));
  const motiImage2 = useDynamicAnimation(() => ({
    top: '45%',
    left: '15%',
  }));
  const motiImage3 = useDynamicAnimation(() => ({
    top: '60%',
    left: '28%',
  }));
  const motiImage4 = useDynamicAnimation(() => ({
    top: '66%',
    left: '38%',
  }));
  const motiImage5 = useDynamicAnimation(() => ({
    top: '20%',
    left: '47%',
  }));
  useEffect(() => {
    if (slidesSecondAnimate) {
      motiImage1.animateTo({top: '10%', left: '15%'});
      motiImage2.animateTo({top: '40%', left: 4});
      motiImage3.animateTo({top: '60%', left: '15%'});
      motiImage4.animateTo({top: '10%', left: '41%'});
      motiImage5.animateTo({top: '15%', left: '73%'});
    }
    return () => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [slidesSecondAnimate]);

  return (
    <View style={styles.container}>
      <Image source={images.walkthrough_02_01} style={styles.solidImage} />
      <Image source={images.walkthrough_02_02} style={styles.solidSmallImage} />
      <MotiImage
        state={motiImage1}
        source={images.walkthrough_02_03}
        style={styles.image}
      />
      <MotiImage
        state={motiImage2}
        source={images.walkthrough_02_04}
        style={styles.image}
      />
      <MotiImage
        state={motiImage3}
        source={images.walkthrough_02_05}
        style={styles.image}
      />
      <MotiImage
        state={motiImage4}
        source={images.walkthrough_02_06}
        style={styles.image}
      />
      <MotiImage
        state={motiImage5}
        source={images.walkthrough_02_07}
        style={styles.image}
      />
    </View>
  );
};

export default SlidesSecond;

const styles = StyleSheet.create({
  image: {
    position: 'absolute',
    width: 86,
    height: 112,
    zIndex: 0,
    borderRadius: SIZES.radius,
  },
  container: {
    flex: 1,
    overflow: 'hidden',
  },
  solidImage: {
    position: 'absolute',
    top: '35%',
    left: '35%',
    width: 106,
    height: 161,
    zIndex: 1,
    borderRadius: SIZES.radius,
  },
  solidSmallImage: {
    position: 'absolute',
    width: 86,
    height: 112,
    zIndex: 0,
    borderRadius: SIZES.radius,
    top: '50%',
    left: '50%',
  },
});
