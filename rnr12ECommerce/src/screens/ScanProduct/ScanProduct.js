import React from 'react';
import {StyleSheet, View} from 'react-native';

import CameraCtxProvider from '../../contexts/CameraContext';
import Footer from './Footer';
import Header from './Header';
import ScanBody from './ScanBody';
const ScanProduct = ({navigation}) => {
  return (
    <CameraCtxProvider>
      <View style={styles.container}>
        <Header navigation={navigation} />
        <ScanBody />
        <Footer />
      </View>
    </CameraCtxProvider>
  );
};

export default ScanProduct;

const styles = StyleSheet.create({
  container: {flex: 1},
});
