import React, {useContext, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';

import {Camera} from 'react-native-vision-camera';
import {CameraContext} from '../../contexts/CameraContext';

import ProductMotiCard from './ProductMotiCard';
import ScanCamera from './ScanCamera';
import ScanQrCode from './ScanQrCode';
import MotiSearchingText from './MotiSearchingText';

const ScanBody = () => {
  const {
    selectedOption,
    options,
    barcodes,
    productAnimationState,
    loaderAnimationState,
    requestCameraPermission,
    toggleActiveState,
    device,
    frameProcessor,
  } = useContext(CameraContext);

  useEffect(() => {
    if (selectedOption == options.qr) {
      toggleActiveState();
    }
  }, [barcodes]);

  useEffect(() => {
    productAnimationState.transitionTo('hide');
    loaderAnimationState.transitionTo('stop');
    requestCameraPermission();
  }, []);

  return (
    <View style={styles.container}>
      {device == null ? (
        <View style={styles.container} />
      ) : (
        <>
          <Camera
            style={styles.camera}
            device={device}
            isActive={true}
            enableZoomGesture
            frameProcessor={frameProcessor}
            frameProcessorFps={5}
          />
          {/* loading /searching */}
          <MotiSearchingText />
          {/* scan Camera or Barcode section */}
          {selectedOption == options.camera ? <ScanCamera /> : <ScanQrCode />}
          {/* product */}
          <ProductMotiCard />
        </>
      )}
    </View>
  );
};

export default ScanBody;

const styles = StyleSheet.create({
  container: {flex: 1},
  camera: {flex: 1},
});
