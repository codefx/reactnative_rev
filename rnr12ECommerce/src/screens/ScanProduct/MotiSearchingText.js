import {StyleSheet, Text} from 'react-native';
import React, {useContext} from 'react';
import {MotiView} from 'moti';
import {CameraContext} from '../../contexts/CameraContext';
import {COLORS, FONTS} from '../../theme/theme';

const MotiSearchingText = () => {
  const {loaderAnimationState} = useContext(CameraContext);
  return (
    <MotiView state={loaderAnimationState} style={styles.motiSearch}>
      <Text style={styles.searchText}>Searching</Text>
    </MotiView>
  );
};

export default MotiSearchingText;

const styles = StyleSheet.create({
  motiSearch: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.dark60,
  },
  searchText: {
    ...FONTS.h2,
    color: COLORS.light,
  },
});
