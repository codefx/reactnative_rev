import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import IconButton from '../../components/IconButton';
import icons from '../../constants/icons';

const SocialLogins = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.orLogin}>OR login with</Text>
      <View style={styles.buttonContainer}>
        <IconButton
          icon={icons.twitter}
          iconStyle={{tintColor: COLORS.dark}}
          containerStyle={styles.iconsContStyle}
        />
        <IconButton
          icon={icons.google}
          iconStyle={{tintColor: COLORS.dark}}
          containerStyle={styles.iconsContStyle}
        />
        <IconButton
          icon={icons.linkedin}
          iconStyle={{tintColor: COLORS.dark}}
          containerStyle={styles.iconsContStyle}
        />
      </View>
    </View>
  );
};

export default SocialLogins;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -30,
    zIndex: -1,
  },
  orLogin: {color: COLORS.dark, ...FONTS.body3},
  buttonContainer: {
    marginTop: 8,
    flexDirection: 'row',
    gap: SIZES.radius,
  },
  iconsContStyle: {
    width: 55,
    height: 55,
    alignitems: 'center',
    justifyContent: 'center',
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.grey20,
  },
});
