import {Image, StyleSheet, Text, View} from 'react-native';
import React, {useContext, useEffect, useState} from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import {AuthContext} from '../../contexts/AuthContext';
import {MotiView} from 'moti';
import {Shadow} from 'react-native-shadow-2';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FormInput from '../../components/FormInput';
import icons from '../../constants/icons';
import IconButton from '../../components/IconButton';
import CountryDropDown from '../../components/CountryDropDown';
import {getCountryDataV2} from '../../services/countryService';
import CountryModal from './CountryModal';
import CheckBox from '../../components/CheckBox';
import TextButton from '../../components/TextButton';

const SignUp = () => {
  const {
    animationState,
    name,
    setName,
    email,
    setEmail,
    phone,
    setPhone,
    password,
    setPassword,
    setCountries,
    selectedCountry,
    showCountryModal,
    setShowCountryModal,
  } = useContext(AuthContext);
  const [isVisible, setIsVisible] = useState(false);
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    // Fetch countires
    fetch('https://restcountries.com/v2/all')
      .then(response => response.json())
      .then(data => {
        let countryData = data.map(item => {
          return {
            code: item.alpha2Code,
            name: item.name,
            callingCode: `+${item.callingCodes[0]}`,
            flag: `https://flagcdn.com/w320/${item.alpha2Code.toLowerCase()}.jpg`,
          };
        });
        setCountries(countryData);
      });
  }, []);

  return (
    <MotiView state={animationState} style={styles.motiView}>
      <Shadow>
        <View style={styles.authContainer}>
          <Text style={styles.title}>Create New Account </Text>
          <KeyboardAwareScrollView
            enableOnAndroid={true}
            keyboardDismissMode="on-drag"
            keyboardShouldPersistTaps={'handled'}
            extraScrollHeight={-300}
            contentContainerStyle={styles.keyboardCont}>
            {/* name */}
            <FormInput
              containerStyle={styles.finputContainer}
              placeholder="name"
              value={name}
              onChange={text => setName(text)}
              prependComponent={
                <Image source={icons.person} style={styles.finputImage} />
              }
            />
            {/* email */}
            <FormInput
              containerStyle={styles.finputContainer}
              placeholder="Email"
              value={email}
              onChange={text => setEmail(text)}
              prependComponent={
                <Image source={icons.email} style={styles.finputImage} />
              }
            />
            {/* phone */}
            <FormInput
              containerStyle={styles.finputContainer}
              placeholder="Phone"
              value={phone}
              onChange={text => setPhone(text)}
              prependComponent={
                <Image source={icons.phone} style={styles.finputImage} />
              }
            />
            {/* Country */}
            <CountryDropDown
              selectedCountry={selectedCountry}
              onPress={() => setShowCountryModal(!showCountryModal)}
            />
            {/* password */}
            <FormInput
              containerStyle={styles.finputContainer}
              placeholder="Password"
              value={password}
              secureTextEntry={!isVisible}
              onChange={text => setPassword(text)}
              prependComponent={
                <Image source={icons.lock} style={styles.finputImage} />
              }
              appendComponent={
                <IconButton
                  icon={isVisible ? icons.eye_off : icons.eye}
                  iconStyle={{tintColor: COLORS.grey}}
                  onPress={() => setIsVisible(!isVisible)}
                />
              }
            />
            {/* terms checkbox*/}
            <CheckBox
              isSelected={checked}
              onPress={() => setChecked(!checked)}
            />
          </KeyboardAwareScrollView>
          <TextButton
            label="Create Account"
            contentContainerStyle={styles.createAccountButton}
            labelStyle={{...FONTS.h3}}
            onPress={() => console.log('CreateAccount')}
          />
        </View>
      </Shadow>
      <CountryModal />
    </MotiView>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  motiView: {
    marginTop: SIZES.padding,
  },
  authContainer: {
    flex: 1,
    width: SIZES.width - SIZES.padding * 2,
    paddingVertical: SIZES.padding,
    paddingHorizontal: SIZES.radius,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.light,
    zIndex: 1,
  },
  title: {
    color: COLORS.dark,
    ...FONTS.h1,
  },
  keyboardCont: {
    flexGrow: 1,
    justifyContent: 'center',
    gap: SIZES.radius,
  },
  finputContainer: {
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.error,
  },
  finputImage: {
    width: 25,
    height: 25,
    marginRight: SIZES.base,
  },
  forgotPas: {
    view: {alignItems: 'flex-end'},
    button: {
      marginTop: SIZES.radius,
      backgroundColor: null,
    },
  },
  loginButton: {
    height: 55,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary,
  },
  createAccountButton: {
    height: 55,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary,
  },
});
