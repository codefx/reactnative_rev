import {StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import strConsts from '../../constants/stringConstants';
import {AuthContext} from '../../contexts/AuthContext';
import TextButton from '../../components/TextButton';

const AuthFooter = () => {
  const {mode, animationState, setMode} = useContext(AuthContext);
  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        {mode == strConsts.SIGN_IN
          ? "Don't have an account?"
          : 'I already have an account.'}
      </Text>
      <TextButton
        label={mode == strConsts.SIGN_IN ? 'Create new account?' : 'Sign In'}
        contentContainerStyle={styles.tbutton.cntnrStyle}
        labelStyle={styles.tbutton.lablStyle}
        onPress={() => {
          if (animationState.current === strConsts.SIGN_IN) {
            animationState.transitionTo(strConsts.SIGN_UP);
            setMode(strConsts.SIGN_UP);
          } else {
            animationState.transitionTo(strConsts.SIGN_IN);
            setMode(strConsts.SIGN_IN);
          }
        }}
      />
    </View>
  );
};

export default AuthFooter;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 80,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginTop: -30,
    marginHorizontal: SIZES.radius,
    paddingBottom: SIZES.radius,
    borderBottomLeftRadius: SIZES.radius,
    borderBottomRightRadius: SIZES.radius,
    backgroundColor: COLORS.light60,
    zIndex: 0,
  },
  text: {
    color: COLORS.grey,
    ...FONTS.body5,
  },
  tbutton: {
    cntnrStyle: {marginLeft: SIZES.base, backgroundColor: null},
    lablStyle: {color: COLORS.support3, ...FONTS.h5},
  },
});
