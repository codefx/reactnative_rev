import {
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useContext, useEffect} from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {AuthContext} from '../../contexts/AuthContext';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import {getCountryDataV2} from '../../services/countryService';

const CountryModal = () => {
  const {
    showCountryModal,
    setShowCountryModal,
    setSelectedCountry,
    countries,
    setCountries,
  } = useContext(AuthContext);

  return (
    <Modal animationType="slide" transparent={true} visible={showCountryModal}>
      <TouchableWithoutFeedback onPress={() => setShowCountryModal(false)}>
        <View style={styles.pressableContainer}>
          <View style={styles.pressableMainview}>
            <FlatList
              data={countries}
              keyExtractor={item => item.code}
              contentContainerStyle={{
                paddingHorizontal: SIZES.padding,
                paddingBottom: SIZES.padding,
              }}
              renderItem={({item}) => {
                return (
                  <TouchableOpacity
                    style={styles.pressable}
                    onPress={() => {
                      console.log(item);
                      setSelectedCountry(item);
                      setShowCountryModal(false);
                    }}>
                    <Image
                      source={{uri: item.flag}}
                      resizeMode="contain"
                      style={styles.image}
                    />
                    <Text style={styles.text}>{item.name}</Text>
                  </TouchableOpacity>
                );
              }}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default CountryModal;

const styles = StyleSheet.create({
  pressableContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.dark80,
  },
  pressableMainview: {
    height: 400,
    width: SIZES.width * 0.8,
    backgroundColor: COLORS.light,
    borderRadius: SIZES.radius,
  },
  pressable: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: SIZES.radius,
  },
  image: {
    width: 30,
    height: 30,
  },
  text: {
    flex: 1,
    marginLeft: SIZES.radius,
    ...FONTS.body3,
  },
});
