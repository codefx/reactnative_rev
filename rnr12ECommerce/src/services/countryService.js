import {countriesUrl, countryFlagsUrl} from '../constants/urls';
// import axios from 'axios';

// export const getCountryDataV3 = async () => {
//   const areasData = await axios.get(countriesUrl).then(resp => resp.data);
//   const data = areasData.map(item => {
//     let suffixes;
//     try {
//       if (item.idd.suffixes.length > 1) {
//         suffixes = item.idd.suffixes[0];
//       } else {
//         suffixes = item.idd.suffixes;
//       }
//     } catch (e) {
//       console.log(e.message);
//     }

//     return {
//       code: item.cca2,
//       name: item.name.common,
//       callingCode: `${item.idd.root}-${suffixes}`,
//       flag: `${countryFlagsUrl}${item.cca2.toLowerCase()}.jpg`,
//     };
//   });

//   return data;
// };
export const getCountryDataV2 = () => {
  let countryData;
  fetch('https://restcountries.com/v2/all')
    .then(response => response.json())
    .then(data => {
      countryData = data.map(item => {
        return {
          code: item.alpha2Code,
          name: item.name,
          callingCode: `+${item.callingCodes[0]}`,
          flag: `https://flagcdn.com/w320/${item.alpha2Code.toLowerCase()}.jpg`,
        };
      });
    });

  return countryData;
};
