import React, {createContext, useRef, useState} from 'react';
import {Animated} from 'react-native';

export const WalktroughContext = createContext({});

const WalktroughCtxProvider = ({children}) => {
  const scrollX = React.useRef(new Animated.Value(0)).current;
  const [slidesSecondAnimate, setSlidesSecondAnimate] = useState(false);
  const onViewChangeRef = useRef(({viewableItems, changed}) => {
    if (viewableItems[0].index == 1) {
      setSlidesSecondAnimate(true);
    }
  });

  const values = {
    scrollX,
    slidesSecondAnimate,
    setSlidesSecondAnimate,
    onViewChangeRef,
  };
  return (
    <WalktroughContext.Provider value={values}>
      {children}
    </WalktroughContext.Provider>
  );
};

export default WalktroughCtxProvider;
