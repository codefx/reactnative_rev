import {createContext, useState} from 'react';

export const AuthContext = createContext({});

import React from 'react';
import {useAnimationState} from 'moti';
import {SIZES} from '../theme/theme';
import strConsts from '../constants/stringConstants';

const AuthCtxProvider = ({children}) => {
  const [mode, setMode] = useState(strConsts.SIGN_IN);
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');

  // Country
  const [countries, setCountries] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [showCountryModal, setShowCountryModal] = useState(false);

  const animationState = useAnimationState({
    signIn: {height: SIZES.height * 0.55},
    signUp: {height: SIZES.height * 0.7},
  });

  const values = {
    mode,
    setMode,
    animationState,
    email,
    setEmail,
    name,
    setName,
    phone,
    setPhone,
    password,
    setPassword,
    countries,
    setCountries,
    selectedCountry,
    setSelectedCountry,
    showCountryModal,
    setShowCountryModal,
  };
  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
};

export default AuthCtxProvider;
