import React from 'react';

import ExpenseCtxProvider from './src/contexts/expenseContext';
import Routes from './src/navigations/Routes';

const App = () => {
  return (
    <ExpenseCtxProvider>
      <Routes />
    </ExpenseCtxProvider>
  );
};

export default App;
