import React, {useContext, useRef} from 'react';
import {SafeAreaView, StyleSheet, ScrollView, View} from 'react-native';

import NavBar from '../components/home/NavBar';
import Header from '../components/home/Header';
import CategoryHeaderSection from '../components/home/CategoryHeaderSection';
import CategoryList from '../components/home/CategoryList';
import IncomingExpenses from '../components/home/IncomingExpenses';
import ExpenseChart from '../components/home/ExpenseChart';
import {ExpenseContext} from '../contexts/expenseContext';
import ExpenseSummary from '../components/home/ExpenseSummary';
import {COLORS} from '../theme/theme';
import NewChart from '../components/home/NewChart';

const HomeScreen = () => {
  const {viewMode} = useContext(ExpenseContext);
  return (
    <SafeAreaView style={styles.mainView}>
      {/* Nav bar section */}
      <NavBar />
      {/* Header section */}
      <Header />
      {/* Category Header Section */}
      <CategoryHeaderSection />
      <ScrollView contentContainerStyle={styles.scrollView}>
        {viewMode === 'list' && (
          <View>
            <CategoryList />
            <IncomingExpenses />
          </View>
        )}
        {viewMode === 'chart' && (
          <View>
            {/* <ExpenseChart /> */}
            <NewChart />
            {/* <ExpenseSummary /> */}
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainView: {flex: 1, backgroundColor: COLORS.lightGray2},
  scrollView: {paddingBottom: 60},
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
});

export default HomeScreen;
