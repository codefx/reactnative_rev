import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import {ExpenseContext} from '../../contexts/expenseContext';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import icons from '../icons';
import IncomingExpensesTitle from './IncomingExpensesTitle';

const IncomingExpenses = () => {
  const {selectedCategory} = useContext(ExpenseContext);

  let allExpenses = selectedCategory ? selectedCategory.expenses : [];
  let incomingExpenses = allExpenses.filter(a => a.status == 'P');

  const renderItem = ({item, index}) => (
    <View
      style={{
        ...styles.renderItemMainView,
        marginLeft: index == 0 ? SIZES.padding : 0,
      }}>
      {/* Title */}
      <View style={styles.renderItemTitleMainView}>
        <View style={styles.renderItemTitleView}>
          <Image
            source={selectedCategory.icon}
            style={{
              ...styles.renderItemTitleImage,
              tintColor: selectedCategory.color,
            }}
          />
        </View>
        <Text
          style={{
            ...styles.renderItemTitleText,
            color: selectedCategory.color,
          }}>
          {selectedCategory.name}
        </Text>
      </View>

      {/* Expense Description */}
      <View style={styles.descriptionMainView}>
        {/* Title and description */}
        <Text style={styles.descriptionTitleBigText}>{item.title}</Text>
        <Text style={styles.descriptionText}>{item.description}</Text>

        {/* Location */}
        <Text style={styles.locationTitleText}>Location</Text>
        <View style={styles.locationView}>
          <Image source={icons.pin} style={styles.locationIcon} />
          <Text style={styles.locationText}>{item.location}</Text>
        </View>
      </View>

      {/* Price */}
      <View
        style={{...styles.priceView, backgroundColor: selectedCategory.color}}>
        <Text style={styles.priceText}>
          CONFIRM {item.total.toFixed(2)} USD
        </Text>
      </View>
    </View>
  );

  return (
    <View>
      <IncomingExpensesTitle />
      {incomingExpenses.length > 0 && (
        <FlatList
          data={incomingExpenses}
          renderItem={renderItem}
          keyExtractor={item => `${item.id}`}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      )}
      {incomingExpenses.length == 0 && (
        <View style={styles.mainViewZero}>
          <Text style={styles.mainTextZero}>No Record</Text>
        </View>
      )}
    </View>
  );
};

export default IncomingExpenses;

const styles = StyleSheet.create({
  renderItemMainView: {
    width: 300,
    marginRight: SIZES.padding,

    marginVertical: SIZES.radius,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.white,
    shadow: {
      shadowColor: '#000',
      shadowOffset: {
        width: 2,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 3,
    },
  },
  renderItemTitleMainView: {
    flexDirection: 'row',
    padding: SIZES.padding,
    alignItems: 'center',
  },
  renderItemTitleView: {
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: COLORS.lightGray,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: SIZES.base,
  },
  renderItemTitleImage: {
    width: 30,
    height: 30,
  },
  renderItemTitleText: {...FONTS.h3},
  descriptionMainView: {paddingHorizontal: SIZES.padding},
  descriptionTitleBigText: {...FONTS.h2},
  descriptionText: {...FONTS.body3, flexWrap: 'wrap', color: COLORS.darkgray},
  locationTitleText: {marginTop: SIZES.padding, ...FONTS.h4},
  locationIcon: {
    width: 20,
    height: 20,
    tintColor: COLORS.darkgray,
    marginRight: 5,
  },
  locationView: {flexDirection: 'row'},
  locationText: {
    marginBottom: SIZES.base,
    color: COLORS.darkgray,
    ...FONTS.body4,
  },

  priceView: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomStartRadius: SIZES.radius,
    borderBottomEndRadius: SIZES.radius,
  },
  priceText: {color: COLORS.white, ...FONTS.body3},

  mainViewZero: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 300,
  },
  mainTextZero: {color: COLORS.primary, ...FONTS.h3},
});
