import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';

const IncomingExpensesTitle = () => {
  return (
    <View style={styles.mainView}>
      {/* Title */}
      <Text style={styles.textBig}>INCOMING EXPENSES</Text>
      <Text style={styles.textInfo}>12 Total</Text>
    </View>
  );
};

export default IncomingExpensesTitle;

const styles = StyleSheet.create({
  mainView: {
    height: 80,
    backgroundColor: COLORS.lightGray2,
    padding: SIZES.padding,
  },
  textBig: {...FONTS.h3, color: COLORS.primary},
  textInfo: {...FONTS.body4, color: COLORS.darkgray},
});
