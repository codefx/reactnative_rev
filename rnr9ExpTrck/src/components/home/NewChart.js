import {StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import {VictoryLabel, VictoryPie, VictoryTheme} from 'victory-native';
import {Svg} from 'react-native-svg';

import {ExpenseContext} from '../../contexts/expenseContext';

const NewChart = () => {
  const {processCategoryDataToDisplay, vicDataPie} = useContext(ExpenseContext);
  let chartData = processCategoryDataToDisplay();
  let vicData = vicDataPie();
  return (
    <Svg viewBox="0 0 400 400">
      <VictoryPie
        animate={{
          duration: 2000,
        }}
        standalone={false}
        width={400}
        height={400}
        data={vicData}
        innerRadius={66}
        labelRadius={95}
        style={{
          data: {
            fillOpacity: 0.9,
            stroke: '#c43a31',
            strokeWidth: 3,
          },
          labels: {
            fontSize: 25,
            fill: '#c43a31',
          },
        }}
        theme={VictoryTheme.material}
      />
      <VictoryLabel
        textAnchor="middle"
        style={{fontSize: 20}}
        x={200}
        y={200}
        text="Pie!"
      />
    </Svg>
  );
};

export default NewChart;

const styles = StyleSheet.create({});

/*
{
        label: `${percentage}%`,
        y: Number(item.y),
        expenseCount: item.expenseCount,
        color: item.color,
        name: item.name,
        id: item.id,
      }
*/
