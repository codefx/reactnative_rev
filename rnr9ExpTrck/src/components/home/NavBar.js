import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import icons from '../icons';
import {COLORS, SIZES} from '../../theme/theme';

const NavBar = () => {
  return (
    <View style={styles.mainView}>
      <TouchableOpacity
        style={styles.leftPressable}
        onPress={() => console.log('Go Back')}>
        <Image source={icons.back_arrow} style={styles.leftIcon} />
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.rightPressable}
        onPress={() => console.log('More')}>
        <Image source={icons.more} style={styles.rightIcon} />
      </TouchableOpacity>
    </View>
  );
};

export default NavBar;

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    height: 80,
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingHorizontal: SIZES.padding,
    backgroundColor: COLORS.white,
  },
  leftPressable: {justifyContent: 'center', width: 50},
  leftIcon: {width: 30, height: 30, tintColor: COLORS.primary},
  rightIcon: {width: 30, height: 30, tintColor: COLORS.primary},
  rightPressable: {justifyContent: 'center', alignItems: 'flex-end', width: 50},
});
