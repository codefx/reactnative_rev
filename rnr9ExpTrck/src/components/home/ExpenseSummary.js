import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useContext} from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import {ExpenseContext} from '../../contexts/expenseContext';

const ExpenseSummary = () => {
  const {
    selectedCategory,
    processCategoryDataToDisplay,
    setSelectCategoryByName,
  } = useContext(ExpenseContext);
  let data = processCategoryDataToDisplay();

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        ...styles.renderItemPressable,
        backgroundColor:
          selectedCategory && selectedCategory.name == item.name
            ? item.color
            : COLORS.white,
      }}
      onPress={() => {
        let categoryName = item.name;
        setSelectCategoryByName(categoryName);
      }}>
      {/* Name/Category */}
      <View style={styles.renderItemPressableCategoryView}>
        <View
          style={{
            ...styles.renderItemPressableCategorySubView,
            backgroundColor:
              selectedCategory && selectedCategory.name == item.name
                ? COLORS.white
                : item.color,
          }}
        />
        <Text
          style={{
            ...styles.renderItemPressableCategoryText,
            color:
              selectedCategory && selectedCategory.name === item.name
                ? COLORS.white
                : COLORS.primary,
          }}>
          {item.name}
        </Text>
      </View>

      {/* Expenses */}
      <View style={styles.expensesView}>
        <Text
          style={{
            color:
              selectedCategory && selectedCategory.name == item.name
                ? COLORS.white
                : COLORS.primary,
            ...FONTS.h3,
          }}>
          {item.y} USD - {item.label}
        </Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.mainView}>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => `${item.id}`}
      />
    </View>
  );
};
export default ExpenseSummary;

const styles = StyleSheet.create({
  renderItemPressable: {
    flexDirection: 'row',
    height: 40,
    paddingHorizontal: SIZES.radius,
    borderRadius: 10,
  },
  renderItemPressableCategoryView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  renderItemPressableCategorySubView: {
    width: 20,
    height: 20,
    borderRadius: 5,
  },
  renderItemPressableCategoryText: {marginLeft: SIZES.base, ...FONTS.h3},
  expensesView: {justifyContent: 'center'},
  mainView: {padding: SIZES.padding},
});
