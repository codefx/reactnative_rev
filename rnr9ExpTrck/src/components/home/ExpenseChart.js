import React, {useContext} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {VictoryPie} from 'victory-native';
import {FONTS, SIZES} from '../../theme/theme';
import {ExpenseContext} from '../../contexts/expenseContext';
import {Svg, RNSVGSvgAndroid} from 'react-native-svg';

const ExpenseChart = () => {
  const {
    selectedCategory,
    processCategoryDataToDisplay,
    setSelectCategoryByName,
  } = useContext(ExpenseContext);
  let chartData = processCategoryDataToDisplay();
  let colorScales = chartData.map(item => item.color);
  let totalExpenseCount = chartData.reduce(
    (a, b) => a + (b.expenseCount || 0),
    0,
  );

  console.log('Check Chart');
  console.log(chartData);

  if (Platform.OS === 'ios') {
    return (
      <View style={styles.mainView}>
        <VictoryPie
          data={chartData}
          labels={datum => `y: ${datum.y}`}
          radius={({datum}) =>
            selectedCategory && selectedCategory.name == datum.name
              ? SIZES.width * 0.4
              : SIZES.width * 0.4 - 10
          }
          innerRadius={70}
          labelRadius={({innerRadius}) =>
            (SIZES.width * 0.4 + innerRadius) / 2.5
          }
          style={{
            labels: {fill: 'white', ...FONTS.body3},
            parent: {
              ...styles.shadow,
            },
          }}
          width={SIZES.width * 0.8}
          height={SIZES.width * 0.8}
          colorScale={colorScales}
          events={[
            {
              target: 'data',
              eventHandlers: {
                onPress: () => {
                  return [
                    {
                      target: 'labels',
                      mutation: props => {
                        let categoryName = chartData[props.index].name;
                        setSelectCategoryByName(categoryName);
                      },
                    },
                  ];
                },
              },
            },
          ]}
        />

        <View style={styles.totalExpView}>
          <Text style={styles.totalExpCountText}>{totalExpenseCount}</Text>
          <Text style={styles.expensesText}>Expenses</Text>
        </View>
      </View>
    );
  } else {
    // Android workaround by wrapping VictoryPie with SVG
    return (
      <View style={styles.mainView}>
        <Svg width={SIZES.width} height={SIZES.width} style={styles.svgAnd}>
          <VictoryPie
            standalone={false} // Android workaround
            data={chartData}
            labels={datum => `y: ${datum.y}`}
            radius={({datum}) =>
              selectedCategory && selectedCategory.name == datum.name
                ? SIZES.width * 0.4
                : SIZES.width * 0.4 - 10
            }
            innerRadius={70}
            labelRadius={({innerRadius}) =>
              (SIZES.width * 0.4 + innerRadius) / 2.5
            }
            style={{
              labels: {fill: 'white', ...FONTS.body3},
              parent: {
                ...styles.shadow,
              },
            }}
            width={SIZES.width}
            height={SIZES.width}
            colorScale={colorScales}
            events={[
              {
                target: 'data',
                eventHandlers: {
                  onPress: () => {
                    return [
                      {
                        target: 'labels',
                        mutation: props => {
                          let categoryName = chartData[props.index].name;
                          setSelectCategoryByName(categoryName);
                        },
                      },
                    ];
                  },
                },
              },
            ]}
          />
        </Svg>
        <View style={styles.totalExpView}>
          <Text style={styles.totalExpCountText}>{totalExpenseCount}</Text>
          <Text style={styles.expensesText}>Expenses</Text>
        </View>
      </View>
    );
  }
};

export default ExpenseChart;

const styles = StyleSheet.create({
  mainView: {alignItems: 'center', justifyContent: 'center'},
  totalExpView: {position: 'absolute', top: '42%', left: '42%'},
  totalExpCountText: {...FONTS.h1, textAlign: 'center'},
  expensesText: {...FONTS.body3, textAlign: 'center'},
  svgAnd: {width: '100%', height: 'auto'},
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
});
