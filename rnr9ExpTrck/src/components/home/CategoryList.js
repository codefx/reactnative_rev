import {
  Animated,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useContext, useRef} from 'react';
import {ExpenseContext} from '../../contexts/expenseContext';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import icons from '../icons';

const CategoryList = () => {
  const categoryListHeightAnimationValue = useRef(
    new Animated.Value(115),
  ).current;

  const {setSelectedCategory, categories, showMoreToggle, setShowMoreToggle} =
    useContext(ExpenseContext);

  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => setSelectedCategory(item)}
      style={styles.renderItemPressable}>
      <Image
        source={item.icon}
        style={{...styles.renderItemImage, tintColor: item.color}}
      />
      <Text style={styles.renderItemText}>{item.name}</Text>
    </TouchableOpacity>
  );

  return (
    <View style={styles.mainView}>
      <Animated.View style={{height: categoryListHeightAnimationValue}}>
        <FlatList
          data={categories}
          renderItem={renderItem}
          keyExtractor={item => `${item.id}`}
          numColumns={2}
        />
      </Animated.View>

      <TouchableOpacity
        style={styles.mainPressable}
        onPress={() => {
          if (showMoreToggle) {
            Animated.timing(categoryListHeightAnimationValue, {
              toValue: 115,
              duration: 500,
              useNativeDriver: false,
            }).start();
          } else {
            Animated.timing(categoryListHeightAnimationValue, {
              toValue: 172.5,
              duration: 500,
              useNativeDriver: false,
            }).start();
          }

          setShowMoreToggle(!showMoreToggle);
        }}>
        <Text style={styles.mainPressableText}>
          {showMoreToggle ? 'LESS' : 'MORE'}
        </Text>
        <Image
          source={showMoreToggle ? icons.up_arrow : icons.down_arrow}
          style={styles.mainPressableImage}
        />
      </TouchableOpacity>
    </View>
  );
};

export default CategoryList;

const styles = StyleSheet.create({
  renderItemPressable: {
    flex: 1,
    flexDirection: 'row',
    margin: 5,
    paddingVertical: SIZES.radius,
    paddingHorizontal: SIZES.padding,
    borderRadius: 5,
    backgroundColor: COLORS.white,
    shadow: {
      shadowColor: '#000',
      shadowOffset: {
        width: 2,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 3,
    },
  },
  renderItemImage: {
    width: 20,
    height: 20,
  },
  renderItemText: {marginLeft: SIZES.base, color: COLORS.primary, ...FONTS.h4},
  mainView: {paddingHorizontal: SIZES.padding - 5},
  mainPressable: {
    flexDirection: 'row',
    marginVertical: SIZES.base,
    justifyContent: 'center',
  },
  mainPressableText: {...FONTS.body4},
  mainPressableImage: {
    marginLeft: 5,
    width: 15,
    height: 15,
    alignSelf: 'center',
  },
});
