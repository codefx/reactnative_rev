import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import icons from '../icons';

const Header = () => {
  return (
    <View style={styles.mainView}>
      <View>
        <Text style={styles.view1Text1}>My Expenses</Text>
        <Text style={styles.view1Text2}>Summary (private)</Text>
      </View>

      <View style={styles.view2}>
        <View style={styles.view2View1}>
          <Image source={icons.calendar} style={styles.image} />
        </View>

        <View style={styles.view2View2}>
          <Text style={styles.view2View2Text1}>11 Nov, 2020</Text>
          <Text style={styles.view2View2Text2}>18% more than last month</Text>
        </View>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  mainView: {
    paddingHorizontal: SIZES.padding,
    paddingVertical: StatusBar.currentHeight,
    backgroundColor: COLORS.white,
  },
  view1Text1: {color: COLORS.primary, ...FONTS.h2},
  view1Text2: {...FONTS.h3, color: COLORS.darkgray},
  view2: {
    flexDirection: 'row',
    marginTop: SIZES.padding,
    alignItems: 'center',
  },
  view2View1: {
    backgroundColor: COLORS.lightGray,
    height: 50,
    width: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {width: 20, height: 20, tintColor: COLORS.lightBlue},
  view2View2: {marginLeft: SIZES.padding},
  view2View2Text1: {color: COLORS.primary, ...FONTS.h3},
  view2View2Text2: {...FONTS.body3, color: COLORS.darkgray},
});
