import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useContext} from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import {ExpenseContext} from '../../contexts/expenseContext';
import icons from '../icons';

const CategoryHeaderSection = () => {
  const {categories, viewMode, setViewMode} = useContext(ExpenseContext);
  return (
    <View style={styles.mainView}>
      {/* Title */}
      <View>
        <Text style={styles.titleText1}>CATEGORIES</Text>
        <Text style={styles.titleText2}>{categories.length} Total</Text>
      </View>

      {/* Button */}
      <View style={styles.buttonView}>
        <TouchableOpacity
          style={{
            ...styles.pressableChart,
            backgroundColor: viewMode === 'chart' ? COLORS.secondary : null,
          }}
          onPress={() => setViewMode('chart')}>
          <Image
            source={icons.chart}
            resizeMode="contain"
            style={{
              ...styles.imageChart,
              tintColor: viewMode === 'chart' ? COLORS.white : COLORS.darkgray,
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            ...styles.pressableList,
            backgroundColor: viewMode === 'list' ? COLORS.secondary : null,
          }}
          onPress={() => setViewMode('list')}>
          <Image
            source={icons.menu}
            resizeMode="contain"
            style={{
              ...styles.imageList,
              tintColor: viewMode === 'list' ? COLORS.white : COLORS.darkgray,
            }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CategoryHeaderSection;

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    padding: SIZES.padding,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  titleText1: {color: COLORS.primary, ...FONTS.h3},
  titleText2: {color: COLORS.darkgray, ...FONTS.body4},
  buttonView: {flexDirection: 'row'},
  pressableChart: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  imageChart: {
    width: 20,
    height: 20,
  },
  pressableList: {
    alignItems: 'center',
    justifyContent: 'center',

    height: 50,
    width: 50,
    borderRadius: 25,
    marginLeft: SIZES.base,
  },
  imageList: {
    width: 20,
    height: 20,
  },
});
