import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  //StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  //useColorScheme,
  View,
} from 'react-native';

const App = () => {
  const [sayi, setSayi] = useState(0);

  return (
    <SafeAreaView
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={{
          flex: 1,
        }}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
          }}>
          <TextInput
            value={sayi}
            style={{
              borderColor: '#00f',
              borderWidth: 1,
              width: '100%',
              height: 60,
              borderRadius: 10,
            }}
          />
          <TouchableOpacity
            onPress={() => {}}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#0f0',
              padding: 5,
              width: '100%',
              height: 50,
              borderRadius: 25,
              marginTop: 10,
            }}>
            <Text>Hi From app</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default App;
