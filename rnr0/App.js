import React, {useRef, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  //StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  //useColorScheme,
  View,
} from 'react-native';

const App = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const checkUser = () => {
    if (email == 'cfx@cfx.com' && password == '123456') {
      alert('Successfully entered.');
    } else {
      alert('not successful operation.');
    }
  };
  return (
    <SafeAreaView
      style={{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
        }}>
        <TextInput
          placeholder="Enter your email"
          value={email}
          onChangeText={value => {
            setEmail(value);
          }}
          style={{
            borderColor: '#00f',
            borderWidth: 1,
            width: '100%',
            height: 60,
            borderRadius: 10,
            marginBottom: 10,
          }}
        />
        <TextInput
          secureTextEntry
          placeholder="Password"
          value={password}
          onChangeText={value => {
            setPassword(value);
          }}
          style={{
            borderColor: '#00f',
            borderWidth: 1,
            width: '100%',
            height: 60,
            borderRadius: 10,
          }}
        />
        <TouchableOpacity
          onPress={() => checkUser()}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#00f',
            padding: 5,
            width: '100%',
            height: 60,
            borderRadius: 25,
            marginTop: 10,
            marginBottom: 10,
          }}>
          <Text style={{color: '#fff', fontSize: 25}}>Giriş</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
export default App;
