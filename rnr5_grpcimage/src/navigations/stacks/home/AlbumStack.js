import React from "react";

import { Text, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";

import AlbumScreen from "../../../screens/album/AlbumScreen";
import SharedAlbumScreen from "../../../screens/album/SharedAlbumScreen";

const Stack = createStackNavigator();

const AlbumStack = () => {
  return (
    <Stack.Navigator initialRouteName="Albums">
      <Stack.Screen name="Albums" component={AlbumScreen} />
      <Stack.Screen name="Shared Album" component={SharedAlbumScreen} />
    </Stack.Navigator>
  );
};

export default AlbumStack;
