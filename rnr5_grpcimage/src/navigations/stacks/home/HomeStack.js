import React from 'react';

import {Text, View} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeBottomTabStack from './HomeBottomTabStack';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const HomeStack = () => {
  return (
    <Stack.Navigator initialRouteName="HomeBottomTabStack">
      <Stack.Screen
        name="HomeBottomTabStack"
        component={HomeBottomTabStack}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default HomeStack;
