import * as React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import GetImageScreen from './GetImageScreen';

const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, width: '100%', alignItems: 'center'}}>
      <View
        style={{
          //flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <Text
          style={{
            fontSize: 21,
            textAlign: 'center',
            marginBottom: 16,
          }}>
          You are on Home Screen
        </Text>
      </View>
      <ScrollView style={{flex: 3, width: '100%', padding: 16}}>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#bdf3bd',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Settings', {screen: 'Settings'})}>
          <Text style={{color: '#406cf5', fontSize: 20}}> Go to Settings</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#935966',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('AlbumStack')}>
          <Text style={{color: '#9fa9af', fontSize: 20}}>
            Open Albums Screen
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#bdf3bd',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('GetImageScreen')}>
          <Text style={{color: '#406cf5', fontSize: 20}}>
            {' '}
            Go to GetImageScreen
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
