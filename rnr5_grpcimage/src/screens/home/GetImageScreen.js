import React, {useRef, useState} from 'react';

import {SafeAreaView, Text, TextInput, View} from 'react-native';

import GetImageButton from '../../components/GetImageButton';
import {MCIcon} from '../../utils/vectorIconsPort';
import grpcClientService from '../../services/grpsService/clientImage';

const GetImageScreen = ({navigation}) => {
  const [text, setText] = useState('');
  const TextInputRef = useRef();
  const getImageGrpc = path => {
    grpcClientService(path);
  };
  return (
    <SafeAreaView
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
          width: '90%',
        }}>
        <MCIcon color="#4A97F5" name="lock-reset" size={80} />
        <Text
          style={{
            color: '#52b752',
            fontSize: 24,
            fontWeight: '400',
            marginBottom: 20,
            marginTop: 20,
          }}>
          Password Resetting Screen
        </Text>

        <>
          <TextInput
            ref={TextInputRef}
            placeholder="Enter your file name with extension"
            value={text}
            onChangeText={value => setText(value)}
            keyboardType="email-address"
            style={{
              borderColor: '#4A97F5',
              borderWidth: 1,
              width: '90%',
              height: 60,
              borderRadius: 10,
              margin: 10,
            }}
          />

          <GetImageButton onPressFunc={()=>getImageGrpc(TextInputRef.current.value)} title="Send Email" />
        </>
      </View>
    </SafeAreaView>
  );
};

export default GetImageScreen;
