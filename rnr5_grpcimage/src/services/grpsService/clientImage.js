import {Platform} from 'react-native';

const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');
import RNFS from 'react-native-fs';
import async from 'async';
import moment from 'moment';

const grpcClientService = filename => {
  // Path to proto file
  const PROTO_FILE = '../../protos/imageservice.proto';
  // Options needed for loading Proto file
  const options = {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
  };
  // Load Proto File
  const pkgDefs = protoLoader.loadSync(PROTO_FILE, options);
  // Load Definition into gRPC
  const ImageService = grpc.loadPackageDefinition(pkgDefs).ImageService;
  // Create the Client
  const client = new ImageService(
    'http://localhost:5236',
    grpc.credentials.createInsecure(),
  );
  const [fileName, fileExtension] = filename.split('.');
  var fileInfo = {
    FileExtension: '.mp4',
    FileName: 'qw',
  };
  var fileInfo2 = {
    FileExtension: fileExtension,
    FileName: fileName,
  };
  var timestamp = moment().utcOffset('+03:00').format('YYYY-MM-DD hh:mm:ss');
  const LOCAL_PATH_TO_FILE =
    Platform.OS === 'ios'
      ? `${RNFS.DocumentDirectoryPath}/${filename}-${timestamp}.${fileExtension}`
      : `${RNFS.ExternalDirectoryPath}/${filename}-${timestamp}.${fileExtension}`;

  const resultFile = client.FileDownload(fileInfo2, (error, file) => {
    if (error) {
      console.log(error);
    } else {
      console.log(file);
      RNFS.writeFile(LOCAL_PATH_TO_FILE, file, 'utf8')
        .then(success => {
          console.log('FILE WRITTEN!');
        })
        .catch(err => {
          console.log(err.message);
        });
    }
  });

  return resultFile;
};
export default grpcClientService;
