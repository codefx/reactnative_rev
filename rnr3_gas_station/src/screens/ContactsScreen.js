import React from 'react';
import {
  AppRegistry,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
//import {phonecall, email, web} from 'react-native-communications';
import Comms from 'react-native-communications';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import IconsAlternatives from '../components/IconsAlternatives'

const ContactsScreen = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, justifyContent: 'center'}}>
      <ScrollView style={{flex: 1, backgroundColor: '#1a7ed3'}}>
        <TouchableOpacity
          onPress={() => navigation.navigate('ContactsFormScreen')}
          style={{
            marginTop: 70,
            height: 80,
            flexDirection: 'row',
            backgroundColor: '#9070d4',
            alignItems: 'center',
            padding: 10,
            margin: 10,
            borderRadius: 30,
            borderWidth: 1,
            borderColor: '#ffffff',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              backgroundColor: '#1a7ed3',
              borderRadius: 40,
              width: 60,
              height: 60,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <MIcon name="edit" size={32} color="#fff" />
          </View>
          <View style={{flex: 1, margin: 10}}>
            <Text style={{color: '#ffffff', fontSize: 18}}>
              Oneri ve Sikayetlerinizi Yazabilirsiniz
            </Text>
          </View>
          <View
            style={{
              width: 70,
              height: 70,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <MIcon name="arrow-forward-ios" size={32} color="#f2e6ee" />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => Comms.phonecall('00905321645027', true)}
          style={{
            height: 80,
            flexDirection: 'row',
            backgroundColor: '#0070d4',
            alignItems: 'center',
            padding: 10,
            margin: 10,
            borderRadius: 30,
            borderWidth: 1,
            borderColor: '#ffffff',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              backgroundColor: '#1a7ed3',
              borderRadius: 40,
              width: 60,
              height: 60,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <MIcon name="phone" size={32} color="#fff" />
          </View>
          <View style={{flex: 1, margin: 16}}>
            <Text style={{color: '#fffFFF', fontSize: 24}}>
              +90 532 164 5027
            </Text>
          </View>
          <View
            style={{
              width: 60,
              height: 60,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <MIcon name="arrow-forward-ios" size={32} color="#f2e6e0" />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => Comms.email(['info@codeks.org'], null, null, null, null)}
          style={{
            height: 80,
            flexDirection: 'row',
            backgroundColor: '#9e7ed4',
            alignItems: 'center',
            padding: 10,
            margin: 10,
            borderRadius: 36,
            borderWidth: 1,
            borderColor: '#fffFFF',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              backgroundColor: '#1a7ed3',
              borderRadius: 40,
              width: 60,
              height: 60,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <MIcon name="alternate-email" size={32} color="#ffffff" />
          </View>
          <View style={{flex: 1, margin: 10}}>
            <Text style={{color: '#fffFFF', fontSize: 24}}>
              info@codeks.org
            </Text>
          </View>
          <View
            style={{
              width: 70,
              height: 70,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <MIcon name="arrow-forward-ios" size={32} color="#f2e600" />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => Comms.web('https://www.codeks.org')}
          style={{
            height: 80,
            flexDirection: 'row',
            backgroundColor: '#007ed4',
            alignItems: 'center',
            padding: 10,
            margin: 10,
            borderRadius: 30,
            borderWidth: 1,
            borderColor: '#ffffff',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              backgroundColor: '#1a7ed3',
              borderRadius: 40,
              width: 60,
              height: 60,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <MIcon name="web" size={32} color="#ffffff" />
          </View>
          <View style={{flex: 1, margin: 10}}>
            <Text style={{color: '#ffffFf', fontSize: 24}}>www.codeks.org</Text>
          </View>
          <View
            style={{
              width: 70,
              height: 70,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <MIcon name="arrow-forward-ios" size={32} color="#f2e600" />
          </View>
        </TouchableOpacity>
        <IconsAlternatives />
      </ScrollView>
    </SafeAreaView>
  );
};
export default ContactsScreen;
