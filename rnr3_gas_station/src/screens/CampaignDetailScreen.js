import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  //StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  //useColorScheme,
  View,
} from 'react-native';
import dims from '../utils/deviceDimensions';

const CampaignDetailScreen = ({route, navigation}) => {
  const item = route.params;
  return (
    <View
      style={{
        width: '100%',
        alignItems: 'center',
      }}>
      <Image
        style={{
          resizeMode: 'cover',
          width: '100%',
          height: dims.deviceHeight / 3,
          marginBottom: 15,
        }}
        source={item.image} //not uri, local and with require()
      />
      <Text style={{fontSize: 28, marginBottom: 15}}>{item.title}</Text>
      <Text style={{fontSize: 18}}>{item.detail}</Text>
    </View>
  );
};
export default CampaignDetailScreen;
