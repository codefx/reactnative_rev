import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  //StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  //useColorScheme,
  View,
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import DropDownPicker from 'react-native-dropdown-picker';
import RNSmtpMailer from 'react-native-smtp-mailer';

const ContactsFormScreen = ({route, navigation}) => {
  //const item = route.params;

  const [adi, setAdi] = useState('');
  const [adiValid, setAdiValid] = useState(false);
  const [adiValidMesaj, setAdiValidMesaj] = useState('Bos Gecilemez!');

  const [email, setEmail] = useState('');
  const [emailValid, setEmailValid] = useState(false);
  const [emailValidMesaj, setEmailValidMesaj] = useState('Bos Gecilemez!');

  const [telefon, setTelefon] = useState('');
  const [telefonValid, setTelefonValid] = useState(false);
  const [telefonValidMesaj, setTelefonValidMesaj] = useState('Bos Gecilemez!');

  const [konu, setKonu] = useState('');
  const [konuValid, setKonuValid] = useState(false);
  const [konuValidMesaj, setKonuValidMesaj] = useState('LUtfen Secim Yapiniz!');

  const [sozlesme, setSozlesme] = useState(false);
  const [mesaj, setMesaj] = useState('');
  const [mesajValid, setMesajValid] = useState(false);
  const [mesajValidMesaj, setMesajValidMesaj] = useState('Bos Gecilemez!');
  const checkAdi = text => {
    setAdi(text);
    if (text.length > 6) {
      setAdiValid(true);
    } else {
      setAdiValid(false);
    }
  };
  const checkEmail = text => {
    setEmail(text);
    if (text.length > 6) {
      const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (reg.test(text)) {
        setEmailValid(true);
      } else {
        setEmailValidMesaj('Gecerli bir email adresi giriniz!');
        setEmailValid(false);
      }
    } else {
      setEmailValid(false);
      setEmailValidMesaj('Bos Gecilemez!');
    }
  };
  const checkTelefon = text => {
    setTelefon(text);
    if (text.length > 0) {
      if (text.length == 10) {
        setTelefonValid(true);
      } else {
        setTelefonValidMesaj(
          'Telefon numaranizi basina 0 Koymadan 10 heneli olarak yaziniz',
        );
        setTelefonValid(false);
      }
    } else {
      setTelefonValid(false);
      setTelefonValidMesaj('Bos Gecilemez!');
    }
  };
  const checkKonu = value => {
    setKonu(value);
    if (value.length > 0) {
      setKonuValid(true);
    } else {
      setKonuValid(false);
    }
  };
  const checkMesaj = text => {
    setMesaj(text);
    if (text.length > 0) {
      setMesajValid(true);
    } else {
      setMesajValid(false);
    }
  };
  const mailGonder = async () => {
    await RNSmtpMailer.sendMail({
      mailhost: 'smtp.yandex.com.tr',
      port: '465',
      ssl: true, // optional. if false, then TLS is enabled. Its true by default in username: ‘test@codeks.org',
      username: 'test@codeks.org',
      password: '123456A+',
      fromName: 'Mehmet Kasim Sular', // optional replyTo: ‘info@codeks.org', // optional //recipients: 'toEmaili,toEmail2',
      bcc: [email], // optional
      subject: 'Opet App ',
      htmlBody: '<h1> Opet App | </h1><p>mesaj</p>',
      /*
      attachmentPaths: [
        RNFS.ExternalDirectoryPath + "/image.jpg",
        RNFS.DocumentDirectoryPath + "/test.txt",
        RNFS.DocumentDirectoryPath + "/test2.csv",
        RNFS.DocumentDirectoryPath + "/pdfFile.pdf",
        RNFS.DocumentDirectoryPath + "/zipFile.zip",
        RNFS.DocumentDirectoryPath + "/image.png"
      ], // optional
      attachmentNames: [
        'image.jpg',
        'firstFile.txt', I
        'secondFile.csv',
        'pdfFile.pdf',
        'zipExample.zip’,
        'pngImage.png',
      ], // required in android, these are renames of original files.
      */
    })
      .then(success => alert(success))
      .catch(err => alert(err));
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#dddddd'}}>
      <ScrollView style={{flex: 1}}>
        <View style={{margin: 20}}>
          <TextInput
            value={adi}
            onChangeText={text => checkAdi(text)}
            style={{
              backgroundColor: '#ffffff',
              borderRadius: 30,
              paddingLeft: 20,
              marginBottom: 10,
              height: 60,
            }}
            placeholder="Adiniz ve Soyadiniz *"
            fontSize={18}
            secureTextEntry={false}
          />
          {adiValid == false ? (
            <View style={{alignItems: 'center', marginBottom: 20}}>
              <Text style={{color: '#ff0000', fontSize: 12}}>
                {adiValidMesaj}
              </Text>
            </View>
          ) : (
            <></>
          )}
          <TextInput
            value={email}
            onChangeText={text => checkEmail(text)}
            style={{
              backgroundColor: '#ffffff',
              borderRadius: 30,
              paddingLeft: 20,
              marginBottom: 10,
              height: 60,
            }}
            placeholder="E-mail Adresiniz *"
            fontSize={18}
            keyboardType="email-address"
          />
          {emailValid == false ? (
            <View style={{alignItems: 'center', marginBottom: 26}}>
              <Text style={{color: '#ff0000', fontSize: 12}}>
                {emailValidMesaj}
              </Text>
            </View>
          ) : (
            <></>
          )}
          <TextInput
            value={telefon}
            onChangeText={text => checkTelefon(text)}
            style={{
              backgroundColor: '#ffffff',
              borderRadius: 30,
              paddingLeft: 20,
              marginBottom: 10,
              height: 60,
            }}
            placeholder="Telefon Numaraniz *"
            fontSize={18}
            keyboardType="phone- pad"
          />
          {telefonValid == false ? (
            <View style={{alignItems: 'center', marginBottom: 206}}>
              <Text style={{color: '#ff0000', fontSize: 12}}>
                {telefonValidMesaj}{' '}
              </Text>
            </View>
          ) : (
            <></>
          )}
          <DropDownPicker
            open={false}
            value={konu}
            setValue={value => setKonu(value)}
            items={[
              {label: 'Istek', value: 'Istek', hidden: true},
              {
                label: 'Sikayet',
                value: 'Sikayet',
              },
              {
                label: '‘Oneri',
                value: 'Oneri',
              },
              {
                label: 'Tesekkür',
                value: 'Tesekkur',
              },
            ]}
            placeholder="Konu Seciniz *"
            defaultValue={konu}
            containerStyle={{height: 60}}
            style={{backgroundColor: '#ffffff', MarginBottom: 10}}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            dropDownStyle={{backgroundColor: '#fafafa'}}
            onChangelItem={item => checkKonu(item.value)}
          />
          {konuValid == false ? (
            <View style={{alignItems: 'center', marginBottom: 20}}>
              <Text style={{color: '#ff0000', fontSize: 12}}>
                {konuValidMesaj}
              </Text>
            </View>
          ) : (
            <></>
          )}
          <TextInput
            value={mesaj}
            onChangeText={text => checkMesaj(text)}
            style={{
              textAlignVertical: 'top',
              backgroundColor: '#ffffff',
              borderRadius: 30,
              paddingLeft: 20,
              marginBottom: 16,
              height: 160,
            }}
            placeholder="Mesajiniz"
            fontSize={18}
            multiline={true}
            numberOfLines={4}
          />
          {mesajValid == false ? (
            <View style={{alignItems: 'center', marginBottom: 20}}>
              <Text style={{color: '#ff@000', fontSize: 12}}>
                {' '}
                {mesajValidMesaj}{' '}
              </Text>
            </View>
          ) : (
            <></>
          )}
          <CheckBox

            onPress={() => setSozlesme(!sozlesme)}
            title="Yukaridaki bilgilerin dogrulugunu onayliyorum"
            checked={sozlesme}
          />
          <TouchableOpacity
            onPress={() => mailGonder()}
            disabled={
              sozlesme &&
              adiValid &&
              emailValid &&
              telefonValid &&
              konuValid &&
              mesajValid
                ? false
                : true
            }
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor:
                sozlesme &&
                adiValid &&
                emailValid &&
                telefonValid &&
                konuValid &&
                mesajValid
                  ? '#0070d4'
                  : '#888',
              borderRadius: 30,
              paddingLeft: 20,
              marginVertical: 10,
              height: 60,
            }}>
            <Text style={{color: '#ffffff', fontSize: 24}}>Gonder</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default ContactsFormScreen;
