import React, {useRef, useState} from 'react';
import {
  FlatList,
  SafeAreaView,
  View,
} from 'react-native';
import {
  MyItemSeparatorComponent,
  MyRenderItem,
} from '../components/MyFlatListSubComponents';
import campaignData from '../data/campaignData';

const CampaignScreen = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          margin: 5,
          padding: 15,
        }}>
        <FlatList
          data={campaignData}
          itemSeparatorComponent={MyItemSeparatorComponent}
          renderItem={({item, index}) =>
            MyRenderItem({item, index}, {navigation}, 'CampaignDetailScreen')
          }
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </SafeAreaView>
  );
};
export default CampaignScreen;
