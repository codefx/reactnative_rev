import * as React from 'react';
import {TouchableOpacity, View, Text, SafeAreaView, Image} from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialIcons';
//import gs1 from '../assets/imgs/gas_station_1.jpg';
//import gs2 from '../assets/imgs/gas_station_2.jpg';
//import gs3 from '../assets/imgs/gas_s5.png';
import dims from '../utils/deviceDimensions';
import {FlatListSlider} from 'react-native-flatlist-slider';

// const deviceWidth = Dimensions.get('window').width;
// const deviceHeight = Dimensions.get('window').height;

const gs1 = '../assets/imgs/gas_station_1.jpg';
const gs2 = '../assets/imgs/gas_station_2.jpg';
const gs3 = '../assets/imgs/gas_s5.png';

const images = [
  //local ise banner: remote ise image:
  {banner: require(gs1), desc: 'future station'},
  {banner: require(gs2), desc: 'future station 2'},
  {banner: require(gs3), desc: 'future Brand'},
];

const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        width: '100%',
        alignItems: 'center',
        backgroundColor: 'rgba(2,113,205,0.7)',
      }}>
      <View /*Main View*/
        style={{
          flex: 1,
          width: '100%',
        }}>
        {/*Head section*/}
        <View
          style={{
            flex: 1,
            height: dims.deviceHeight / 2,
          }}>
          <FlatListSlider
            data={images}
            imageKey={'banner'}
            local={true}
            height={dims.deviceHeight / 2}
            //width={dims.deviceWidth}
            timer={5000}
            onPress={item => alert(JSON.stringify(item))}
            contentContainerStyle={{paddingHorizontal: 0}}
            indicatorContainerStyle={{position: 'absolute', bottom: 20}}
            indicatorActiveColor={'#8e44ad'}
            indicatorInActiveColor={'#ffffff'}
            indicatorActiveWidth={30}
            animation
          />
          {/*<Image*/}
          {/*  style={{*/}
          {/*    resizeMode: 'cover',*/}
          {/*    width: dims.deviceWidth,*/}
          {/*    height: dims.deviceHeight / 2,*/}
          {/*  }}*/}
          {/*  source={gs1}*/}
          {/*/>*/}
        </View>
        {/*Middle section*/}
        <View
          style={{
            flex: 1,
            height: dims.deviceHeight / 2,
            width: '100%',
          }}>
          <View /*first row */
            style={{
              flexDirection: 'row',
              flex: 1,
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View /*first row 1. cell*/
              style={{
                flex: 1,
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                borderRightWidth: 1,
                borderRightColor: 'rgba(255,255,255,0.4)',
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(255,255,255,0.4)',
                height: dims.deviceHeight / 4,
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 18,
                }}>
                Nearest station
              </Text>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 24,
                }}>
                3.5
              </Text>
              <Text style={{color: '#fff', fontSize: 18}}>KM</Text>
            </View>
            <View /*first row 2. cell*/
              style={{
                flex: 1,
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(255,255,255,0.4)',
                height: dims.deviceHeight / 4,
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 18,
                }}>
                Smart Filling
              </Text>
              <MIcon name="local-gas-station" size={32} color="#fff" />
            </View>
          </View>
          <View /*second row*/
            style={{
              flexDirection: 'row',
              flex: 1,
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View /*second row 1. cell*/
              style={{
                flex: 1,
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                borderRightWidth: 1,
                borderRightColor: 'rgba(255,255,255,0.4)',
                height: dims.deviceHeight / 4,
              }}>
              <Text style={{color: '#fff', fontSize: 18}}>Gasoline</Text>
              <Text style={{color: '#fff', fontSize: 24}}>26</Text>
              <Text style={{color: '#fff', fontSize: 18}}>TL</Text>
            </View>
            <View /*second row 2. cell*/
              style={{
                flex: 1,
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                height: dims.deviceHeight / 4,
              }}>
              <Text style={{color: '#fff', fontSize: 18}}>Payment</Text>
              <MIcon name="credit-card" size={24} color="#fff" />
            </View>
          </View>
        </View>
        {/*Bottom section*/}
        <TouchableOpacity onPress={() => navigation.navigate('CampaignScreen')}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              //position: 'absolute',
              bottom: 0,
              height: 50,
              width: '100%',
              borderTopWidth: 1,
              borderTopColor: 'rgba(255,255,255,0.4)',
              paddingLeft: 16,
              paddingRight: 16,
              backgroundColor: 'rgba(2,113,205,1)',
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 18,
              }}>
              Campaigns
            </Text>
            <MIcon name="chevron-right" size={18} color="#fff" />
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;
