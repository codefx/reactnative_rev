import React from 'react';

import Routes from './navigation/routes';
import { AppRegistry } from "react-native";
import ContactsScreen from "./screens/ContactsScreen";

const App = () => {
  return <Routes />;
};
export default App;


