import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

import MIcon from 'react-native-vector-icons/MaterialIcons';
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Fa5Icon from 'react-native-vector-icons/FontAwesome5';

import HomeScreen from '../screens/HomeScreen';
import MyDrawer from './drawers';
import CampaignDetailScreen from '../screens/CampaignDetailScreen';
import ContactsScreen from '../screens/ContactsScreen';
import ContactsFormScreen from '../screens/ContactsFormScreen';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="MyDrawer"
          component={MyDrawer}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen //not in drawer menu navs
          name="CampaignDetailScreen"
          component={CampaignDetailScreen}
          options={{
            title: 'Campaign Details',
            headerStyle: {
              backgroundColor: '#4ba9f6',
            },
            headerTintColor: '#fff0ff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen // not in drawer menu navs
          name="ContactsFormScreen"
          component={ContactsFormScreen}
          options={{
            title: 'Contact Form',
            headerStyle: {
              backgroundColor: '#4ba9f6',
            },
            headerTintColor: '#fff0ff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
