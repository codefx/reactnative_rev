import React from 'react';
import 'react-native-gesture-handler';

import {createDrawerNavigator} from '@react-navigation/drawer';

import MIcon from 'react-native-vector-icons/MaterialIcons';
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Fa5Icon from 'react-native-vector-icons/FontAwesome5';

import {
  CampaignScreenStack,
  ContactsScreenStack,
  HomeScreenStack,
} from '../stacks';

const Drawer = createDrawerNavigator();
const MyDrawer = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        drawerStyle: {
          backgroundColor: 'rgba(2,113,205,0.9)',
        },
        drawerActiveTintColor: '#fff',
        drawerInactiveTintColor: '#999',
        headerShown: false,
        headerStyle: {
          //backgroundColor: 'rgba(2,113,205,1)',
          borderBottomColor: '#fff',
          borderBottomWidth: 1,
        },
        itemStyle: {marginVertical: 5},
        labelStyle: {fontSize: 18},
      }}>
      <Drawer.Screen
        name="HomeScreen"
        component={HomeScreenStack}
        options={{
          drawerLabel: 'Home Page',
          drawerIcon: ({focused}) => (
            <MIcon name="home" size={24} color="#67d956" />
          ),
        }}
      />
      <Drawer.Screen
        name="CampaignScreen"
        component={CampaignScreenStack}
        options={{
          drawerLabel: 'Campaigns',
          drawerIcon: ({focused}) => (
            <MIcon name="campaign" size={24} color="#67d956" />
          ),
        }}
      />
      <Drawer.Screen
        name="ContactsScreen"
        component={ContactsScreenStack}
        options={{
          drawerLabel: 'Contacts',
          drawerIcon: ({focused}) => (
            <MIcon name="contact-phone" size={24} color="#67d956" />
          ),
        }}
      />
    </Drawer.Navigator>
  );
};

export default MyDrawer;
