import React from 'react';

import {Image, Text, TouchableOpacity, View} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../../screens/HomeScreen';
import myLogo from '../../assets/imgs/mylogo.png';
import drawerWhite from '../../assets/imgs/drawerWhite.png';
import CampaignScreen from '../../screens/CampaignScreen';
import ContactsScreen from '../../screens/ContactsScreen';

const Stack = createStackNavigator();
//const logo= myLogo;
export const HomeScreenStack = ({navigation}) => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: '',
          headerLeft: () => (
            <Image
              style={{width: 70, height: 24, marginLeft: 10}}
              source={myLogo}
            />
          ),
          headerRight: () => (
            <NavigationDrawerStructure navigationProps={navigation} />
          ),
          headerTransparent: true,
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
};

export const CampaignScreenStack = ({navigation}) => {
  return (
    <Stack.Navigator initialRouteName="Campaign">
      <Stack.Screen
        name="Campaign"
        component={CampaignScreen}
        options={{
          title: 'Campaigns',
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Image
                style={{width: 70, height: 24, marginLeft: 10}}
                source={myLogo}
              />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <NavigationDrawerStructure navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: 'rgba(2,113,205,0.7)',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            alignSelf: 'center',
          },
        }}
      />
    </Stack.Navigator>
  );
};

export const ContactsScreenStack = ({navigation}) => {
  return (
    <Stack.Navigator initialRouteName="Contacts">
      <Stack.Screen
        name="Contacts"
        component={ContactsScreen}
        options={{
          title: 'Contact Us!',
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Image
                style={{width: 70, height: 24, marginLeft: 10}}
                source={myLogo}
              />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <NavigationDrawerStructure navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: 'rgba(2,113,205,0.7)',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            alignSelf: 'center',
          },
        }}
      />
    </Stack.Navigator>
  );
};

const NavigationDrawerStructure = props => {
  //Structure for the navigatin Drawer
  const toggleDrawer = () => {
    //Props to open/close the drawer
    props.navigationProps.toggleDrawer();
  };

  return (
    <View style={{flexDirection: 'row'}}>
      <TouchableOpacity onPress={() => toggleDrawer()}>
        {/*Donute Button Image */}
        <Image
          source={drawerWhite}
          style={{
            width: 25,
            height: 25,
            marginLeft: 5,
          }}
        />
      </TouchableOpacity>
    </View>
  );
};
