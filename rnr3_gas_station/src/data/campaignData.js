//const gs1 = '../../assets/imgs/gas_station_1.jpg';
import gs1 from '../assets/imgs/gas_station_1.jpg';

const data = [
  {
    id: 1,
    title: 'title - 1',
    subtitle: 'subtitle - 1',
    detail: 'detail - 1',
    image: require('../assets/imgs/gas_station_1.jpg'),
  },
  {
    id: 2,
    title: 'title - 2',
    subtitle: 'subtitle - 2',
    detail: 'detail - 2',
    image: require('../assets/imgs/gas_station_2.jpg'),
  },
  {
    id: 3,
    title: 'title - 3',
    subtitle: 'subtitle - 3',
    detail: 'detail - 3',
    image: require('../assets/imgs/gas_s3.png'),
  },
  {
    id: 4,
    title: 'title - 4',
    subtitle: 'subtitle - 4',
    detail: 'detail - 4',
    image: require('../assets/imgs/gas_s4.png'),
  },
  {
    id: 5,
    title: 'title - 5',
    subtitle: 'subtitle - 5',
    detail: 'detail - 5',
    image: require('../assets/imgs/gas_s5.png'),
  },
];
export default data;
