import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
//import IconM from "react-native-vector-icons/FontAwesome5";
import dims from '../utils/deviceDimensions';

export const MyItemSeparatorComponent = () => {
  return (
    <View
      style={{
        margin: 5,
        height: 1,
        borderColor: '#252a33',
        borderStyle: 'solid',
        width: '90%',
        backgroundColor: '#9bbaf6',
      }}>
      <Text>******</Text>
    </View>
  );
};

export const MyRenderItem = ({item, index}, {navigation}, scr) => {
  return (
    <View
      style={{
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Image
        style={{
          resizeMode: 'cover',
          width: '100%',
          height: dims.deviceHeight / 3,
        }}
        source={item.image}
      />
      <Text style={{fontSize: 28}}>{item.title}</Text>
      <Text style={{fontSize: 18}}>{item.subtitle}</Text>
      <TouchableOpacity
        onPress={() => navigation.navigate(scr, item)}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          height: 55,
          backgroundColor: 'rgba(144,252,197,0.9)',
          borderRadius: 25,
          padding: 15,
          margin: 15,
        }}>
        <Text style={{color: 'rgba(95,135,215,0.9)'}}>Campaign Detail</Text>
      </TouchableOpacity>
    </View>
  );
};
