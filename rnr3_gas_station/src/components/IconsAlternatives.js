import React, {useState} from 'react';

import {Text, View, StyleSheet, Pressable} from 'react-native';
import Icon from 'react-native-ico-material-design';

const IconsAlternatives = () => {
  const [screenText, setScreenText] = useState('Press a button');
  const iconHeight = 26;
  const iconWidth = 26;
  return (
    <View>
      <View>
        <Text style={{fontSize: 30, color: 'white'}}> {screenText}</Text>
      </View>
      <View>
        <Pressable
          onPress={() => setScreenText('Favourites')}
          style={styles.IconBehave}
          android_ripple={{borderless: true, radius: 50}}>
          <Icon
            name="favorite-heart—button"
            height={iconHeight}
            width={iconWidth}
            color="#448aff"
          />
        </Pressable>
        <Pressable
          onPress={() => setScreenText('Chat')}
          style={styles.IconBehave}
          android_ripple={{borderless: true, radius: 50}}>
          <Icon
            name="chat—bubble"
            height={iconHeight}
            width={iconWidth}
            color="#1e88e5"
          />
        </Pressable>
        <Pressable
          onPress={() => setScreenText('User')}
          style={styles.IconBehave}
          android_ripple={{borderless: true, radius: 50}}>
          <Icon
            name="user—shape"
            height={iconHeight}
            width={iconWidth}
            color="#1565c0"
          />
        </Pressable>
        <Pressable
          onPress={() => setScreenText('Settings')}
          style={styles.IconBehave}
          android_ripple={{borderless: true, radius: 50}}>
          <Icon
            name="settings—cogwheel-button}"
            eight={iconHeight}
            width={iconWidth}
            color="#2962ff"
          />
        </Pressable>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  NavContainer: {
    position: 'absolute',
    alignItems: 'center',
    bottom: 20,
  },
  NavBar: {
    flexDirection: 'row',
    backgroundColor: '#eee',
    width: '90%',
    justifyContent: 'space-evenly',
    borderRadius: 40,
  },
  IconBehave: {
    padding: 14,
  },
});

export default IconsAlternatives;

