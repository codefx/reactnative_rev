import React, {useRef, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  //StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  //useColorScheme,
  View,
} from 'react-native';
import IconM from 'react-native-vector-icons/FontAwesome5';
import Loading from './components/Loading';

const App = () => {
  const TextInputRef = useRef();
  let initialTodos = [
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
  ];
  const [todoTitle, setTodoTitle] = useState('');
  const [todoList, setTodoList] = useState(initialTodos);
  const [isLoading, setIsLoading] = useState(false);
  const clearText = () => {
    TextInputRef.current.clear();
  };
  const addTodo = value => {
    setIsLoading(true);
    if (value.length < 3) {
      alert('Cannot be empty or at least 3 characters');
    } else {
      let todoItem = {title: value, isDone: false};
      setTodoList(prev => [...prev, todoItem]);
      clearText();
    }
    setIsLoading(false);
  };

  function updateTodo(td, ix) {
    setTodoList(prev => {
      prev[ix].isDone = !prev[ix].isDone;
      return [...prev];
    });
  }

  function deleteTodo(td, ix) {
    setTodoList(prev => {
      prev.pop(prev[ix]);
      return [...prev];
    });
  }

  return isLoading ? (
    <Loading />
  ) : (
    <SafeAreaView
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
          width: '100%',
        }}>
        <TextInput
          placeholder=" Enter your To Do's here"
          ref={TextInputRef}
          onChangeText={value => setTodoTitle(value)}
          value={todoTitle}
          style={{
            borderColor: '#00f',
            borderWidth: 1,
            width: '100%',
            marginTop: 20,
          }}
        />
        <View
          style={{
            flex: 3,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            padding: 10,
          }}>
          <Text style={{color: 'rgba(95,135,215,0.9)', margin: 10}}>
            Yapılacaklar:
          </Text>
          <ScrollView style={{flex: 1, width: '100%'}}>
            {todoList.map((td, ix) => (
              <TouchableOpacity
                onPress={() => updateTodo(td, ix)}
                onLongPress={() => deleteTodo(td, ix)}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderColor: td.isDone ? '#0f0' : '#f00',
                  borderWidth: 1,
                  padding: 10,
                  width: '90%',
                  height: 60,
                  borderRadius: 20,
                  margin: 10,
                }}
                key={ix.toString()}>
                <Text style={{color: 'rgba(95,135,215,0.9)'}}>{td.title}</Text>
                <IconM
                  name={td.isDone ? 'check-circle' : 'circle-notch'}
                  size={30}
                  color={td.isDone ? '#0f0' : '#f00'}
                />
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => addTodo(todoTitle)}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#0ff',
          //padding: 5,
          width: 60,
          height: 60,
          borderRadius: 30,
          //marginTop: 10,
          position: 'absolute',
          right: 30,
          bottom: 30,
        }}>
        <Text style={{color: '#fff', fontSize: 38}}>+</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
export default App;
