import {StyleSheet} from 'react-native';
import {deviceWidth} from '../utils/deviceDimensions';
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#312e38',
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: deviceWidth,
    height: deviceWidth * (3 / 8),
    //backgroundColor: '#312e38',
  },
  tiptitle: {
    fontSize: 20,
    color: '#eeeeee',
    textAlign: 'center',
    margin: 10,
  },
  title: {
    fontWeight: '600',
    fontSize: 20,
    color: '#eeeeee',
    textAlign: 'center',
  },
  artist: {
    fontWeight: '200',
    fontSize: 17,
    color: '#eeeeee',
    textAlign: 'center',
  },

  artworkWrapper: {
    width: deviceWidth * 0.7,
    height: deviceWidth * 0.75,
    marginBottom: 10,
    shadowColor: '#fff',
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.9,
    elevation: 5,
  },
  artworkImg: {
    width: '100%',
    height: '100%',
    borderRadius: 15,
  },
  progresContainer: {
    width: deviceWidth * 0.85,
    height: deviceWidth * 0.3,
    marginTop: 10,
    flexDirection: 'row',
  },
  progressLabelContainer: {
    width: deviceWidth * 0.85,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  progressLabelText: {
    color: '#fff',
  },
  musicControls: {
    width: deviceWidth * 0.6,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15,
  },
});
