import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Pressable} from 'react-native';
import {deviceWidth} from '../../utils/deviceDimensions';
import Rmode from '../../constants/repeatModeconstants';

const BottomContainer = ({
  Icon1,
  Icon2,
  Icon3,
  Icon4,
  TrackPlayer,
  RepeatMode,
  op1,
  op2,
  op3,
  op4,
}) => {
  const [repeatModeName, setRepeatModeName] = useState(Rmode.OFF);
  const [iconname, setIconname] = useState('repeat-off');
  // eslint-disable-next-line prettier/prettier

  const changeIconName = rpm => {
    switch (rpm) {
      case Rmode.OFF:
        setIconname(Rmode.REPEAT_OFF);
        setRepeatModeName(Rmode.TRACK);
        TrackPlayer.setRepeatMode(RepeatMode.Off);
        break;
      case Rmode.TRACK:
        setIconname(Rmode.REPEAT_ONCE);
        setRepeatModeName(Rmode.QUEUE);
        TrackPlayer.setRepeatMode(RepeatMode.Track);
        break;
      case Rmode.QUEUE:
        setIconname(Rmode.REPEAT);
        setRepeatModeName(Rmode.OFF);
        TrackPlayer.setRepeatMode(RepeatMode.Queue);
        break;
    }
  };

  return (
    <View style={styles.bottomContainer}>
      <View style={styles.bottomControls}>
        <Pressable onPress={op1}>
          <Icon1 name="heart-outline" size={30} color="#777777" />
        </Pressable>
        <Pressable onPress={() => changeIconName(repeatModeName)}>
          <Icon2
            name={iconname}
            size={30}
            color={iconname !== Rmode.REPEAT_OFF ? '#87dd91' : '#777777'}
          />
        </Pressable>
        <Pressable onPress={op3}>
          <Icon3 name="share-outline" size={30} color="#777777" />
        </Pressable>
        <Pressable onPress={op4}>
          <Icon4 name="ellipsis-horizontal" size={30} color="#777777" />
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomContainer: {
    borderTopColor: '#393E46',
    borderTopWidth: 1,
    width: deviceWidth,
    alignItems: 'center',
    paddingVertical: 15,
  },
  bottomControls: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
  },
});
export default BottomContainer;
