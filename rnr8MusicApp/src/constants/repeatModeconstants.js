const REPEAT_OFF = 'repeat-off';
const REPEAT_ONCE = 'repeat-once';
const REPEAT = 'repeat';
const TRACK = 'Track';
const QUEUE = 'Queue';
const OFF = 'Off';

export default {REPEAT_OFF, REPEAT_ONCE, REPEAT, TRACK, QUEUE, OFF};
