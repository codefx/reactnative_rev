import Slider from '@react-native-community/slider';
import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Pressable,
  Image,
  FlatList,
  Animated,
} from 'react-native';
import BottomContainer from '../../components/bottom-container';
import TrackPlayer, {
  Capability,
  Event,
  RepeatMode,
  State as TState,
  usePlaybackState,
  useProgress,
  useTrackPlayerEvents,
} from 'react-native-track-player';

import {musicData} from '../../data/data';
import {deviceWidth} from '../../utils/deviceDimensions';
import {IonIcon} from '../../utils/vectorIconsPortFull';
import {MCIcon} from '../../utils/vectorIconsPortFull';
import {styles} from '../../theme/mainStyles';

const setupPlayer = async () => {
  await TrackPlayer.setupPlayer();
  TrackPlayer.updateOptions({
    capabilities: [
      Capability.Play,
      Capability.Pause,
      Capability.Stop,
      Capability.JumpBackward,
      Capability.JumpForward,
    ],
  });
  await TrackPlayer.add(musicData);
};
const togglePlayback = async playbackState => {
  const currentTrack = await TrackPlayer.getCurrentTrack();
  if (currentTrack !== null) {
    if (playbackState == TState.Ready || playbackState == TState.Paused) {
      console.log('burası tgpb --' + playbackState);
      await TrackPlayer.play();
    } else {
      await TrackPlayer.pause();
    }
  }
};

export default function MusicScreen() {
  const playerState = usePlaybackState();
  const progress = useProgress();

  const scrollX = useRef(new Animated.Value(0)).current;
  const songSlider = useRef(null);
  const [songIdx, setSongIdx] = useState(0);
  const [trackArtWork, setTrackArtWork] = useState();
  const [trackArtist, setTrackArtist] = useState();
  const [trackTitle, setTrackTitle] = useState();

  useTrackPlayerEvents([Event.PlaybackTrackChanged], async event => {
    if (event.type === Event.PlaybackTrackChanged && event.nextTrack != null) {
      const track = await TrackPlayer.getTrack(event.nextTrack);
      const {title, artwork, artist} = track;
      setTrackTitle(title);
      setTrackArtist(artist);
      setTrackArtWork(artwork);
    }
  });

  const skipTo = async trackId => {
    await TrackPlayer.skip(trackId);
  };

  useEffect(() => {
    setupPlayer();
    scrollX.addListener(({value}) => {
      //console.log('Scrolx', scrollX);
      const index = Math.round(value / deviceWidth);
      skipTo(index);
      setSongIdx(index);
    });
    return () => {
      scrollX.removeAllListeners();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const skipNext = () => {
    songSlider.current.scrollToOffset({
      offset: (songIdx + 1) * deviceWidth,
    });
  };
  const skipPrev = () => {
    songSlider.current.scrollToOffset({
      offset: (songIdx - 1) * deviceWidth,
    });
  };
  const RenderItem = ({index, item}) => {
    return (
      <Animated.View
        style={{
          width: deviceWidth,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={styles.artworkWrapper}>
          <Image source={trackArtWork} style={styles.artworkImg} />
        </View>
      </Animated.View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.mainContainer}>
        <Text style={styles.tiptitle}>MusicScreen</Text>
        <View style={{width: deviceWidth}}>
          <Animated.FlatList
            ref={songSlider}
            data={musicData}
            renderItem={RenderItem}
            keyExtractor={item => item.id}
            horizontal
            pagingEnabled
            showsHorizontalScrollIndicator={false}
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {x: scrollX},
                  },
                },
              ],
              {useNativeDriver: true},
            )}
          />
        </View>
        <View>
          <Text style={styles.title}>{trackTitle}</Text>
          <Text style={styles.artist}>{trackArtist}</Text>
        </View>
        <View>
          <Slider
            style={styles.progresContainer}
            value={progress.position}
            minimumValue={0}
            maximumValue={progress.duration}
            thumbTintColor="#ffd369"
            minimumTrackTintColor="#ffd369"
            maximumTrackTintColor="#ffffff"
            onSlidingComplete={async value => {
              await TrackPlayer.seekTo(value);
            }}
          />
          <View style={styles.progressLabelContainer}>
            <Text style={styles.progressLabelText}>
              {new Date(progress.position * 1000)
                .toISOString()
                .substring(14, 19)}
            </Text>
            <Text style={styles.progressLabelText}>
              {new Date((progress.duration - progress.position) * 1000)
                .toISOString()
                .substring(14, 19)}
            </Text>
          </View>
        </View>
        <View style={styles.musicControls}>
          <Pressable onPress={skipPrev}>
            <IonIcon name="play-skip-back-outline" size={35} color="#87dd91" />
          </Pressable>
          <Pressable onPress={() => togglePlayback(playerState)}>
            <IonIcon
              name={
                playerState === TState.Playing ? 'pause-circle' : 'play-circle'
              }
              size={65}
              color="#87dd91"
            />
          </Pressable>
          <Pressable onPress={skipNext}>
            <IonIcon
              name="play-skip-forward-outline"
              size={35}
              color="#87dd91"
            />
          </Pressable>
        </View>
      </View>
      <BottomContainer
        Icon1={IonIcon}
        Icon2={MCIcon}
        Icon3={IonIcon}
        Icon4={IonIcon}
        TrackPlayer={TrackPlayer}
        RepeatMode={RepeatMode}
      />
    </SafeAreaView>
  );
}
