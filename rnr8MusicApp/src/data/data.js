export const musicData = [
  {
    title: 'duy_unu_gz_r',
    artist: 'unknown',
    artwork: require('../assets/images/music-note.png'),
    url: require('../assets/audios/du_enstr2.mp3'),
    id: 1,
  },
  {
    title: 'f_march',
    artist: 'unknown',
    artwork: require('../assets/images/spotlights-mobile.jpg'),
    url: require('../assets/audios/fm_fon.mp3'),
    id: 2,
  },
  {
    title: 'ze_go_fon',
    artist: 'unknown',
    artwork: require('../assets/images/gas_s4.png'),
    url: require('../assets/audios/zg_fon.mp3'),
    id: 3,
  },
  {
    title: 'mu_ku_sa_se',
    artist: 'unknown',
    artwork: require('../assets/images/sound-kare.jpg'),
    url: require('../assets/audios/mk_ss.mp3'),
    id: 4,
  },
  {
    title: 'ogr_fon',
    artist: 'unknown',
    artwork: require('../assets/images/music-note.png'),
    url: require('../assets/audios/ogr_fon.mp3'),
    id: 5,
  },
];
