import React from 'react';
import {StatusBar, View} from 'react-native';

import MusicScreen from './screens/MusicScreen';

const App = () => {
  return (
    <>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="#312e38"
      />
      <MusicScreen />
    </>
  );
};
export default App;
