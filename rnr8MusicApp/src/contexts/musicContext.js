import {createContext, useRef, useState} from 'react';

import {View, Text, Animated} from 'react-native';
import React from 'react';
import TrackPlayer, {
  Capability,
  State,
  usePlaybackState,
  useProgress,
  useTrackPlayerEvents,
} from 'react-native-track-player';
import {deviceWidth} from '../utils/deviceDimensions';

const MusicContext = createContext({});

const MusicContextProvider = () => {
  const playerState = usePlaybackState();
  const progress = useProgress();

  const scrollX = useRef(new Animated.Value(0)).current;
  const songSlider = useRef(null);
  const [songIdx, setSongIdx] = useState(0);
  const [trackArtWork, setTrackArtWork] = useState();
  const [trackArtist, setTrackArtist] = useState();
  const [trackTitle, setTrackTitle] = useState();

  const setupPlayer = async musicData => {
    await TrackPlayer.setupPlayer();
    TrackPlayer.updateOptions({
      capabilities: [
        Capability.Play,
        Capability.Pause,
        Capability.Stop,
        Capability.JumpBackward,
        Capability.JumpForward,
      ],
    });
    await TrackPlayer.add(musicData);
  };
  const togglePlayback = async playbackState => {
    const currentTrack = await TrackPlayer.getCurrentTrack();
    if (currentTrack !== null) {
      if (playbackState == State.Ready || playbackState == State.Paused) {
        console.log('burası tgpb --' + playbackState);
        await TrackPlayer.play();
      } else {
        await TrackPlayer.pause();
      }
    }
  };
  useTrackPlayerEvents([Event.PlaybackTrackChanged], async event => {
    if (event.type === Event.PlaybackTrackChanged && event.nextTrack != null) {
      const track = await TrackPlayer.getTrack(event.nextTrack);
      const {title, artwork, artist} = track;
      setTrackTitle(title);
      setTrackArtist(artist);
      setTrackArtWork(artwork);
    }
  });

  const skipTo = async trackId => {
    await TrackPlayer.skip(trackId);
  };
  const skipNext = () => {
    songSlider.current.scrollToOffset({
      offset: (songIdx + 1) * deviceWidth,
    });
  };
  const skipPrev = () => {
    songSlider.current.scrollToOffset({
      offset: (songIdx - 1) * deviceWidth,
    });
  };

  const values = {};
  return (
    <MusicContext.Provider value={values}>
      <Text>MusicContextProvider</Text>
    </MusicContext.Provider>
  );
};

export default MusicContextProvider;
