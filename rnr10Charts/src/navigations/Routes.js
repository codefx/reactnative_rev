import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import ChartKitScreen from '../screens/ChartKitScreen';
import ReNatPieScreen from '../screens/ReNatPieScreen';
import AnimatedSplashScreen from '../screens/AnimatedSplashScreen';

const Stack = createStackNavigator();
const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Splash"
          component={AnimatedSplashScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            headerLeft: () => {},
            title: 'Setting Page',
            // headerShown: false,
            headerStyle: {
              backgroundColor: '#7bcc62',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="ReNatPieScreen"
          component={ReNatPieScreen}
          options={{
            title: 'Setting Page',
            headerStyle: {
              backgroundColor: '#7bcc62',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="ChartKitScreen"
          component={ChartKitScreen}
          options={{
            title: 'Details Page',
            headerStyle: {
              backgroundColor: '#67e8cc',
            },

            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
