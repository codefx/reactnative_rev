import {View, Text, Dimensions} from 'react-native';
import React from 'react';
import {styles} from './styles';
import {PieChart} from 'react-native-chart-kit';

const MyPieChart = () => {
  return (
    <>
      <Text style={styles.header}>Pie Chart</Text>
      <PieChart
        data={[
          {
            name: 'Seoul',
            population: 21500000,
            color: 'rgba(237, 230, 99, 1)',
            legendFontColor: '#7F7F7F',
            legendFontSize: 13,
          },
          {
            name: 'Toronto',
            population: 2800000,
            color: '#F00',
            legendFontColor: '#7F7F7F',
            legendFontSize: 13,
          },
          {
            name: 'New York',
            population: 8538000,
            color: '#76ee80',
            legendFontColor: '#7F7F7F',
            legendFontSize: 13,
          },
          {
            name: 'Moscow',
            population: 11920000,
            color: 'rgb(0, 0, 255)',
            legendFontColor: '#7F7F7F',
            legendFontSize: 13,
          },
        ]}
        width={Dimensions.get('window').width}
        height={220}
        chartConfig={{
          backgroundColor: '#1cc910',
          backgroundGradientFrom: '#d0daf6',
          backgroundGradientTo: '#f9dfdf',
          decimalPlaces: 2,
          color: (opacity = 1) => `rgba(0, 200, 0, ${opacity})`,
          style: {
            borderRadius: 16,
          },
        }}
        style={{
          marginVertical: 8,
          borderRadius: 16,
        }}
        accessor="population"
        backgroundColor="transparent"
        paddingLeft="10"
        paddingRight="10"
        center={[15, 0]}
        absolute //For the absolute number else percentage
      />
    </>
  );
};

export default MyPieChart;
