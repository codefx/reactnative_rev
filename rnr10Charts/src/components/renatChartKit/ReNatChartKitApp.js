import {View, Text, SafeAreaView, ScrollView} from 'react-native';
import React from 'react';
import {styles} from './styles';
import BezierLineChart from './MyBezierLineChart';
import MyLineChart from './MyLineChart';
import MyProgressChart from './MyProgressChart';
import MyBarChart from './MyBarChart';
import MyStackedBarChart from './MyStackedBarChart';
import MyPieChart from './MyPieChart';
import MyContributionGraph from './MyContributionGraph';

const ReNatChartKitApp = () => {
  return (
    <ScrollView styles={{flex: 1}}>
      <View style={styles.container}>
        <View>
          {/*Example of Bezier LineChart*/}
          <BezierLineChart />
          {/*Example of LineChart*/}
          <MyLineChart />
          {/*Example of Progress Chart*/}
          <MyProgressChart />
          {/*Example of Bar Chart*/}
          <MyBarChart />
          {/*Example of StackedBar Chart*/}
          <MyStackedBarChart />
          {/*Example of Pie Chart*/}
          <MyPieChart />
          {/*Example of Contribution Chart*/}
          <MyContributionGraph />
        </View>
        <Text style={{marginBottom: 40, padding: 10, color: '#000'}}>
          The end
        </Text>
      </View>
    </ScrollView>
  );
};

export default ReNatChartKitApp;
