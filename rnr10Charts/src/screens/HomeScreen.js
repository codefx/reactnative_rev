import * as React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  StatusBar,
  Alert,
  BackHandler,
} from 'react-native';

const HomeScreen = ({navigation}) => {
  React.useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want to exit?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  return (
    <SafeAreaView style={{flex: 1, width: '100%', alignItems: 'center'}}>
      <StatusBar hidden={false} />
      <View style={styles.container}>
        <Text style={styles.text}>You are on Home Screen</Text>
      </View>
      <ScrollView style={{flex: 2, width: '100%'}}>
        <TouchableOpacity
          style={{...styles.topacity, backgroundColor: '#bdf3bd'}}
          onPress={() => navigation.navigate('ReNatPieScreen')}>
          <Text style={{color: '#406cf5', fontSize: 16}}>
            {' '}
            Open React native pie library's chart
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{...styles.topacity, backgroundColor: '#87eaed'}}
          onPress={() => navigation.navigate('ChartKitScreen')}>
          <Text style={{color: '#9fa9af', fontSize: 16}}>
            Open react-native-chart-kit library's charts
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  topacity: {
    flex: 1,
    width: '90%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#bdf3bd',
    borderRadius: 28,
    margin: 10,
  },
  text: {
    fontSize: 21,
    textAlign: 'center',
    marginBottom: 16,
  },
});

export default HomeScreen;
