import {View, Text} from 'react-native';
import React from 'react';
import ReNatChartKitApp from '../components/renatChartKit/ReNatChartKitApp';

const ChartKitScreen = () => {
  return <ReNatChartKitApp />;
};

export default ChartKitScreen;
