import {
  View,
  Text,
  Animated,
  StyleSheet,
  Image,
  ImageBackground,
  StatusBar,
} from 'react-native';
import React, {useEffect, useRef} from 'react';
import {deviceHeight, deviceWidth} from '../utils/devicedimensions';

const bg = require('../assets/images/starrybackground.jpg');
const logo = require('../assets/images/mylogo.png');

const AnimatedSplashScreen = ({navigation}) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 4000,
      useNativeDriver: false,
    }).start(({finished}) => navigation.push('Home'));
  }, [fadeAnim, navigation]);

  return (
    <>
      <StatusBar hidden={true} />

      <Animated.View style={styles.container}>
        <ImageBackground
          source={bg}
          resizeMode="cover"
          style={{
            width: deviceWidth,
            height: deviceHeight,
            flex: 1,
            //   opacity: fadeAnim,
          }}>
          <Animated.View style={{...styles.logoContainer, opacity: fadeAnim}}>
            <Animated.Image source={logo} style={styles.logo} />
            <Animated.Text style={styles.textStyle}>
              Speed and Fast
            </Animated.Text>
          </Animated.View>
        </ImageBackground>
      </Animated.View>
    </>
  );
};

export default AnimatedSplashScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 48,
    backgroundColor: 'rgba(11, 56, 82, 0.3)',
  },
  logo: {
    width: 100,
    height: 100,
  },
  textStyle: {
    fontSize: 18,
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    textAlign: 'center',
  },
});
