import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import HomeScreen from './src/screens/HomeScreen';
import Routes from './src/navigations/Routes';

const App = () => {
  return <Routes />;
};

export default App;
