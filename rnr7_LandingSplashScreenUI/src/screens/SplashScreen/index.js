import React from 'react';
import { View, Text, Image, StatusBar } from 'react-native';
import CustomLinearButton from '../../components/CustomLinearButton';
import { styles } from './splashStyles';
import { MIcon } from '../../utils/vectorIconsPortFull';
import * as Animatable from 'react-native-animatable';

const SplashScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
    <StatusBar backgroundColor="#009387" barStyle="light-content" />
      <View style={styles.header}>
        <Animatable.Image
          animation="flipInX"
          source={require('../../assets/onboard2.png')}
          style={{
            width: '60%', height: '40%', borderRadius: 80,
            borderColor: '#fff', borderWidth: 2,
          }}
          resizeMode="cover"
        />
      </View>

      <Animatable.View animation="fadeInUpBig" style={styles.footer}>
        <Text style={styles.title}>Stay connected with Everyone!</Text>
        <Text style={styles.text}>Sign in with account</Text>
        <View style={styles.button}>
          <CustomLinearButton
            onPress={() => navigation.navigate('SignInScreen')}
            linearStyle={styles.signin}
            colors={['#60c9c9', '#319f9f']}
            textStyle={styles.textSign}
            text="Get Started"
            icon={<MIcon name="navigate-next" color="#fff" size={20} />}
          />
        </View>
      </Animatable.View>
    </View>
  );
};

export default SplashScreen;
