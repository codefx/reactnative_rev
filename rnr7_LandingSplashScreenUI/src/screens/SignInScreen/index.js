import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  Pressable,
  StatusBar,
} from 'react-native';
import {styles} from './signInStyles';
import {FaIcon, FeatIcon, MIcon} from '../../utils/vectorIconsPortFull';
import * as Animatable from 'react-native-animatable';
import {TextInput} from 'react-native-gesture-handler';
import CustomLinearButton from '../../components/CustomLinearButton';

const SignInScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [inputTextChanges, setInputTextChanges] = useState(false);
  const [secTextEntry, setSecTextEntry] = useState(true);

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.text_header}>Welcome, now let's begin !</Text>
      </View>
      <Animatable.View animation="fadeInUpBig" style={styles.footer}>
        <Text style={styles.text_footer}>Email</Text>
        <View style={styles.action}>
          <FaIcon name="user-o" color="#05375a" size={20} />
          <TextInput
            value={email}
            placeholder="email"
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={value => {
              setEmail(value);
              if (value.length !== 0) {
                setInputTextChanges(true);
              } else {
                setInputTextChanges(false);
              }
            }}
          />
          {inputTextChanges ? (
            <Animatable.View animation="bounceIn">
              <FeatIcon name="check-circle" color="green" size={20} />
            </Animatable.View>
          ) : null}
        </View>
        <Text style={styles.text_footer}>Password</Text>
        <View style={styles.action}>
          <FaIcon name="lock" color="#05375a" size={20} />
          <TextInput
            value={password}
            placeholder="password"
            style={styles.textInput}
            autoCapitalize="none"
            secureTextEntry={secTextEntry}
            onChangeText={value => setPassword(value)}
          />
          <Pressable
            onPress={() => {
              setSecTextEntry(!secTextEntry);
            }}>
            <FeatIcon
              name={secTextEntry ? 'eye-off' : 'eye'}
              color="gray"
              size={20}
            />
          </Pressable>
        </View>
        <View style={styles.button}>
          <CustomLinearButton
            onPress={() => navigation.navigate('SignInScreen')}
            linearStyle={styles.signIn}
            colors={['#60c9c9', '#319f9f']}
            textStyle={{...styles.textSign, color: '#ffffff'}}
            text="Sign In"
          />
          <CustomLinearButton
            onPress={() => navigation.navigate('SignUpScreen')}
            linearStyle={[
              styles.signIn,
              {borderColor: '#009387', borderWidth: 1, marginTop: 15},
            ]}
            colors={['#f9fbfb', '#cbe7e7']}
            textStyle={[styles.textSign, {color: '#009387'}]}
            text="Sign Up"
          />
        </View>
      </Animatable.View>
    </View>
  );
};
export default SignInScreen;
