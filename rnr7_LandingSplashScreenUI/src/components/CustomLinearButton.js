import React from 'react';
import { View, Text, StyleSheet, Pressable } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const CustomLinearButton = (
  { colors,
    linearStyle,
    textStyle,
    text,
    onPress,icon }
) => {
  return (
    <Pressable onPress={onPress}>
      <LinearGradient colors={colors} style={linearStyle}>
        <Text style={textStyle}>{text}</Text>
        {icon}
      </LinearGradient>
    </Pressable>
  );
};

export default CustomLinearButton;
