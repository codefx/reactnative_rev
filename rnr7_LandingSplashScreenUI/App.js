import React from 'react';
import 'react-native-gesture-handler';

import {NavigationContainer} from '@react-navigation/native';
// import MainTabScreen from './src/screens/MainTabScreen/index';
// import SupportScreen from './src/screens/SupportScreen/index';
// import SettingsScreen from './src/screens/SettingsScreen/index';
// import BookmarkScreen from './src/screens/BookmarkScreen/index';
// import {createDrawerNavigator} from '@react-navigation/drawer';
// import DrawerContent from './src/screens/DrawerContent/index';
import RootStackScreen from './src/screens/RootStackScreen';

// const Drawer = createDrawerNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <RootStackScreen />
      {/* <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
        <Drawer.Screen name="HomeDrawer" component={MainTabScreen} />
        <Drawer.Screen name="SupportScreen" component={SupportScreen} />
        <Drawer.Screen name="SettingsScreen" component={SettingsScreen} />
        <Drawer.Screen name="BookmarkScreen" component={BookmarkScreen} />
      </Drawer.Navigator> */}
    </NavigationContainer>
  );
};
export default App;
