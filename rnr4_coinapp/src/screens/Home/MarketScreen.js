import React, {useEffect, useState} from 'react';
import {View, Text, SafeAreaView, FlatList, Pressable} from 'react-native';
import {CoinMarketApi, MarketApi} from '../../services/api/baseApi';
import Loading from '../../components/Loading';
import {
  MyItemSeparatorComponent,
  MyRenderItem,
} from '../../components/MyFlatListSubComponents';
import {MCIcon} from '../../utils/vectorIconsPort';

const MarketScreen = ({route, navigation}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const getCurrentData = async () => {
    try {
      setIsLoading(true);
      let resp = await MarketApi.get();
      let respData = await resp.json();
      setData(respData);
      setIsLoading(false);
    } catch (e) {
      alert(e.message);
    }
  };
  const getCoinData = async () => {
    try {
      setIsLoading(true);
      let resp = await CoinMarketApi.get().then(resp => resp.data);
      setData(resp);
      setIsLoading(false);
    } catch (e) {
      alert(e.message);
    }
  };

  useEffect(() => {
    //getCurrentData();
    getCoinData();
  }, []);
  return (
    <SafeAreaView style={{flex: 1}}>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              padding: 10,
              width: '100%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems:'center',
                justifyContent: 'space-between',
                width:'90%'
              }}>
              <Text style={{color: 'rgba(86,222,150,0.9)', fontSize: 26}}>
                Market's last status
              </Text>
              <Pressable onPress={getCoinData}>
                <MCIcon name="reload" size={20} color="#03f" />
              </Pressable>
            </View>
            <FlatList
              style={{flex: 1, width: '100%'}}
              data={data}
              renderItem={MyRenderItem}
              itemSeparatorComponent={MyItemSeparatorComponent}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </>
      )}
    </SafeAreaView>
  );
};
export default MarketScreen;
