import * as React from 'react';

import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {deviceHeight, deviceWidth} from '../../utils/deviceDimensions';

const ProfileScreen = ({route, navigation}) => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View
            style={{
              flex: 1,
              padding: 16,
              margin: 20,
              alignItems: 'center',
              justifyContent: 'center',
              borderWidth: 4,
              width: deviceWidth / 2,
              height: deviceHeight / 4,
              borderRadius: deviceWidth,
            }}>
            <Text
              style={{
                fontSize: 20,
                textAlign: 'center',
                marginBottom: 16,
              }}>
              You are on Profile Screen
            </Text>
          </View>

          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('HomeScreen')}>
            <Text>Go to Home Tab</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('Market')}>
            <Text>Open Market Screen</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('Settings')}>
            <Text>Open settings Screen</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
});

export default ProfileScreen;
