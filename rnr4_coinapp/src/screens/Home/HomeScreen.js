import * as React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import MyButton from '../../components/MyButton';
import {useAuthContext} from '../../contexts';

const HomeScreen = ({navigation}) => {
  const {logout, setUser} = useAuthContext();
  return (
    <SafeAreaView style={{
      flex: 1,
      width: '100%',
      alignItems: 'center',
      justifyContent:'center',
      padding:20,
      margin:20,
    }}>
      <View
        style={{
          //flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <Text
          style={{
            fontSize: 21,
            textAlign: 'center',
            marginBottom: 16,
            marginRight:16,
          }}>
          You are on Home Screen
        </Text>
      </View>
      <ScrollView style={{flex: 3, width: '100%', padding: 16}}>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#bdf3bd',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Settings', {screen: 'Settings'})}>
          <Text style={{color: '#406cf5', fontSize: 20}}> Go to Settings</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#eeae64',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Market')}>
          <Text style={{color: '#51a1c0', fontSize: 20}}>
            Open Market Screen
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#f17070',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Profile')}>
          <Text style={{color: '#f2f6cd', fontSize: 20}}>
            {' '}
            Go to Profile Page
          </Text>
        </TouchableOpacity>
        <MyButton
          title="Sign Out"
          onPressFunc={() =>
            logout().then(resp => {
              resp ? setUser(null) : '';
            })
          }
          bgColor="#d00"
          style={{
            margin: 10,
            height: 50,
            width:'85%',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
