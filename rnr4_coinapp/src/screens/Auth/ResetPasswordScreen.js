import React, {useRef, useState} from 'react';
import {Formik} from 'formik';
import * as yup from 'yup';
import {
  SafeAreaView,
  Text,
  TextInput,
  View,
} from 'react-native';
import {useAuthContext} from '../../contexts';
import {myResetPasswordValidateSchema} from '../../validations/yupValidations';
import ResetButton from '../../components/MyButton';
import {MCIcon} from '../../utils/vectorIconsPort';

const ResetPasswordScreen = ({navigation}) => {
  const {resetPassword, setUser, user} = useAuthContext();

  return (
    <SafeAreaView
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
          width: '90%',
        }}>
        <MCIcon color="#4A97F5" name="lock-reset" size={80} />
        <Text
          style={{
            color: '#52b752',
            fontSize: 24,
            fontWeight: '400',
            marginBottom: 20,
            marginTop: 20,
          }}>
          Password Resetting Screen
        </Text>
        <Formik
          validationSchema={myResetPasswordValidateSchema}
          initialValues={{email: ''}}
          onSubmit={values => resetPassword(values.email)}>
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            isValid,
            dirty,
          }) => (
            <>
              <TextInput
                placeholder="Enter your email which the Mail will sent"
                value={values.email}
                name="email"
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                keyboardType="email-address"
                style={{
                  borderColor: '#4A97F5',
                  borderWidth: 1,
                  width: '90%',
                  height: 60,
                  borderRadius: 10,
                  margin: 10,
                }}

              />
              {errors.email && (
                <Text style={{color: '#f00', fontSize: 14}}>
                  {errors.email}
                </Text>
              )}

              <ResetButton
                onPressFunc={handleSubmit}
                disabled={!isValid || !dirty}
                title="Send Email"
              />
            </>
          )}
        </Formik>
      </View>
    </SafeAreaView>
  );
};

export default ResetPasswordScreen;
