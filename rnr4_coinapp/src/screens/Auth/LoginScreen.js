import React, {useRef, useState} from 'react';
import {Formik} from 'formik';
import * as yup from 'yup';
import {
  Button, Pressable,
  SafeAreaView,
  ScrollView,
  //StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  //useColorScheme,
  View,
} from "react-native";
import {useAuthContext} from '../../contexts';
import {myLoginValidateSchema} from '../../validations/yupValidations';
import LoginButton from '../../components/MyButton';
import {MCIcon} from '../../utils/vectorIconsPort';

const LoginScreen = ({navigation}) => {
  const {login, setUser, user} = useAuthContext();
  const [visiblePass, setVisiblePass] = useState(false);

  return (
    <SafeAreaView
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
          width: '90%',
        }}>
        <MCIcon color="#4A97F5" name="shield-lock" size={80} />
        <Text
          style={{
            color: '#52b752',
            fontSize: 24,
            fontWeight: '400',
            marginBottom: 20,
            marginTop: 20,
          }}>
          Login Screen
        </Text>
        <Formik
          validationSchema={myLoginValidateSchema}
          initialValues={{email: '', password: ''}}
          onSubmit={values => login(values.email, values.password).then(
            resp => {
              resp.status ? setUser('cfx') : setUser(null);
            })
          }>
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            isValid,
            dirty,
          }) => (
            <>
              <TextInput
                placeholder="Enter your email, qq@qw.qq"
                value={values.email}
                name="email"
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                keyboardType="email-address"
                style={{
                  borderColor: '#4A97F5',
                  borderWidth: 1,
                  width: '90%',
                  height: 60,
                  borderRadius: 10,
                  margin: 10,
                }}
              />
              {errors.email && (
                <Text style={{color: '#f00', fontSize: 14}}>
                  {errors.email}
                </Text>
              )}
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderColor: '#4A97F5',
                  borderWidth: 1,
                  width: '90%',
                  height: 60,
                  borderRadius: 10,
                  margin: 10,
                }}>
                <TextInput
                  placeholder="Enter your password"
                  value={values.password}
                  name="password"
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                  secureTextEntry={!visiblePass}
                  style={{
                    width: '90%',
                    height: 60,
                  }}
                />
                <Pressable
                  onPress={() => {
                    setVisiblePass(!visiblePass);
                  }}>
                  <MCIcon
                    color="#999"
                    name={visiblePass ? 'eye' : 'eye-off'}
                    size={22}
                    style={{marginRight: 10}}
                  />
                </Pressable>
              </View>
              {errors.password && (
                <Text style={{color: '#f00', fontSize: 14}}>
                  {errors.password}
                </Text>
              )}
              <LoginButton
                onPressFunc={() => handleSubmit()}
                disabled={!isValid || !dirty}
                title="Log in"
              />
            </>
          )}
        </Formik>
      </View>
    </SafeAreaView>
  );
};
export default LoginScreen;
