import React from 'react';

import {Text, View} from 'react-native';
import {MIcon} from '../utils/vectorIconsPort';

const Warning = () => {
  return (
    <View
      style={{
        flex: 1,
        alignitems: 'center',
        justifyContent: 'center',
      }}>
      <MIcon name="warning" size={72} color="#f00" />
      <Text style={{fontSize: 32}}>Hata!</Text>
    </View>
  );

};

export default Warning;
