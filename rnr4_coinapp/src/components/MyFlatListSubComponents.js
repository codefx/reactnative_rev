import {Text, TouchableOpacity, View, Image} from 'react-native';
import { MCIcon } from "../utils/vectorIconsPort";
//import IconM from "react-native-vector-icons/FontAwesome5";

export const MyItemSeparatorComponent = () => {
  return (
    <View
      style={{margin: 1, height: 1, width: '100%', backgroundColor: '#cde'}}
    />
  );
};

export const MyRenderItem = ({item, index}) => {
  return (
    <TouchableOpacity
      /*onPress={() => (item, index)}
      onLongPress={() => (item, index)}*/
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        //borderColor: item.isDone ? '#0f0' : '#f00',
        borderWidth: 1,
        padding: 10,
        width: '95%',
        height: 60,
        borderRadius: 20,
        margin: 5,
      }}
      key={index.toString()}>
      <Image
        source={{uri: item.image.small}}
        style={{width: 28, height: 28, marginEnd: 10}}
      />
      <View style={{flex: 1, marginTop: 5}}>
        <Text style={{textAlign: 'left', fontSize: 18}}>{item.symbol} </Text>
        <Text style={{textAlign: 'left', fontSize: 12}}>{item.name}</Text>
      </View>
      <View
        style={{
          flex: 1,
          marginTop: 10,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text style={{textAlign: 'left', fontSize: 14, color: '#1a58ff'}}>
          {item.market_data.current_price.try}
        </Text>
        <Text style={{textAlign: 'center', fontSize: 10, marginStart:5}}>TRY</Text>
      </View>
      <View style={{flex: 1, flexDirection: 'row', alignItems:'flex-end',justifyContent:'space-between'}}>
        <MCIcon name={
          item.market_data.price_change_percentage_24h > 0
            ? 'arrow-up-thick'
            : item.market_data.price_change_percentage_24h < 0
              ? 'arrow-down-thick'
              : 'arrow-left-right-bold'
        } size={20} color={item.market_data.price_change_percentage_24h > 0
          ? '#0f0'
          : item.market_data.price_change_percentage_24h < 0
            ? '#f00'
            : '#000'}/>
        <Text
          style={{
            textAlign: 'right',
            fontSize: 18,
            color:
              item.market_data.price_change_percentage_24h > 0
                ? '#0f0'
                : item.market_data.price_change_percentage_24h < 0
                ? '#f00'
                : '#000',
          }}>
          {item.market_data.price_change_percentage_24h}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
