import {useState} from 'react';
import {Pressable, Text} from 'react-native';

const MyButton = ({
  onPressFunc,
  disabled,
  title = 'Mybutton',
  borderRadius = 45,
  bgColor = '#2196F3',
  width='70%',
}) => {
  return (
    <Pressable
      onPress={onPressFunc}
      style={{
        backgroundColor: disabled ? '#999' : bgColor,
        justifyContent: 'center',
        alignItems:'center',
        padding: 10,
        width: width,
        borderRadius: borderRadius,
        elevation: 8,
        margin: 20,
      }}
      disabled={disabled}>
      <Text
        style={{
          fontSize: 18,
          color: '#fff',
          fontWeight: 'bold',
          alignSelf: 'center',
          textTransform: 'uppercase',
        }}>
        {title}
      </Text>
    </Pressable>
  );
};
export default MyButton;
