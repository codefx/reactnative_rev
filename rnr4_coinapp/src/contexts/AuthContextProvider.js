import React, {useState} from 'react';

import {Text, View} from 'react-native';
import auth from '../services/api/AuthService';
import {AuthContext} from './index';

const AuthProvider = ({children}) => {
  const [user, setUser] = useState(null);
  const values = {
    user,
    setUser,
    login: auth().login,
    register: auth().createUserWithEmailAndPassword,
    resetPassword: auth().sendPasswordResetEmail,
    logout: auth().signout,
    checkAuthStateChanges: auth().checkAuthStateChanges,
  };
  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
};

export default AuthProvider;
