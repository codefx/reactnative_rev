import api from './baseApi';
import {useAuthContext} from '../../contexts';
import delay from '../../utils/delayFunction';
const AuthService = () => {
  const {user, setUser} = useAuthContext();
  const login = async (email, password) => {
    let uname = null;
    try {
      console.log(email + '-' + password + ' burası login init');
      if (email === 'qq@qw.qq' && password === '123456') {
        console.log(user + ' burası awaitten sonra');
        return {name: 'cfx', status: true};
      }
      /*if (email !== null && email !== '') {
        const username = api
          .get('users')
          .then(response => {
            // handle success
            let qname = response.data.find(x => {
              return y => y.email === email;
            }).username;
            console.log(qname + ' --burası login then');
            alert(qname + ' login');
            return qname;
          })
          .catch(error => {
            // handle error
            alert(error.message + ' *burası catch');
          });
        setUser(username);
        uname=username;
      }*/
    } catch (e) {
      alert(e.message + ' burası login func catch');
    }
    return {name: null, status: false};
  };
  const loginWithUsernameAndPassword = ({Username, password}) => {
    /*api
      .get('https://jsonplaceholder.typicode.com/posts/1')
      .then(function (response) {
        // handle success
        alert(JSON.stringify(response.data));
      })
      .catch(function (error) {
        // handle error
        alert(error.message);
      })
      .finally(function () {
        // always executed
        alert('Finally called');
      });*/
  };
  const createUserWithEmailAndPassword = ({name, email, password,navigation}) => {
   /* api
      .get('https://jsonplaceholder.typicode.com/posts/1')
      .then(function (response) {
        // handle success
        alert(JSON.stringify(response.data));
      })
      .catch(function (error) {
        // handle error
        alert(error.message);
      })
      .finally(function () {
        // always executed
        alert('Finally called');
      });*/
    // navigation.navigate('LoginScreen');
  };
  const sendPasswordResetEmail = () => {
    /*api
      .get('https://jsonplaceholder.typicode.com/posts/1')
      .then(function (response) {
        // handle success
        alert(JSON.stringify(response.data));
      })
      .catch(function (error) {
        // handle error
        alert(error.message);
      });*/
  };
  const signout = async () => {
    alert(user + ' deleted, and signed out.');
    /*api
      .get('posts/1')
      .then(function (response) {
        // handle success
        alert(JSON.stringify(response.data));
      })
      .catch(function (error) {
        // handle error
        alert(error.message);
      });*/
    return true;
  };
  const checkAuthStateChanges = user => {
    let uname = null;
    /* if (user !== null && user !== '') {
      *api
         .get('users')
         .then(response => {
           // handle success
           let qname = response.data.find(x => {
             return y => y.username === user;
           }).username;
           console.log(qname + ' burası check qname');
           if (qname) {
             uname = qname;
           } else {
             uname = null;
           }

           alert(uname + ' checkAuth then');
         })
         .catch(function (error) {
           // handle error
           alert(error.message + ' burası check catch');
         });
    return uname != null
      ? {name: uname, status: true}
      : {name: null, status: false};*/
    return user ? user : null;
  };
  const getDataUsingSimpleGetCall = () => {
    /*api
      .get('https://jsonplaceholder.typicode.com/posts/1')
      .then(function (response) {
        // handle success
        alert(JSON.stringify(response.data));
      })
      .catch(function (error) {
        // handle error
        alert(error.message);
      })
      .finally(function () {
        // always executed
        alert('Finally called');
      });*/
  };

  const getDataUsingAsyncAwaitGetCall = async () => {
    /* try {
      const response = await api.get(
        'https://jsonplaceholder.typicode.com/posts/1',
      );
      alert(JSON.stringify(response.data));
    } catch (error) {
      // handle error
      alert(error.message);
    }*/
  };

  const postDataUsingSimplePostCall = () => {
    /*api
      .post('https://jsonplaceholder.typicode.com/posts', {
        title: 'foo',
        body: 'bar',
        userId: 1,
      })
      .then(function (response) {
        // handle success
        alert(JSON.stringify(response.data));
      })
      .catch(function (error) {
        // handle error
        alert(error.message);
      });*/
  };

  const multipleRequestsInSingleCall = () => {
    /*api
      .all([
        api
          .get('https://jsonplaceholder.typicode.com/posts/1')
          .then(function (response) {
            // handle success
            alert('Post 1 : ' + JSON.stringify(response.data));
          }),
        api
          .get('https://jsonplaceholder.typicode.com/posts/2')
          .then(function (response) {
            // handle success
            alert('Post 2 : ' + JSON.stringify(response.data));
          }),
      ])
      .then(
        api.spread(function (acct, perms) {
          // Both requests are now complete
          alert('Both requests are now complete');
        }),
      );*/
  };
  return {
    login,
    loginWithUsernameAndPassword,
    createUserWithEmailAndPassword,
    sendPasswordResetEmail,
    signout,
    checkAuthStateChanges,
    getDataUsingSimpleGetCall,
    getDataUsingAsyncAwaitGetCall,
    postDataUsingSimplePostCall,
    multipleRequestsInSingleCall,
  };
};

export default AuthService;
