import axios from 'axios';
import { BASE_URL, CoinMarket_BASE_URL, Market_BASE_URL } from "../../constants/urls";
const BaseApi = axios.create({
  baseURL: BASE_URL, //'http://www.google.com',
  //headers: {},
});

export const MarketApi = axios.create({
  baseURL: Market_BASE_URL, //'http://www.google.com',
  //headers: {},
});
export const CoinMarketApi = axios.create({
  baseURL: CoinMarket_BASE_URL, //'http://www.google.com',
  //headers: {},
});
export default BaseApi;
