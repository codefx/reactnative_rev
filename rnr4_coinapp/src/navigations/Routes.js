import React, {useContext, useEffect, useState} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import AuthStack from './Stacks/Auth/AuthStack';
import {useAuthContext} from '../contexts';
import Loading from '../components/Loading';
import HomeStack from './Stacks/Home/HomeStack';
import delay from "../utils/delayFunction";

const Stack = createStackNavigator();


const Routes = () => {
  const {user, setUser, checkAuthStateChanges} = useAuthContext();
  const [isLoading, setIsLoading] = useState(false);
  const [isInitial, setIsInitial] = useState(true);

  useEffect(() => {
    setIsLoading(false);
      const cuser = checkAuthStateChanges(user);
      if (cuser != null) {
      console.log(user + ' 2 burası user ve '+ cuser);
      setIsLoading(false);
    }
    setIsInitial(false);
  }, []);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <NavigationContainer>
      {/*{user ? <HomeStack /> : <AuthStack />}*/}
      <HomeStack />
    </NavigationContainer>
  );
};

export default Routes;
