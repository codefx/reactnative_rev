import React from 'react';

import {Text, View} from 'react-native';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {MCIcon} from '../../../utils/vectorIconsPort';
import HomeScreen from '../../../screens/Home/HomeScreen';
import ProfileScreen from "../../../screens/Home/ProfileScreen";
import SettingsScreen from "../../../screens/Home/SettingsScreen";
import MarketScreen from "../../../screens/Home/MarketScreen";

const Tab = createBottomTabNavigator();
const HomeBottomTabStack = () => {
  return (
    <Tab.Navigator
      initialRouteName="HomeScreen"
      screenOptions={{
        tabBarActiveTintColor: '#91dc91',
        tabBarInactiveTintColor: '#d3e7e5',
        tabBarStyle: {
          // position: 'absolute',
          height: 70,
          backgroundColor: '#c4a491',
          paddingBottom: 18,
          paddingTop: 10,
        },
        tabBarLabelStyle: {
          textAlign: 'center',
          fontSize: 14,
        },
      }}>
      <Tab.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home Page',
          tabBarIcon: ({color, size}) => (
            <MCIcon name="home" color={color} size={size} />
          ),
        }}
      />
       <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarLabel: 'Profiliniz',
          tabBarIcon: ({color, size}) => (
            <MCIcon name="card-account-details-star" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          tabBarLabel: 'Ayarlar',
          tabBarIcon: ({color, size}) => (
            <MCIcon name="account-wrench" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Market"
        component={MarketScreen}
        options={{
          tabBarLabel: 'Market',
          tabBarIcon: ({color, size}) => (
            <MCIcon name="bitcoin" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default HomeBottomTabStack;
