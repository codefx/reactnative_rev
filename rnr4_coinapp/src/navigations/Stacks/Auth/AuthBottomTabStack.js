import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LoginScreen from '../../../screens/Auth/LoginScreen';
import {MCIcon} from '../../../utils/vectorIconsPort';
import RegisterScreen from '../../../screens/Auth/RegisterScreen';
import ResetPasswordScreen from '../../../screens/Auth/ResetPasswordScreen';




const Tab = createBottomTabNavigator();
const AuthBottomTabStack = () => {
  return (
    <Tab.Navigator
      initialRouteName="LoginScreen"
      /* tabBarOptions={{
        activeTintColor: '#0f0',
        inactiveTintColor: '#999',
        style: {
          backgroundColor: '#54c7e7',
          padding:16,
        },
        labelStyle: {
          textAlign: 'center',
          fontSize: 16
        },*/
      screenOptions={{
        tabBarActiveTintColor: '#91dc91',
        tabBarInactiveTintColor: '#d3e7e5',
        tabBarStyle: {
          // position: 'absolute',
          height: 80,
          backgroundColor: '#c4a491',
          paddingBottom: 20,
          paddingTop:10
        },
        tabBarLabelStyle: {
          textAlign: 'center',
          fontSize: 16,
        },
      }}>
      <Tab.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{
          tabBarLabel: 'Giriş',
          tabBarIcon: ({color, size}) => (
            <MCIcon name="account-lock" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="RegisterScreen"
        component={RegisterScreen}
        options={{
          tabBarLabel: 'Yeni Uye',
          tabBarIcon: ({color, size}) => (
            <MCIcon name="account-plus" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="ResetPasswordScreen"
        component={ResetPasswordScreen}
        options={{
          tabBarLabel: 'Şifre İşlemleri',
          tabBarIcon: ({color, size}) => (
            <MCIcon name="account-key" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default AuthBottomTabStack;
