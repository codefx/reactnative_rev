import React from 'react';

import {Text, View} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AuthBottomTabStack from './AuthBottomTabStack';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const AuthStack = () => {
  return (
    <Stack.Navigator initialRouteName="AuthBottomTabStack">
      <Stack.Screen
        name="AuthBottomTabStack"
        component={AuthBottomTabStack}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default AuthStack;
