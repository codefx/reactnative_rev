import React from 'react';

import Routes from './navigations/Routes';
import AuthProvider from './contexts/AuthContextProvider';

const App = () => {
  return (
    <AuthProvider><Routes /></AuthProvider>
  );
};
export default App;
