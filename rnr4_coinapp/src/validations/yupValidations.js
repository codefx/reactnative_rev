import * as yup from 'yup';

export const myLoginValidateSchema = yup.object().shape({
  email: yup
    .string()
    .required('cannot be empty')
    .email('please enter a valid email'),
  password: yup
    .string()
    .required('cannot be empty')
    .min(6, ({min}) => 'password must at least be ' + min + ' characters'),
});
export const myRegisterValidateSchema = yup.object().shape({
  name: yup
    .string()
    .required('cannot be empty')
    .min(3, ({min}) => 'name must at least be ' + min + ' characters'),
  email: yup
    .string()
    .required('cannot be empty')
    .email('please enter a valid email'),
  password: yup
    .string()
    .required('cannot be empty')
    .min(6, ({min}) => 'password must at least be ' + min + ' characters')
    .matches(/\w*[a-z]\w*/, 'En az 1 adet kucuk harf kullanmalisiniz!')
    .matches(/\w*[A-Z]\w*/, 'En az 1 adet Buyuk harf kullanmalisiniz!')
    .matches(/\d/, 'En az 1 adet rakam kullanmalisiniz!')
    .matches(/[!@#$^%&*()\-_"=+{}; :,<.>]/, 'En az 1 adet ozel karakter kullanmalisiniz!'),
  confirmPassword: yup
    .string()
    .required('cannot be empty')
    .oneOf([yup.ref('password')], 'Passwords are not equal')
});

export const myResetPasswordValidateSchema = yup.object().shape({
  email: yup
    .string()
    .required('cannot be empty')
    .email('please enter a valid email')
});
