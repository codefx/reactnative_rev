import React from 'react';
import 'react-native-gesture-handler';

import AuthCtxProvider from './src/contexts/AuthContext';
import Routes from './src/navigations/Routes';

const App = () => {
  return (
    <AuthCtxProvider>
      <Routes />
    </AuthCtxProvider>
  );
};

export default App;
