const Home = 'HomeScreen';
const SignUp = 'SignUpScreen';
const Scan = 'ScanScreen';
const Tabs = 'Tabs';
const User = 'User';
const ScanProduct = 'ScanProduct';

const routeNames = {
  Home,
  SignUp,
  Scan,
  Tabs,
  User,
  ScanProduct,
};
export default routeNames;
