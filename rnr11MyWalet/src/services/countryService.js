import {countriesUrl, countryFlagsUrl} from '../constants/urls';
import axios from 'axios';

export const getCountryData = async () => {
  const areasData = await axios.get(countriesUrl).then(resp => resp.data);
  const data = areasData.map(item => {
    let suffixes;
    try {
      if (item.idd.suffixes.length > 1) {
        suffixes = item.idd.suffixes[0];
      } else {
        suffixes = item.idd.suffixes;
      }
    } catch (e) {
      console.log(e.message);
    }

    return {
      code: item.cca2,
      name: item.name.common,
      callingCode: `${item.idd.root}-${suffixes}`,
      flag: `${countryFlagsUrl}${item.cca2.toLowerCase()}.jpg`,
    };
  });

  return data;
};
