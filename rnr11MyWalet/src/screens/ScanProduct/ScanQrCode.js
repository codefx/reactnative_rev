import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import CameraFrame from './CameraFrame';
import {COLORS, FONTS, SIZES} from '../../theme/theme2';

const ScanQrCode = () => {
  return (
    <View style={styles.qrContainerView}>
      <CameraFrame />
      {/* label1  */}
      <View style={styles.qrLabel1View}>
        <Text style={styles.qrLabel1Text}>Scan...</Text>
      </View>
      {/* label2 */}
      <View style={styles.qrLabel2View}>
        <Text style={styles.qrLabel2Text}>
          Align the code to be in the middle of the box
        </Text>
      </View>
    </View>
  );
};

export default ScanQrCode;

const styles = StyleSheet.create({
  qrContainerView: {position: 'absolute', top: 0, left: 0, right: 0, bottom: 0},
  qrLabel1View: {
    position: 'absolute',
    top: '15%',
    left: 0,
    right: 0,
    alignItems: 'center',
  },
  qrLabel1Text: {...FONTS.h1, color: COLORS.light},
  qrLabel2View: {
    position: 'absolute',
    top: SIZES.height * 0.3 + 220,
    left: 0,
    right: 0,
    alignItems: 'center',
  },
  qrLabel2Text: {...FONTS.body3, color: COLORS.light},
});
