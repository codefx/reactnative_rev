import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import {MotiView} from 'moti';
import {Shadow} from 'react-native-shadow-2';
import {CameraContext} from '../../contexts/CameraContext';
import images from '../../components/images';
import {COLORS, FONTS, SIZES} from '../../theme/theme2';

const ProductMotiCard = () => {
  const {productAnimationState} = useContext(CameraContext);
  return (
    <MotiView state={productAnimationState} style={styles.motiProduct}>
      <Shadow>
        <Pressable style={styles.pressable}>
          {/* Image */}
          <Image source={images.luggage_01} style={styles.prssbleImage} />
          {/* Product name & SKU */}
          <View style={styles.prssbleView.container}>
            <Text style={styles.prssbleView.title}>Valiz Pro </Text>
            <Text style={styles.prssbleView.sku}>SKU: 12345678 </Text>
          </View>
          {/* Price */}
          <Text style={styles.price}> $ 48.50</Text>
        </Pressable>
      </Shadow>
    </MotiView>
  );
};

export default ProductMotiCard;

const styles = StyleSheet.create({
  motiProduct: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 120,
    paddingVertical: SIZES.radius,
    alignItems: 'center',
    zIndex: 1,
  },
  pressable: {
    flex: 1,
    flexDirection: 'row',
    width: SIZES.width - SIZES.padding * 2,
    alignItems: 'center',
    paddingHorizontal: SIZES.radius,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.Light,
  },
  prssbleImage: {width: 60, height: 62, borderRadius: 30},
  prssbleView: {
    container: {flex: 1, marginLeft: SIZES.radius},
    title: {...FONTS.h3, color: COLORS.primary},
    sku: {...FONTS.body4},
  },
  price: {...FONTS.h3, color: COLORS.primary},
});
