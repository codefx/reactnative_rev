import {StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme2';
import {CameraContext} from '../../contexts/CameraContext';
import TextButton from '../../components/TextButton';

const Footer = () => {
  const {selectedOption, setSelectedOption, options} =
    useContext(CameraContext);
  return (
    <View style={styles.container}>
      <TextButton
        label="Scan QR Code"
        contentContainerStyle={{
          ...styles.tbQR,
          backgroundColor:
            selectedOption == options.qr ? COLORS.primary : COLORS.lightGrey,
        }}
        labelStyle={{
          ...FONTS.h3,
          color:
            selectedOption == options.qr ? COLORS.secondary : COLORS.primary,
        }}
        onPress={() => {
          setSelectedOption(options.qr);
        }}
      />
      <TextButton
        label="Scan Camera"
        contentContainerStyle={{
          ...styles.tbQR,
          backgroundColor:
            selectedOption == options.camera
              ? COLORS.primary
              : COLORS.lightGrey,
        }}
        labelStyle={{
          ...FONTS.h3,
          color:
            selectedOption == options.camera
              ? COLORS.secondary
              : COLORS.primary,
        }}
        onPress={() => {
          setSelectedOption(options.camera);
        }}
      />
    </View>
  );
};

export default Footer;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 90,
    paddingTop: SIZES.radius,
    paddingHorizontal: SIZES.radius,
    backgroundColor: COLORS.light,
    gap: SIZES.radius,
  },
  tbQR: {
    flex: 1,
    height: 55,
    borderRadius: SIZES.radius,
  },
});
