import {StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import IconButton from '../../components/IconButton';
import icons from '../../components/icons';
import {CameraContext} from '../../contexts/CameraContext';
import {COLORS, SIZES} from '../../theme/theme2';

const ScanCamera = () => {
  const {productAnimationState, loaderAnimationState} =
    useContext(CameraContext);
  return (
    <View style={styles.cmrOver}>
      <IconButton
        icon={icons.scan}
        containerStyle={styles.icoButContSty}
        iconStyle={styles.iconStyle}
        onPress={() => {
          loaderAnimationState.transitionTo('start');
          setTimeout(() => {
            loaderAnimationState.transitionTo('stop');
            productAnimationState.transitionTo('show');
          }, 2000);
        }}
      />
    </View>
  );
};

export default ScanCamera;

const styles = StyleSheet.create({
  cmrOver: {
    position: 'absolute',
    alignItems: 'center',
    bottom: SIZES.padding,
    left: 0,
    right: 0,
  },
  icoButContSty: {
    height: 60,
    width: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.light,
  },
  iconStyle: {width: 50, height: 50, tintColor: COLORS.primary},
});
