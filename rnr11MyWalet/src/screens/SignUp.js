import React, {useContext, useEffect} from 'react';
import {
  KeyboardAvoidingView,
  ScrollView,
  Platform,
  StyleSheet,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Loading from '../components/Loading';
import SignUpForm from '../components/SignUp/form/SignUpForm';
import Header from '../components/SignUp/Header';
import Logo from '../components/SignUp/Logo';
import AreaCodesModal from '../components/SignUp/modal/AreaCodesModal';
import SignUpButton from '../components/SignUp/SignUpButton';

import {AuthContext} from '../contexts/AuthContext';
import {COLORS} from '../theme/theme';

const SignUp = ({navigation}) => {
  const {setSelectedArea, setAreas, getCountryData, areas} =
    useContext(AuthContext);

  useEffect(() => {
    async function some() {
      let dt = await getCountryData();
      setAreas(dt);
      if (dt.length > 0) {
        let defaultData = dt.filter(a => a.code === 'TR');
        console.log(defaultData[0].name);
        if (defaultData.length > 0) {
          setSelectedArea(defaultData[0]);
        }
      }
    }
    some();
  }, [getCountryData, setAreas, setSelectedArea]);

  return areas.length > 0 ? (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      style={styles.linear}>
      <LinearGradient
        colors={[COLORS.lime, COLORS.emerald]}
        style={styles.linear}>
        <ScrollView>
          <Header />
          <Logo />
          <SignUpForm />
          <SignUpButton navigation={navigation} />
        </ScrollView>
      </LinearGradient>
      <AreaCodesModal />
    </KeyboardAvoidingView>
  ) : (
    <Loading />
  );
};

export default SignUp;
const styles = StyleSheet.create({
  linear: {flex: 1},
});
