import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import Promos from '../components/Home/promos/Promos';
import WalletCtxProvider from '../contexts/WalletContext';
import {COLORS} from '../theme/theme';

const HomeScreen = () => {
  return (
    <WalletCtxProvider>
      <SafeAreaView style={styles.safeView}>
        <Promos />
      </SafeAreaView>
    </WalletCtxProvider>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  safeView: {flex: 1, backgroundColor: COLORS.white},
});
