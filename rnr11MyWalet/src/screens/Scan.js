import React from 'react';
import {View, StyleSheet} from 'react-native';

import Header from '../components/Scan/Header';
import PaymentMethods from '../components/Scan/PaymentMethods';
import ScanFocus from '../components/Scan/ScanFocus';
import VisCamCodeScan from '../components/Scan/VisCamCodeScan';

import {COLORS} from '../theme/theme';

const Scan = ({navigation}) => {
  function onBarCodeRead(result) {
    console.log(result.data);
  }

  return (
    <View style={styles.main}>
      <VisCamCodeScan />
      <Header navigation={navigation} />
      <ScanFocus />
      <PaymentMethods />
    </View>
  );
};

export default Scan;

const styles = StyleSheet.create({
  main: {flex: 1, backgroundColor: COLORS.transparent},
});
