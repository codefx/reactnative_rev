import React from 'react';
import 'react-native-gesture-handler';

import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import routeNames from '../constants/routeNames';
import SignUp from '../screens/SignUp';
import ScanProduct from '../screens/ScanProduct/ScanProduct';
import Tabs from './tabs';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    border: 'transparent',
  },
};

const Stack = createStackNavigator();
const Routes = () => {
  return (
    <NavigationContainer theme={theme}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName={routeNames.ScanProduct}>
        <Stack.Screen name={routeNames.ScanProduct} component={ScanProduct} />
        <Stack.Screen name={routeNames.SignUp} component={SignUp} />
        {/* Tabs */}
        <Stack.Screen name={routeNames.Home} component={Tabs} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
