import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import CustomTabBar from '../components/TabBar/CustomTabBar';
import icons from '../components/icons';

import HomeScreen from '../screens/HomeScreen';
import Scan from '../screens/Scan';

import routeNames from '../constants/routeNames';
import {COLORS} from '../theme/theme';
import TabBarCustomButton from '../components/TabBar/TabBarCustomButton';
import User from '../screens/User';

const Tab = createBottomTabNavigator();

const Tabs = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarShowLabel: false,
        tabBarStyle: {...styles.tabBarStyle},
        tabBarActiveTintColor: COLORS.white,
        tabBarInactiveTintColor: COLORS.secondary,
      }}
      tabBar={props => <CustomTabBar props={props} />}>
      <Tab.Screen
        name={routeNames.Home}
        component={HomeScreen}
        options={{
          tabBarIcon: ({focused, color, size}) => (
            <Image
              source={icons.more}
              resizeMode="contain"
              style={{
                ...styles.screens,
              }}
            />
          ),
          tabBarButton: props => <TabBarCustomButton {...props} />,
        }}
      />
      <Tab.Screen
        name={routeNames.Scan}
        component={Scan}
        options={{
          tabBarIcon: ({focused, color, size}) => (
            <Image
              source={icons.scan}
              resizeMode="contain"
              style={{
                ...styles.screens,
              }}
            />
          ),
          tabBarButton: props => <TabBarCustomButton {...props} />,
        }}
      />
      <Tab.Screen
        name={routeNames.User}
        component={User}
        options={{
          tabBarIcon: ({focused, color, size}) => (
            <Image
              source={icons.user}
              resizeMode="contain"
              style={{
                ...styles.screens,
              }}
            />
          ),
          tabBarButton: props => <TabBarCustomButton {...props} />,
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  screens: {width: 25, height: 25},
  tabBarStyle: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    elevation: 0,
  },
});

export default Tabs;
