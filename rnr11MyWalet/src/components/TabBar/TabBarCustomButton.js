import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {Path, Svg} from 'react-native-svg';
import {COLORS} from '../../theme/theme';

const TabBarCustomButton = ({
  accessibilityLabel,
  accessibilityState,
  children,
  onPress,
}) => {
  var isSelected = accessibilityState.selected;

  if (isSelected) {
    return (
      <View style={styles.selected.container}>
        <View style={styles.selected.mainView}>
          <View style={styles.selected.head} />
          <Svg width={75} height={61} viewBox="0 0 75 61">
            <Path
              d="M75.2 0v61H0V0c4.1 0 7.4 3.1 7.9 7.1C10 21.7 22.5 33 37.7 33c15.2 0 27.7-11.3 29.7-25.9.5-4 3.9-7.1 7.9-7.1h-.1z"
              fill={COLORS.white}
            />
          </Svg>
          <View style={styles.selected.bottom} />
        </View>

        <TouchableOpacity style={styles.selected.topacity} onPress={onPress}>
          {children}
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <TouchableOpacity
        style={styles.notSelected.topacity}
        activeOpacity={1}
        onPress={onPress}>
        {children}
      </TouchableOpacity>
    );
  }
};

export default TabBarCustomButton;

const styles = StyleSheet.create({
  selected: {
    container: {flex: 1, alignItems: 'center'},
    mainView: {
      flexDirection: 'row',
      position: 'absolute',
      top: 0,
    },
    head: {flex: 1, backgroundColor: COLORS.white},
    bottom: {flex: 1, backgroundColor: COLORS.white},
    topacity: {
      top: -22.5,
      justifyContent: 'center',
      alignItems: 'center',
      width: 50,
      height: 50,
      borderRadius: 25,
      backgroundColor: COLORS.primary,
      shadowColor: COLORS.primary,
      shadowOffset: {
        width: 0,
        height: 10,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 5,
    },
  },
  notSelected: {
    topacity: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: 50,
      height: 50,
      backgroundColor: COLORS.white,
    },
  },
});
