import React from 'react';
import {StyleSheet, View} from 'react-native';
import {BottomTabBar} from '@react-navigation/bottom-tabs';
import {COLORS} from '../../theme/theme';
import {isIphoneX} from 'react-native-iphone-x-helper';

const CustomTabBar = props => {
  if (isIphoneX()) {
    return (
      <View>
        <View style={styles.bg} />
        <BottomTabBar {...props.props} />
      </View>
    );
  } else {
    return <BottomTabBar {...props.props} />;
  }
};

export default CustomTabBar;

const styles = StyleSheet.create({
  bg: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 30,
    backgroundColor: COLORS.white,
  },
});
