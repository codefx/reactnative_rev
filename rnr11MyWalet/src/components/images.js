const wallieLogo = require('../assets/images/wallie-logo.png');
const banner = require('../assets/images/banner.png');
const promoBanner = require('../assets/images/promo-banner.png');
const focus = require('../assets/images/focus.png');
const mylogo = require('../assets/images/mylogo.png');
const luggage_01 = require('../assets/images/luggage_01.png');

// Dummy
const usFlag = require('../assets/images/us-flag.jpg');

const images = {
  wallieLogo,
  banner,
  promoBanner,
  focus,
  mylogo,
  luggage_01,

  // Dummy
  usFlag,
};
export default images;
