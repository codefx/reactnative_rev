import {StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../theme/theme';

const FormInput = ({
  containerStyle,
  inputContainerStyle,
  placeholder,
  inputStyle,
  value = '',
  prependComponent,
  appendComponent,
  onChange,
  onPress,
  editable,
  secureTextEntry,
  keyboardType = 'default',
  autoCompleteType = 'off',
  autoCapitalize = 'none',
  maxLength,
  placeholderTextColor = COLORS.grey60,
}) => {
  return (
    <View style={{...containerStyle}}>
      <View style={{...styles.innerMain, ...inputContainerStyle}}>
        {prependComponent}
        <TextInput
          style={{...styles.tinput, ...inputStyle}}
          value={value}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor}
          secureTextEntry={secureTextEntry}
          keyboardType={keyboardType}
          autoCompleteType={autoCompleteType}
          autoCapitalize={autoCapitalize}
          maxLength={maxLength}
          onChange={text => onChange(text)}
          onPressIn={onPress}
          editable={editable}
        />
        {appendComponent}
      </View>
    </View>
  );
};

export default FormInput;

const styles = StyleSheet.create({
  innerMain: {
    flexDirection: 'row',
    height: 55,
    paddingHorizontal: SIZES.radius,
    borderRadius: SIZES.radius,
    alignItems: 'center',
    backgroundColor: COLORS.lightGrey,
  },
  tinput: {
    flex: 1,
    paddingVertical: 0,
    ...FONTS.body3,
  },
});
