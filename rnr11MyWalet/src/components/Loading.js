import React from 'react';
import {View, ActivityIndicator} from 'react-native';

const Loading = props => (
  <View>
    <ActivityIndicator size="large" color="#00f" />
  </View>
);

export default Loading;
