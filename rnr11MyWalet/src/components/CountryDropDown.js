import React from 'react';
import {View, TouchableOpacity, Text, Image, StyleSheet} from 'react-native';

import {FONTS, SIZES, COLORS} from '../theme/theme';
import icons from '../constants/icons';

const CountryDropDown = ({containerStyle, selectedCountry, onPress}) => {
  return (
    <TouchableOpacity
      style={{...styles.dropdownPressable, ...containerStyle}}
      onPress={onPress}>
      <View style={styles.mainView}>
        {!selectedCountry && <Text style={styles.title}>Country</Text>}

        {selectedCountry && (
          <>
            <Image
              source={{uri: selectedCountry?.flag}}
              resizeMode="cover"
              style={styles.flag}
            />
            <Text style={styles.cntryName}>{selectedCountry?.name}</Text>
          </>
        )}
      </View>
      <Image source={icons.arrow_down_fill} style={styles.downArrow} />
    </TouchableOpacity>
  );
};

export default CountryDropDown;
const styles = StyleSheet.create({
  dropdownPressable: {
    flexDirection: 'row',
    height: SIZES.height > 800 ? 55 : 45,
    paddingHorizontal: SIZES.radius,
    borderRadius: SIZES.radius,
    alignItems: 'center',
    backgroundColor: COLORS.lightGrey,
  },
  mainView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {color: COLORS.grey, ...FONTS.body3},
  flag: {
    width: 30,
    height: 30,
    borderRadius: SIZES.radius,
  },
  cntryName: {flex: 1, marginLeft: SIZES.radius, ...FONTS.h3},
  downArrow: {
    width: 30,
    height: 30,
  },
});
