import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {FONTS, SIZES} from '../../theme/theme';
import icons from '../icons';

const PaymentMethodButton = ({bgColor, tintColor, title}) => {
  return (
    <TouchableOpacity
      style={styles.topacity}
      onPress={() => console.log(`${title}`)}>
      <View style={{...styles.view, backgroundColor: bgColor}}>
        <Image
          source={icons.phone}
          resizeMode="cover"
          style={{...styles.icon, tintColor: tintColor}}
        />
      </View>
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
};

export default PaymentMethodButton;

const styles = StyleSheet.create({
  topacity: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  view: {
    width: 40,
    height: 40,

    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  icon: {
    height: 25,
    width: 25,
  },
  text: {marginLeft: SIZES.padding, ...FONTS.body4},
});
