import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import images from '../images';

const ScanFocus = () => {
  return (
    <View style={styles.main}>
      <Image source={images.focus} resizeMode="stretch" style={styles.image} />
    </View>
  );
};

export default ScanFocus;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    marginTop: '-55%',
    width: 200,
    height: 300,
  },
});
