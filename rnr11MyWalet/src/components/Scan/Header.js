import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import icons from '../icons';
import routeNames from '../../constants/routeNames';

const Header = ({navigation}) => {
  return (
    <View style={styles.mainView}>
      <TouchableOpacity
        style={styles.left.topacity}
        onPress={() => navigation.navigate(routeNames.Home)}>
        <Image source={icons.close} style={styles.left.icon} />
      </TouchableOpacity>

      <View style={styles.middle.view}>
        <Text style={styles.middle.text}>Scan for Payment</Text>
      </View>

      <TouchableOpacity
        style={styles.right.topacity}
        onPress={() => console.log('Info')}>
        <Image source={icons.info} style={styles.right.icon} />
      </TouchableOpacity>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    marginTop: SIZES.padding * 4,
    paddingHorizontal: SIZES.padding * 3,
  },
  left: {
    topacity: {
      width: 45,
      alignItems: 'center',
      justifyContent: 'center',
    },
    icon: {
      height: 20,
      width: 20,
      tintColor: COLORS.white,
    },
  },
  middle: {
    view: {flex: 1, alignItems: 'center', justifyContent: 'center'},
    text: {color: COLORS.white, ...FONTS.body3},
  },
  right: {
    topacity: {
      height: 45,
      width: 45,
      backgroundColor: COLORS.green,
      borderRadius: 10,
      alignItems: 'center',
      justifyContent: 'center',
    },
    icon: {
      height: 25,
      width: 25,
      tintColor: COLORS.white,
    },
  },
});
