import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import PaymentMethodButton from './PaymentMethodButton';

const PaymentMethods = () => {
  return (
    <View style={styles.mainView}>
      <Text style={styles.title}>Another payment methods</Text>

      <View style={styles.buttonsContainer}>
        <PaymentMethodButton
          bgColor={COLORS.lightpurple}
          tintColor={COLORS.purple}
          title="Phone Number"
        />
        <PaymentMethodButton
          bgColor={COLORS.lightGreen}
          tintColor={COLORS.primary}
          title="Barcode"
        />
      </View>
    </View>
  );
};

export default PaymentMethods;

const styles = StyleSheet.create({
  mainView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 220,
    padding: SIZES.padding * 3,
    borderTopLeftRadius: SIZES.radius,
    borderTopRightRadius: SIZES.radius,
    backgroundColor: COLORS.white,
  },
  title: {...FONTS.h4},
  buttonsContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: SIZES.padding * 2,
    justifyContent: 'space-between',
  },
});
