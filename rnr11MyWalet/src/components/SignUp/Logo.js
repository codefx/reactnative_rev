import {Image, StyleSheet, View} from 'react-native';
import React from 'react';
import {SIZES} from '../../theme/theme';
import images from '../images';

const Logo = () => {
  return (
    <View style={styles.view}>
      <Image source={images.mylogo} resizeMode="contain" style={styles.image} />
    </View>
  );
};

export default Logo;

const styles = StyleSheet.create({
  view: {
    marginTop: SIZES.padding * 5,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: '60%',
  },
});
