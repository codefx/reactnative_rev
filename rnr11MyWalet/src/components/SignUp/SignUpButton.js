import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import routeNames from '../../constants/routeNames';

const SignUpButton = ({navigation}) => {
  return (
    <View style={styles.mainView}>
      <TouchableOpacity
        style={styles.topacity}
        onPress={() => navigation.navigate(routeNames.Home)}>
        <Text style={{color: COLORS.white, ...FONTS.h3}}>Continue</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SignUpButton;

const styles = StyleSheet.create({
  mainView: {margin: SIZES.padding * 3},
  topacity: {
    height: 60,
    backgroundColor: COLORS.black,
    borderRadius: SIZES.radius / 1.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
