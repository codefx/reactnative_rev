import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import icons from '../icons';

const Header = () => {
  return (
    <TouchableOpacity
      style={styles.topacity}
      onPress={() => console.log('Sign Up')}>
      <Image source={icons.back} resizeMode="contain" style={styles.image} />

      <Text style={styles.text}>Sign Up</Text>
    </TouchableOpacity>
  );
};

export default Header;

const styles = StyleSheet.create({
  topacity: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: SIZES.padding * 6,
    paddingHorizontal: SIZES.padding * 2,
  },
  image: {
    width: 20,
    height: 20,
    tintColor: COLORS.white,
  },
  text: {
    marginLeft: SIZES.padding * 1.5,
    color: COLORS.white,
    ...FONTS.h4,
  },
});
