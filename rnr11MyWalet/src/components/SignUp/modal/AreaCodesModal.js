import React, {useContext} from 'react';
import {
  FlatList,
  Modal,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {AuthContext} from '../../../contexts/AuthContext';
import AreaCodesModalRenderItem from './AreaCodesModalRenderItem';
import {COLORS, SIZES} from '../../../theme/theme';
import HeaderSearch from './HeaderSearch';

const AreaCodesModal = () => {
  const {setModalVisible, modalVisible, setSelectedArea, areas, searchData} =
    useContext(AuthContext);

  return (
    <Modal animationType="slide" transparent={true} visible={modalVisible}>
      <TouchableWithoutFeedback onPress={() => setModalVisible(false)}>
        <View style={styles.mainView}>
          <View style={styles.innerView}>
            <HeaderSearch />

            <FlatList
              data={searchData == null ? areas : searchData}
              renderItem={({item}) =>
                AreaCodesModalRenderItem({
                  item,
                  setModalVisible,
                  setSelectedArea,
                })
              }
              keyExtractor={item => item.code}
              showsVerticalScrollIndicator={false}
              style={styles.flatlist}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default AreaCodesModal;

const styles = StyleSheet.create({
  mainView: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  innerView: {
    height: 400,
    width: SIZES.width * 0.8,
    backgroundColor: COLORS.lightGreen,
    borderRadius: SIZES.radius,
  },
  flatlist: {
    padding: SIZES.padding * 2,
    marginBottom: SIZES.padding * 2,
  },
});
