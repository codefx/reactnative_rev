import {StyleSheet, TextInput, View} from 'react-native';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {AuthContext} from '../../../contexts/AuthContext';

const HeaderSearch = () => {
  const {areas, searchText, setSearchText, searchData, setSearchData} =
    useContext(AuthContext);
  useEffect(() => {
    let textOut;
    const handleSearch = async text => {
      const filteredData = await areas.filter(item => {
        textOut = text;
        return item.name.toLowerCase().includes(text.toLowerCase());
      });
      setSearchData(filteredData);
    };
    textOut.length >= 3 ? handleSearch(searchText) : '';
  }, []);

  return (
    <View style={styles.main}>
      <TextInput
        autoCapitalize="none"
        autoCorrect={false}
        clearButtonMode="while-editing"
        value={searchText}
        onChangeText={text => setSearchText(text)}
        placeholder="Search"
        style={styles.text}
        on
      />
    </View>
  );
};

export default HeaderSearch;

const styles = StyleSheet.create({
  main: {
    backgroundColor: '#fff',
    padding: 20,
    margin: 10,
    borderRadius: 40,
  },
  text: {backgroundColor: '#ffe', paddingHorizontal: 10},
});
