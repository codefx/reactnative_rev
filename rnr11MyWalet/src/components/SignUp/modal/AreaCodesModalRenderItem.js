import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {FONTS, SIZES} from '../../../theme/theme';

const AreaCodesModalRenderItem = ({item, setSelectedArea, setModalVisible}) => {
  console.log(item);
  return (
    <TouchableOpacity
      style={styles.pressable}
      onPress={() => {
        setSelectedArea(item);
        setModalVisible(false);
      }}>
      <Image source={{uri: item.flag}} style={styles.image} />
      <Text style={styles.text}>{item.name}</Text>
    </TouchableOpacity>
  );
};

export default AreaCodesModalRenderItem;

const styles = StyleSheet.create({
  pressable: {padding: SIZES.padding, flexDirection: 'row'},
  image: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  text: {...FONTS.body4},
});
