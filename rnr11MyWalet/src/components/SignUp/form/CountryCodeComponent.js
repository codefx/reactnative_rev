import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useContext} from 'react';
import {COLORS, FONTS} from '../../../theme/theme';
import icons from '../../icons';
import {AuthContext} from '../../../contexts/AuthContext';

const CountryCodeComponent = () => {
  const {setModalVisible, selectedArea} = useContext(AuthContext);
  return (
    <TouchableOpacity
      style={styles.topacity}
      onPress={() => setModalVisible(true)}>
      <View style={styles.viewDown}>
        <Image source={icons.down} style={styles.iconDown} />
      </View>
      <View style={styles.viewFlag}>
        <Image
          source={{uri: selectedArea?.flag}}
          resizeMode="contain"
          style={styles.imageFlag}
        />
      </View>

      <View style={styles.viewCallCode}>
        <Text style={styles.textCallcode}>{selectedArea?.callingCode}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default CountryCodeComponent;

const styles = StyleSheet.create({
  topacity: {
    flex: 1,
    width: 100,
    height: 50,
    marginHorizontal: 5,
    borderBottomColor: COLORS.white,
    borderBottomWidth: 1,
    flexDirection: 'row',
    ...FONTS.body2,
  },
  viewDown: {justifyContent: 'center'},
  iconDown: {
    width: 10,
    height: 10,
    tintColor: COLORS.white,
  },
  viewFlag: {justifyContent: 'center', marginLeft: 5},
  imageFlag: {
    width: 30,
    height: 30,
  },
  viewCallCode: {justifyContent: 'center', marginLeft: 5},
  textCallcode: {color: COLORS.white, ...FONTS.body3},
});
