import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {COLORS, FONTS, SIZES} from '../../../theme/theme';
import MyTextInput from './MyTextInput';
import CountryCodeComponent from './CountryCodeComponent';
import {AuthContext} from '../../../contexts/AuthContext';
import icons from '../../icons';

const SignUpForm = () => {
  const {showPassword, setShowPassword} = useContext(AuthContext);
  return (
    <View style={styles.mainView}>
      {/* Full Name */}
      <View style={styles.fullName.view}>
        <Text style={styles.fullName.text}>Full Name</Text>
        <MyTextInput placeholder="Enter Full Name" />
      </View>

      {/* Phone Number */}
      <View style={styles.phoneNumber.mainView}>
        <Text style={styles.phoneNumber.title}>Phone Number</Text>

        <View style={styles.phoneNumber.innerView}>
          {/* Country Code */}
          <CountryCodeComponent />
          {/* Phone Number */}
          <MyTextInput
            placeholder="Enter Phone Number"
            keyboardType="phone-pad"
          />
        </View>
      </View>
      {/* Password */}
      <View style={styles.phoneNumber.mainView}>
        <Text style={styles.phoneNumber.title}>Password</Text>
        <MyTextInput
          placeholder="Enter Password"
          secureTextEntry={showPassword}
        />

        <TouchableOpacity
          style={styles.password.topacity}
          onPress={() => setShowPassword(prev => !prev)}>
          <Image
            source={showPassword ? icons.disable_eye : icons.eye}
            style={styles.password.image}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SignUpForm;

const styles = StyleSheet.create({
  mainView: {
    marginTop: SIZES.padding * 3,
    marginHorizontal: SIZES.padding * 3,
  },
  fullName: {
    view: {marginTop: SIZES.padding * 3},
    text: {color: COLORS.lightGreen, ...FONTS.body3},
  },
  phoneNumber: {
    mainView: {marginTop: SIZES.padding * 2},
    title: {color: COLORS.lightGreen, ...FONTS.body3},
    innerView: {flexDirection: 'row'},
  },
  password: {
    topacity: {
      position: 'absolute',
      right: 0,
      bottom: 10,
      height: 30,
      width: 30,
    },
    image: {
      height: 20,
      width: 20,
      tintColor: COLORS.white,
    },
  },
});
