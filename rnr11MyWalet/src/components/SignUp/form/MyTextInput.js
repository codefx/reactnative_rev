import {StyleSheet, TextInput} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../../theme/theme';

const MyTextInput = ({placeholder, show, keyboardType = 'email-address'}) => {
  return (
    <TextInput
      style={styles.main}
      placeholder={placeholder}
      placeholderTextColor={COLORS.white}
      selectionColor={COLORS.white}
      keyboardType={keyboardType}
      secureTextEntry={show}
    />
  );
};

export default MyTextInput;

const styles = StyleSheet.create({
  main: {
    flex: 2,
    witdh: '100%',
    marginVertical: SIZES.padding,
    borderBottomColor: COLORS.white,
    borderBottomWidth: 1,
    height: 40,
    color: COLORS.white,
    ...FONTS.body3,
  },
});
