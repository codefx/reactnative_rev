import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../../theme/theme';
import images from '../../images';

const PromosRenderItem = ({item}) => (
  <TouchableOpacity
    style={styles.pressable}
    onPress={() => console.log(item.title)}>
    <View style={styles.viewTop}>
      <Image
        source={images.promoBanner}
        resizeMode="cover"
        style={styles.image}
      />
    </View>

    <View style={styles.viewBottom}>
      <Text style={styles.title}>{item.title}</Text>
      <Text style={styles.description}>{item.description}</Text>
    </View>
  </TouchableOpacity>
);

export default PromosRenderItem;

const styles = StyleSheet.create({
  pressable: {
    marginVertical: SIZES.base,
    width: SIZES.width / 2.5,
  },
  viewTop: {
    height: 80,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: COLORS.primary,
  },
  image: {
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  viewBottom: {
    padding: SIZES.padding,
    backgroundColor: COLORS.lightGray,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  title: {...FONTS.h4},
  description: {...FONTS.body4},
});
