import React from 'react';
import {View} from 'react-native';

import Banner from '../Banner';
import Features from '../feature/Features';
import Header from '../Header';
import PromoHeader from './PromoHeader';

const PromosHeaderComponent = () => {
  return (
    <View>
      <Header />
      <Banner />
      <Features />
      <PromoHeader />
    </View>
  );
};

export default PromosHeaderComponent;
