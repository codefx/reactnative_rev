import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../../theme/theme';

const PromoHeader = () => {
  return (
    <View style={styles.mainView}>
      <View style={styles.middle.View}>
        <Text style={styles.middle.text}>Special Promos</Text>
      </View>
      <TouchableOpacity onPress={() => console.log('View All')}>
        <Text style={styles.pressableText}>View All</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PromoHeader;

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    marginBottom: SIZES.padding,
  },
  middle: {
    View: {flex: 1},
    text: {...FONTS.h3},
  },
  pressableText: {color: COLORS.gray, ...FONTS.body4},
});
