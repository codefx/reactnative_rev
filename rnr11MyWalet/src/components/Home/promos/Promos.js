import {FlatList, StyleSheet, View} from 'react-native';
import React, {useContext} from 'react';
import {SIZES} from '../../../theme/theme';

import PromosHeaderComponent from './PromosHeaderComponent';
import PromosRenderItem from './PromosRenderItem';
import {WalletContext} from '../../../contexts/WalletContext';

const Promos = () => {
  const {specialPromos} = useContext(WalletContext);
  return (
    <FlatList
      ListHeaderComponent={PromosHeaderComponent}
      contentContainerStyle={styles.containerStyle}
      numColumns={2}
      columnWrapperStyle={styles.wrapperStyle}
      data={specialPromos}
      keyExtractor={item => `${item.id}`}
      renderItem={PromosRenderItem}
      showsVerticalScrollIndicator={false}
      ListFooterComponent={<View style={styles.footer} />}
    />
  );
};

export default Promos;

const styles = StyleSheet.create({
  containerStyle: {paddingHorizontal: SIZES.padding * 3},
  wrapperStyle: {justifyContent: 'space-between'},
  footer: {marginBottom: 80},
});
