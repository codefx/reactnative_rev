import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../../theme/theme';
import icons from '../icons';

const Header = () => {
  return (
    <View style={styles.mainView}>
      <View style={styles.topView}>
        <Text style={styles.titleBig}>Hello!</Text>
        <Text style={styles.title}>CFX</Text>
      </View>

      <View style={styles.bottomView}>
        <TouchableOpacity style={styles.pressable}>
          <Image source={icons.bell} style={styles.icon} />
          <View style={styles.redPoint} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  mainView: {flexDirection: 'row', marginVertical: SIZES.padding * 2},
  topView: {flex: 1},
  titleBig: {...FONTS.h1},
  title: {...FONTS.body2, color: COLORS.gray},
  bottomView: {alignItems: 'center', justifyContent: 'center'},
  pressable: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.lightGray,
  },
  icon: {
    width: 20,
    height: 20,
    tintColor: COLORS.secondary,
  },
  redPoint: {
    position: 'absolute',
    top: -5,
    right: -5,
    height: 10,
    width: 10,
    backgroundColor: COLORS.red,
    borderRadius: 5,
  },
});
