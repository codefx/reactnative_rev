import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import images from '../images';

const Banner = () => {
  return (
    <View style={styles.mainView}>
      <Image source={images.banner} resizeMode="cover" style={styles.image} />
    </View>
  );
};

export default Banner;

const styles = StyleSheet.create({
  mainView: {
    height: 120,
    borderRadius: 20,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 20,
  },
});
