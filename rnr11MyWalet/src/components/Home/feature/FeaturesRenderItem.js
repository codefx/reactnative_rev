import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {FONTS, SIZES} from '../../../theme/theme';

const FeaturesRenderItem = ({item}) => (
  <TouchableOpacity
    style={styles.pressable}
    onPress={() => console.log(item.description)}>
    <View
      style={{
        ...styles.view,
        backgroundColor: item.backgroundColor,
      }}>
      <Image
        source={item.icon}
        resizeMode="contain"
        style={{...styles.image, tintColor: item.color}}
      />
    </View>
    <Text style={styles.text}>{item.description}</Text>
  </TouchableOpacity>
);

export default FeaturesRenderItem;

const styles = StyleSheet.create({
  pressable: {
    marginBottom: SIZES.padding * 2,
    width: 60,
    alignItems: 'center',
  },
  view: {
    height: 50,
    width: 50,
    marginBottom: 5,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {height: 20, width: 20},
  text: {textAlign: 'center', flexWrap: 'wrap', ...FONTS.body4},
});
