import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {FONTS, SIZES} from '../../../theme/theme';

const FeaturesListHeader = () => {
  return (
    <View style={styles.view}>
      <Text style={styles.text}>Features</Text>
    </View>
  );
};

export default FeaturesListHeader;

const styles = StyleSheet.create({
  view: {marginBottom: SIZES.padding * 2},
  text: {...FONTS.h3},
});
