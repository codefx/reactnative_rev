import {FlatList, StyleSheet} from 'react-native';
import React, {useContext} from 'react';
import {SIZES} from '../../../theme/theme';
import {WalletContext} from '../../../contexts/WalletContext';
import FeaturesRenderItem from './FeaturesRenderItem';
import FeaturesListHeader from './FeaturesListHeader';

const Features = () => {
  const {features} = useContext(WalletContext);

  return (
    <FlatList
      ListHeaderComponent={FeaturesListHeader}
      data={features}
      numColumns={4}
      columnWrapperStyle={styles.wrapper}
      keyExtractor={item => `${item.id}`}
      renderItem={FeaturesRenderItem}
      style={styles.style}
    />
  );
};

export default Features;

const styles = StyleSheet.create({
  wrapper: {justifyContent: 'space-between'},
  style: {marginTop: SIZES.padding * 2},
});
