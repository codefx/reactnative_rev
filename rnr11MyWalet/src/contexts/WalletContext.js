import React, {createContext, useState} from 'react';
import featuresData from '../data/features';
import specialPromoData from '../data/specialPromos';

export const WalletContext = createContext({});

const WalletCtxProvider = ({children}) => {
  const [features, setFeatures] = useState(featuresData);
  const [specialPromos, setSpecialPromos] = useState(specialPromoData);

  const values = {
    features,
    setFeatures,
    specialPromos,
    setSpecialPromos,
  };
  return (
    <WalletContext.Provider value={values}>{children}</WalletContext.Provider>
  );
};

export default WalletCtxProvider;
