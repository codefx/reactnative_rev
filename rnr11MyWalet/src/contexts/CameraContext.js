import React, {createContext, useCallback, useState} from 'react';
import {Linking} from 'react-native';
import {useAnimationState} from 'moti';
import {Camera, useCameraDevices} from 'react-native-vision-camera';
import {BarcodeFormat, useScanBarcodes} from 'vision-camera-code-scanner';

import scan_product_option from '../constants/scan_product_option';

export const CameraContext = createContext({});

const CameraCtxProvider = ({children}) => {
  const devices = useCameraDevices();
  const device = devices.back;
  const [frameProcessor, barcodes] = useScanBarcodes([BarcodeFormat.QR_CODE]);

  // Moti
  const loaderAnimationState = useAnimationState({
    start: {opacity: 1},
    stop: {
      opacity: 0,
    },
  });
  const productAnimationState = useAnimationState({
    hide: {opacity: 0, translateY: -10},
    show: {opacity: 1, translateY: 10},
  });

  const [selectedOption, setSelectedOption] = useState(
    scan_product_option.camera,
  );
  const [barcode, setBarcode] = useState('');
  const [isScanned, setIsScanned] = useState(false);

  // Handler
  const requestCameraPermission = useCallback(async () => {
    const permission = await Camera.requestCameraPermission();
    if (permission === 'denied') await Linking.openSettings();
  }, []);
  //Barcode section
  const toggleActiveState = async () => {
    if (barcodes && barcodes.length > 0 && isScanned === false) {
      setIsScanned(true);
      barcodes.forEach(async scannedBarcode => {
        if (scannedBarcode.rawValue !== '') {
          setBarcode(scannedBarcode.rawValue);
          productAnimationState.transitionTo('show');
        }
      });
    }
  };

  const values = {
    device,
    frameProcessor,
    barcodes,
    loaderAnimationState,
    productAnimationState,
    selectedOption,
    setSelectedOption,
    options: scan_product_option,
    barcode,
    setBarcode,
    isScanned,
    setIsScanned,
    requestCameraPermission,
    toggleActiveState,
  };

  return (
    <CameraContext.Provider value={values}>{children}</CameraContext.Provider>
  );
};

export default CameraCtxProvider;
