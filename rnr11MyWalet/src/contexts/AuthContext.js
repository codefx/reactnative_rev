import React, {createContext, useState} from 'react';
import {getCountryData} from '../services/countryService';

export const AuthContext = createContext({});

const AuthCtxProvider = ({children}) => {
  const [showPassword, setShowPassword] = useState(false);
  const [areas, setAreas] = useState([]);
  const [selectedArea, setSelectedArea] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [searchData, setSearchData] = useState(null);

  const values = {
    showPassword,
    setShowPassword,
    areas,
    setAreas,
    selectedArea,
    setSelectedArea,
    modalVisible,
    setModalVisible,
    getCountryData: getCountryData,
    searchText,
    setSearchText,
    searchData,
    setSearchData,
  };

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
};

export default AuthCtxProvider;
