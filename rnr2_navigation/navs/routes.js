import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import SettingsScreen from '../screens/SettingsScreen';
import TodoScreen from '../screens/TodoScreen';
import LoginScreen from '../screens/LoginScreen';
import FactorialScreen from '../screens/FactorialScreen';
import TodoFlatListScreen from '../screens/TodoFlatList';

const Stack = createStackNavigator();
const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Settings"
          component={SettingsScreen}
          options={{
            title: 'Setting Page',
            headerStyle: {
              backgroundColor: '#7bcc62',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="Details"
          component={DetailsScreen}
          options={{
            title: 'Details Page',
            headerStyle: {
              backgroundColor: '#67e8cc',
            },

            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="Profile"
          component={ProfileScreen}
          options={{
            title: 'Profile Page',
            headerStyle: {
              backgroundColor: '#56b4d7',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="Todo"
          component={TodoScreen}
          options={{
            title: 'Todo App',
            headerStyle: {
              backgroundColor: '#bdba7a',
            },
            headerTintColor: '#222456',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="Factorial"
          component={FactorialScreen}
          options={{
            title: 'Factorial Calculating',
            headerStyle: {
              backgroundColor: '#e5a04e',
            },
            headerTintColor: '#188533',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{
            title: 'Login Page',
            headerStyle: {
              backgroundColor: '#ad0b0b',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
              color: '#fff',
            },
          }}
        />
        <Stack.Screen
          name="TodoFlatList"
          component={TodoFlatListScreen}
          options={{
            title: 'Todo with FlatList ',
            headerStyle: {
              backgroundColor: '#2bdc4e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
              color: '#fff',
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
