import React, {useState} from 'react';
import {SafeAreaView} from 'react-native';
import Routes from './navs/routes';

const App = () => {
  return <Routes />;
};
export default App;
