import { Text, TouchableOpacity, View } from "react-native";
//import IconM from "react-native-vector-icons/FontAwesome5";

export const MyItemSeparatorComponent = () => {
  return (
    <View
      style={{margin: 5, height: 1, width: '100%', backgroundColor: '#cde'}}
    />
  );
};

export const MyRenderItem = ({item, index}) => {
  return (
    <TouchableOpacity
      onPress={() => (item, index)}
      onLongPress={() => (item, index)}
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderColor: item.isDone ? '#0f0' : '#f00',
        borderWidth: 1,
        padding: 10,
        width: '90%',
        height: 60,
        borderRadius: 20,
        margin: 10,
      }}
      key={index.toString()}>
      <Text style={{color: 'rgba(95,135,215,0.9)'}}>{item.title}</Text>
      {/*<IconM*/}
      {/*  name={item.isDone ? 'check-circle' : 'circle-notch'}*/}
      {/*  size={30}*/}
      {/*  color={item.isDone ? '#0f0' : '#f00'}*/}
      {/*/>*/}
    </TouchableOpacity>
  );
};
