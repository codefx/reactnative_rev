import React, {useRef, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  //StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  //useColorScheme,
  View,
} from 'react-native';

const FactorialScreen = () => {
  const [sayi, setSayi] = useState('');
  const [factorial, setFactorial] = useState('');
  const tinputId = useRef();
  const calcFactorial = value => {
    let r = 1;
    for (let i = 1; i <= value; i++) {
      r *= i;
    }
    return r;
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
        }}>
        <TextInput
          ref={tinputId}
          value={sayi}
          onChangeText={value => {
            setSayi(value);
            setFactorial('');
          }}
          style={{
            borderColor: '#00f',
            borderWidth: 1,
            width: '100%',
            height: 60,
            borderRadius: 10,
          }}
        />
        <TouchableOpacity
          onPress={() => {
            setFactorial(x => calcFactorial(sayi));
          }}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#00f',
            padding: 5,
            width: '100%',
            height: 60,
            borderRadius: 25,
            marginTop: 10,
            marginBottom: 10,
          }}>
          <Text style={{color: '#fff', fontSize: 25}}>Calculate</Text>
        </TouchableOpacity>
        <Text style={{color: '#0f0', fontSize: 30}}>
          {factorial != '' ? sayi + '! = ' + factorial : ''}
        </Text>
      </View>
    </SafeAreaView>
  );
};
export default FactorialScreen;
