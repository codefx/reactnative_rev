import * as React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  SafeAreaView,
  ScrollView,
} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, width: '100%', alignItems: 'center'}}>
      <View
        style={{
          //flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <Text
          style={{
            fontSize: 21,
            textAlign: 'center',
            marginBottom: 16,
          }}>
          You are on Home Screen
        </Text>
      </View>
      <ScrollView style={{flex: 3, width: '100%', padding: 16}}>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#bdf3bd',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Settings', {screen: 'Settings'})}>
          <Text style={{color: '#406cf5', fontSize: 20}}> Go to Settings</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#935966',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Details')}>
          <Text style={{color: '#9fa9af', fontSize: 20}}>
            Open Details Screen
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#bdf3bd',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Factorial')}>
          <Text style={{color: '#406cf5', fontSize: 20}}>
            {' '}
            Go to Factorial App
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#bdf3bd',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Login')}>
          <Text style={{color: '#be1111', fontSize: 20}}> Go to Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#bdf3bd',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Todo')}>
          <Text style={{color: '#406cf5', fontSize: 20}}> Go to Todo App</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#bdf3bd',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('Profile')}>
          <Text style={{color: '#406cf5', fontSize: 20}}>
            {' '}
            Go to Profile Page
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            flex: 1,
            width: '85%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#80b297',
            borderRadius: 28,
          }}
          onPress={() => navigation.navigate('TodoFlatList')}>
          <Text style={{color: '#406cf5', fontSize: 20}}>
            {' '}
            To do with flatList
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
