import React, {useRef, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  //StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  //useColorScheme,
  View,
  FlatList,
} from 'react-native';
import IconM from 'react-native-vector-icons/FontAwesome5';
import Loading from '../components/Loading';

const TodoFlatListScreen = () => {
  const TextInputRef = useRef();
  let initialTodos = [
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
    {title: 'Writing Code', isDone: true},
    {title: 'Reading API', isDone: false},
    {title: 'Working Algo_s', isDone: false},
    {title: 'Watching Movie', isDone: false},
  ];
  const [todoTitle, setTodoTitle] = useState('');
  const [todoList, setTodoList] = useState(initialTodos);
  const [isLoading, setIsLoading] = useState(false);

  const MyItemSeparatorComponent = () => {
    return (
      <View
        style={{margin: 5, height: 1, width: '100%', backgroundColor: '#cde'}}
      />
    );
  };
  const MyRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => updateTodo(item, index)}
        onLongPress={() => deleteTodo(item, index)}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          borderColor: item.isDone ? '#0f0' : '#f00',
          borderWidth: 1,
          padding: 10,
          width: '90%',
          height: 60,
          borderRadius: 20,
          margin: 10,
        }}
        key={index.toString()}>
        <Text style={{color: 'rgba(95,135,215,0.9)'}}>{item.title}</Text>
        <IconM
          name={item.isDone ? 'check-circle' : 'circle-notch'}
          size={30}
          color={item.isDone ? '#0f0' : '#f00'}
        />
      </TouchableOpacity>
    );
  };
  const clearText = () => {
    TextInputRef.current.clear();
  };
  const addTodo = value => {
    setIsLoading(true);
    if (value.length < 3) {
      alert('Cannot be empty or at least 3 characters');
    } else {
      let todoItem = {title: value, isDone: false};
      setTodoList(prev => [...prev, todoItem]);
      clearText();
    }
    setIsLoading(false);
  };

  const updateTodo = (td, ix) => {
    setTodoList(prev => {
      prev[ix].isDone = !prev[ix].isDone;
      return [...prev];
    });
  };

  const deleteTodo = (td, ix) => {
    setTodoList(prev => {
      prev.pop(prev[ix]);
      return [...prev];
    });
  };

  return isLoading ? (
    <Loading />
  ) : (
    <SafeAreaView
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
          width: '100%',
        }}>
        <TextInput
          placeholder=" Enter your To Do's here"
          ref={TextInputRef}
          onChangeText={value => setTodoTitle(value)}
          value={todoTitle}
          style={{
            borderColor: '#00f',
            borderWidth: 1,
            width: '100%',
            marginTop: 20,
          }}
        />
        <View
          style={{
            flex: 3,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            padding: 10,
          }}>
          <Text style={{color: 'rgba(95,135,215,0.9)', margin: 10}}>
            Yapılacaklar:
          </Text>
          <FlatList
            style={{flex: 1, width: '100%'}}
            data={todoList}
            renderItem={MyRenderItem}
            itemSeparatorComponent={MyItemSeparatorComponent}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
      <TouchableOpacity
        onPress={() => addTodo(todoTitle)}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#0ff',
          //padding: 5,
          width: 60,
          height: 60,
          borderRadius: 30,
          //marginTop: 10,
          position: 'absolute',
          right: 30,
          bottom: 30,
        }}>
        <Text style={{color: '#fff', fontSize: 38}}>+</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
export default TodoFlatListScreen;
